/*eslint-disable */
import React, { useEffect, useState } from "react";
import Aux from "../../hoc/_Aux";
import {
  Button,
  Col,
  FormControl,
  InputGroup,
  Row,
  OverlayTrigger,
  Tooltip,
  Form
} from "react-bootstrap";
import Card from "../../App/components/MainCard";

import moment from "moment";
import { RemoteTable } from "../../App/components/RemoteTable";
import { toast } from "react-toastify";
import CkeditorModal from "../../App/components/CkeditorModal";
import { PlusCircle, Search, Check, X, Share2 } from "react-feather";
import { Link } from "react-router-dom";
import baseApi from "../../services/baseApi";
import { localDisk } from "../../services/Utilities";
import Tambah from "./Tambah";

import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
import "react-bootstrap-table2-toolkit/dist/react-bootstrap-table2-toolkit.min.css";
import "react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css";
import ReactHtmlParser from "react-html-parser";
import Pagination from "react-bootstrap/Pagination";
import PageItem from "react-bootstrap/PageItem";
import BootstrapTable from "react-bootstrap-table-next";

const Home = () => {
  const id = require("moment/locale/id");
  moment.updateLocale("id", id);

  const [resultTrxData, setResultTrxData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [page, setPage] = useState(1);
  const [currentPage, setCurrentPage] = useState(1);
  const [totalRows, setTotalRows] = useState(0);
  const [sizePerPage, setSizePerPage] = useState(10);
  const [fromPage, setFromPage] = useState(1);
  const [keyword, setKeyword] = useState("");
  const [openTambahModal, setOpenTambahModal] = useState(false);
  const [openCkeditorModal, setOpenCkeditorModal] = useState(false);
  const [openCkeditorModalDesk, setOpenCkeditorModalDesk] = useState(false);
  const [paginationParameter, setPaginationParameter] = useState("");

  const [inputData, setInputData] = useState({
    mst_jenis_dok_id: "",
    sumber_data: "",
    nama_data: "",
    url_file_upload: "",
    is_shared: "",
  });

  const [catatan, setCatatan] = useState("");
  const [deskripsi, setDeskripsi] = useState("");

  const [error, setError] = useState([]);
  const [row, setRow] = useState("");

  const headerSortingClasses = (column, sortOrder, isLastSorting, colIndex) =>
    sortOrder === "asc" ? "bg-success" : "bg-info";

  const fetchData = (e) => {
    // try {
    e?.preventDefault();

    let apiTrxData = "";
    if (keyword === "" || keyword === undefined || keyword === null) {
      apiTrxData = "/trx-data";
    } else {
      apiTrxData = `/trx-data?q=${keyword}`;
    }

    // const response = baseApi.get(apiTrxData);
    baseApi
      .get(apiTrxData)
      .then((response) => {
        if (response?.status === 200) {
          setResultTrxData(response?.data?.result?.data);
          setTotalRows(response?.data?.result?.total);
          setPaginationParameter(response?.data?.result);
        }
      })
      .catch((err) => {
        toast.warning("Terjadi kesalahan", {
          position: toast.POSITION.TOP_RIGHT,
        });
        // console.log(error);
      });
    // } catch (error) {
    // }
  };

  const handleChangeInputData = (e) => {
    e.persist();
    setInputData({ ...inputData, [e.target.name]: e.target.value });
  };

  const handleInputCatatan = (e, editor) => {
    const data = editor.getData();
    setCatatan(data);
  };

  const handleInputDeskripsi = (e, editor) => {
    const data = editor.getData();
    setDeskripsi(data);
  };

  const handleKeyword = (e) => {
    setKeyword(e.target.value);
  };

  const handleTambahModal = () => {
    setOpenTambahModal(!openTambahModal);
    setInputData({
      grupDokumen: "",
      keterangan: "",
    });
    setError([]);
  };

  const handleOnTableChange = (page) => async (e) => {
    let result = resultTrxData;
    setLoading(true);
    setPage(page);

    const response = await baseApi.get(`/trx-data?page=${page}`);
    if (response?.status === 200) {
      setResultTrxData(response?.data?.result?.data);
      setCurrentPage(response?.data?.result?.current_page);
      setSizePerPage(response?.data?.result.per_page);
      setFromPage(response?.data?.result?.total);
      setLoading(false);
      setPaginationParameter(response?.data?.result);
    }
  };

  function prevPages(hal) {
    const listPages = [];
    listPages.push(
      <PageItem onClick={handleOnTableChange(hal - 1)}>{hal - 1}</PageItem>
    );
    return listPages;
  }

  function nextPages(hal) {
    const listPages = [];
    listPages.push(
      <PageItem onClick={handleOnTableChange(hal + 1)}>{hal + 1}</PageItem>
    );
    return listPages;
  }

  const columns = [
    // {
    //   dataField: "",
    //   text: "No.",
    //   formatter: (cell, row, rowIndex, extraData) => (
    //     <>{page * sizePerPage - sizePerPage + (rowIndex + 1)}</>
    //   ),
    //   headerAlign: "center",
    //   align: "center",
    // },
    {
      dataField: "",
      text: "",
      headerAlign: "center",
      align: "center",
      formatter: (cell, row, rowIndex, extraData) => (
        <div>
          <OverlayTrigger overlay={<Tooltip>Kelola Data</Tooltip>}>
            <Link
              to={{
                pathname: "/kelola-data",
                state: {
                  idTrxData: row.id,
                },
              }}
            >
              <Button className="py-1 px-2" variant="outline-primary" size="sm">
                <Share2 size={18} />
              </Button>
            </Link>
          </OverlayTrigger>
        </div>
      ),
    },
    // {
    //   dataField: "mst_jenis_dok.jenis_dokumen",
    //   text: "Jenis Dokumen",
    //   headerAlign: "center",
    //   sort: true,
    //   headerSortingClasses,
    // },
    {
      dataField: "nama_data",
      text: "Nama Data",
      headerAlign: "center",
      formatter: (cell, row, rowIndex, extraData) => (
        <>
          {row.jenis_data === "UPLOAD" && (
            <a href={`${localDisk}/${row.url_file_upload}`} target="_blank">
              {row.nama_data}
              {/* <File size={14} /> */}
            </a>
          )}
          {row.jenis_data === "URL" && (
            <a href={row.url_upload} target="_blank">
              {row.nama_data}
              {/* <File size={14} /> */}
            </a>
          )}
        </>
      ),
      sort: true,
      headerSortingClasses,
    },
    {
      dataField: "sumber_data",
      text: "Sumber Data",
      headerAlign: "center",
      sort: true,
      headerSortingClasses,
      formatter: (cell, row, rowIndex, extraData) => (
        <>
          <a
            href="#"
            onClick={() =>
              setOpenCkeditorModalDesk(
                !openCkeditorModalDesk,
                setDeskripsi(row.deskripsi_data)
              )
            }
          >
            {row?.sumber_data}
          </a>
        </>
      ),
    },
    // {
    //   dataField: "deskripsi_data",
    //   text: "Deskripsi",
    //   headerAlign: "center",
    //   sort: true,
    //   headerSortingClasses,
    // },
    {
      dataField: "is_shared",
      text: "Kelola Bersama",
      headerAlign: "center",
      align: "center",
      sort: true,
      headerSortingClasses,
      formatter: (cell, row, rowIndex, extraData) => (
        <>
          <a
            href="#"
            onClick={() =>
              // setOpenUpdateModal(!openUpdateModal, setRow(row?.id))
              setOpenCkeditorModal(!openCkeditorModal, setCatatan(row.catatan))
            }
          >
            {row?.is_shared === true ? <Check /> : <X />}
            {/* <File size={14} /> */}
          </a>
        </>
      ),
    },
    {
      dataField: "created_at",
      text: "Tanggal Diunggah",
      formatter: (cell, row, rowIndex, extraData) => (
        <>{moment(row.created_at).format("D MMMM YYYY H:mm:ss")}</>
      ),
      headerAlign: "center",
      align: "center",
      sort: true,
      headerSortingClasses,
    },
    {
      dataField: "created_by",
      text: "Diunggah Oleh",
      headerAlign: "center",
      align: "center",
      sort: true,
      headerSortingClasses,
    },
  ];

  /*eslint-disable */
  useEffect(() => {
    let cleanUp = true;
    if (cleanUp) {
      fetchData();
    }

    return () => {
      cleanUp = false;
    };
  }, []);

  return (
    <>
      <Aux>
        <Row>
          <Col>
            <Card title="Data" isOption>
              <div>
                <Form onSubmit={fetchData}>
                  <InputGroup className="mb-3">
                    <FormControl
                      autoFocus
                      placeholder="Cari disini..."
                      aria-label="Cari disini..."
                      aria-describedby="basic-addon2"
                      type="text"
                      value={keyword}
                      name="keyword"
                      onChange={handleKeyword}
                    />
                    <InputGroup.Append>
                      <Button
                        variant="outline-primary"
                        type="submit"
                      >
                        <Search size={14} className="mr-1" />
                        CARI
                      </Button>
                    </InputGroup.Append>
                  </InputGroup>
                </Form>

                <OverlayTrigger overlay={<Tooltip>Tambah Data</Tooltip>}>
                  <Button
                    className="py-2 px-3 btn-sync"
                    variant="outline-success"
                    onClick={handleTambahModal}
                  >
                    <PlusCircle size={14} />
                  </Button>
                </OverlayTrigger>
              </div>

              <div className="table-responsive">
                <BootstrapTable
                  keyField="id"
                  data={resultTrxData}
                  columns={columns}
                  hover
                  bordered
                  headerClasses="thead-dark"
                  className="table table-responsive"
                  noDataIndication={"Tidak ada data..."}
                />

                {resultTrxData?.length > 0 && (
                  <div className="d-lg-flex">
                    <div className="ml-auto text-center">
                      <Pagination
                        className="pagination justify-content-center"
                        listClassName="justify-content-center"
                      >
                        {paginationParameter.current_page > 1 && (
                          <>
                            <PageItem
                              onClick={handleOnTableChange(
                                paginationParameter.current_page - 1
                              )}
                              tabIndex="-1"
                            >
                              <i className="fa fa-angle-left" />
                              <span className="sr-only">Previous</span>
                            </PageItem>
                            {prevPages(paginationParameter?.current_page)}
                          </>
                        )}

                        <PageItem
                          className="active"
                          onClick={(e) => e.preventDefault()}
                        >
                          {paginationParameter.current_page}
                        </PageItem>
                        {paginationParameter?.next_page_url && (
                          <>
                            {nextPages(paginationParameter?.current_page)}
                            <PageItem
                              onClick={handleOnTableChange(
                                paginationParameter.current_page + 1
                              )}
                            >
                              <i className="fa fa-angle-right" />
                              <span className="sr-only">Next</span>
                            </PageItem>
                          </>
                        )}
                      </Pagination>
                    </div>
                  </div>
                )}
              </div>
            </Card>
          </Col>
        </Row>
      </Aux>

      {openTambahModal && (
        <Tambah
          openTambahModal={openTambahModal}
          setOpenTambahModal={setOpenTambahModal}
          handleTambahModal={handleTambahModal}
          inputData={inputData}
          setInputData={setInputData}
          handleChangeInputData={handleChangeInputData}
          catatan={catatan}
          setCatatan={setCatatan}
          handleInputCatatan={handleInputCatatan}
          deskripsi={deskripsi}
          setDeskripsi={setDeskripsi}
          handleInputDeskripsi={handleInputDeskripsi}
          error={error}
          setError={setError}
          loading={loading}
          setLoading={setLoading}
          fetchData={fetchData}
        />
      )}

      <CkeditorModal
        title="Catatan"
        value={ReactHtmlParser(catatan)}
        namaState={openCkeditorModal}
        namaSetState={setOpenCkeditorModal}
        buttonCancel="Tutup"
        size="sm" //lg,xl,sm,md
      />

      <CkeditorModal
        title="Deskripsi Data"
        value={ReactHtmlParser(deskripsi)}
        namaState={openCkeditorModalDesk}
        namaSetState={setOpenCkeditorModalDesk}
        buttonCancel="Tutup"
        size="xl" //lg,xl,sm,md
      />
    </>
  );
};

export default Home;
