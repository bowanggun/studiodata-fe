import React, { useEffect, useState } from "react";
import Aux from "../../hoc/_Aux";
import {
  Button,
  Col,
  Row,
  OverlayTrigger,
  Card as CardBootstrap,
  Tooltip,
  Tabs,
  Tab,
} from "react-bootstrap";
import { toast } from "react-toastify";

import moment from "moment";
import Card from "../../App/components/MainCard";
import { sourceIcon } from "../../App/components/Icon";
import TambahRiwayat from "./TambahRiwayat";
import RiwayatData from "./RiwayatData";
import DetailData from "./DetailData";
import {
  getFormatEkstensi,
  getFormatData,
} from "../../App/components/FunctionFormatData";

import baseApi from "../../services/baseApi";
import { localDisk } from "../../services/Utilities";
import { useLocation, useHistory } from "react-router-dom";
import { ArrowLeft, PlusCircle } from "react-feather";

const KelolaData = () => {
  const id = require("moment/locale/id");
  moment.updateLocale("id", id);
  const location = useLocation();
  const history = useHistory();

  const [resultTrxData, setResultTrxData] = useState([]);
  const [openTambahRiwayatModal, setOpenTambahRiwayatModal] = useState(false);
  const [inputData, setInputData] = useState({
    mst_jenis_dok_id: "",
    sumber_data: "",
    nama_data: "",
    url_file_upload: "",
    is_shared: "",
  });

  const [error, setError] = useState([]);
  const [row, setRow] = useState("");
  const [catatan, setCatatan] = useState("");
  const [deskripsi, setDeskripsi] = useState("");
  const [loading, setLoading] = useState(false);

  const subtitleCard = (
    <OverlayTrigger overlay={<Tooltip>Kembali ke halaman sebelumnya</Tooltip>}>
      <Button
        variant="outline-secondary"
        size="sm"
        className="py-1 px-2"
        onClick={history.goBack}
      >
        <ArrowLeft size={16} />
      </Button>
    </OverlayTrigger>
  );

  // console.log("location", location.state);

  const fetchTrxData = async () => {
    try {
      const response = await baseApi.get(
        `/trx-data/${location.state.idTrxData}`
      );
      if (response?.status === 200) {
        setResultTrxData(response?.data?.result);
      }
    } catch (error) {
      history.push("/input-data");
      toast.warning("Terjadi kesalahan", {
        position: toast.POSITION.TOP_RIGHT,
      });
      // console.log(error);
    }
  };

  const handleTambahRiwayatModal = () => {
    setOpenTambahRiwayatModal(!openTambahRiwayatModal);
    setError([]);
  };

  const handleInputCatatan = (e, editor) => {
    const data = editor.getData();
    setCatatan(data);
  };

  const handleInputDeskripsi = (e, editor) => {
    const data = editor.getData();
    setDeskripsi(data);
  };

  /*eslint-disable */
  useEffect(() => {
    let cleanUp = true;
    if (cleanUp) {
      fetchTrxData();
    }

    return () => {
      cleanUp = false;
    };
  }, []);

  return (
    <>
      <Aux>
        <Row>
          <Col>
            <Card
              title="Detail Data" //{resultTrxData.nama_data}
              subtitle={subtitleCard}
              isOption
              className="px-2"
            >
              <CardBootstrap.Body className="border-bottom mb-2">
                <div className="row align-items-center justify-content-center">
                  <div className="col-auto">
                    {getFormatData(resultTrxData.format_data)}

                    {resultTrxData.format_data === "URL" ? (
                      <a href={resultTrxData.url_upload} target="_blank">
                        <img
                          className="img-responsive mb-1 mx-2"
                          width="30"
                          src={sourceIcon.formatEkstensiURL}
                          alt={resultTrxData.ekstensi_data}
                        />
                      </a>
                    ) : (
                      <a
                        href={`${localDisk}/${resultTrxData.url_file_upload}`}
                        target="_blank"
                      >
                        {getFormatEkstensi(resultTrxData.ekstensi_data)}
                      </a>
                    )}
                  </div>
                  <div className="col text-center">
                    <h3>{resultTrxData.nama_data}</h3>
                    <h6 className="text-c-blue mb-0">
                      Terakhir Diubah Oleh : {resultTrxData.updated_by} pada{" "}
                      {moment(resultTrxData.updated_at).format(
                        "D MMMM YYYY H:mm:ss"
                      )}
                    </h6>
                  </div>
                </div>
              </CardBootstrap.Body>

              <Tabs defaultActiveKey="home" className="px-2">
                <Tab eventKey="home" title="DATA">
                  <DetailData resultTrxData={resultTrxData} />
                </Tab>
                <Tab eventKey="profile" title="RIWAYAT">
                  <RiwayatData
                    resultTrxData={resultTrxData}
                    // openSumberDataModal={openSumberDataModal}
                    // handleTambahRiwayatModal={handleTambahRiwayatModal}
                    // inputData={inputData}
                    // error={error}
                    // setError={setError}
                    // catatan={catatan}
                    // setCatatan={setCatatan}
                    // handleInputCatatan={handleInputCatatan}
                    // deskripsi={deskripsi}
                    // setDeskripsi={setDeskripsi}
                    // handleInputDeskripsi={handleInputDeskripsi}
                    // loading={loading}
                    // setLoading={setLoading}
                    // fetchTrxData={fetchTrxData}
                    // row={row}
                  />
                </Tab>
              </Tabs>
            </Card>
          </Col>
        </Row>
      </Aux>

      <OverlayTrigger overlay={<Tooltip>Tambah Riwayat Data</Tooltip>}>
        <Button
          className="py-2 px-3 btn-sync"
          variant="outline-success"
          onClick={() =>
            setOpenTambahRiwayatModal(
              !openTambahRiwayatModal,
              setInputData({
                mst_jenis_dok_id: resultTrxData.mst_jenis_dok_id,
                sumber_data: resultTrxData.sumber_data,
                nama_data: resultTrxData.nama_data,
                url_file_upload: resultTrxData.url_file_upload,
                url_upload: resultTrxData.url_upload,
                is_shared: resultTrxData.is_shared === true ? 1 : 0,
              }),
              setCatatan(resultTrxData.catatan),
              setDeskripsi(resultTrxData.deskripsi_data),
              setRow(resultTrxData.id)
            )
          }
        >
          <PlusCircle size={14} />
        </Button>
      </OverlayTrigger>

      {openTambahRiwayatModal && (
        <TambahRiwayat
          openTambahRiwayatModal={openTambahRiwayatModal}
          setOpenTambahRiwayatModal={setOpenTambahRiwayatModal}
          handleTambahRiwayatModal={handleTambahRiwayatModal}
          inputData={inputData}
          setInputData={setInputData}
          catatan={catatan}
          setCatatan={setCatatan}
          handleInputCatatan={handleInputCatatan}
          deskripsi={deskripsi}
          setDeskripsi={setDeskripsi}
          handleInputDeskripsi={handleInputDeskripsi}
          error={error}
          setError={setError}
          loading={loading}
          setLoading={setLoading}
          fetchTrxData={fetchTrxData}
          row={row}
        />
      )}
    </>
  );
};

export default KelolaData;
