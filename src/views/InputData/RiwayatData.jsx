import React, { useState } from "react";
import { OverlayTrigger, Table, Tooltip } from "react-bootstrap";
import avatar2 from "../../assets/images/user/avatar-2.jpg";
import { sourceIcon } from "../../App/components/Icon";
import InfoMetaDataFileModal from "../../App/components/InfoMetaDataFile";
import moment from "moment";
import { localDisk } from "../../services/Utilities";
import {
  getFormatEkstensi,
  getFormatData,
} from "../../App/components/FunctionFormatData";

const RiwayatData = (props) => {
  const { resultTrxData } = props;

  const id = require("moment/locale/id");
  moment.updateLocale("id", id);

  const [dataRiwayat, setDataRiwayat] = useState({});
  const [openInfoMetaDataFileModal, setOpenInfoMetaDataFileModal] =
    useState(false);

  return (
    <>
      <Table responsive hover>
        <tbody>
          {resultTrxData?.trx_riwayat_data?.map((item, index) => (
            <tr className="unread" key={index} value={item.id}>
              <td>
                <img
                  className="rounded-circle"
                  style={{ width: "40px" }}
                  src={avatar2}
                  alt="activity-user"
                />
              </td>
              <td>
                <h6 className="mb-1 text-primary">{item.nama_data}</h6>
                <p className="m-0">{item.created_by}</p>
              </td>
              <td>
                <h6 className="text-muted">
                  <i className="fa fa-circle text-c-green f-10 m-r-15" />
                  {moment(item.created_at).format("D MMMM YYYY H:mm:ss")}
                </h6>
              </td>
              <td>
                {item.is_shared === true ? (
                  <span className="label theme-bg2 text-white f-12">
                    Dikelola Bersama
                  </span>
                ) : (
                  <span className="label theme-bg text-white f-12">
                    Tidak Dikelola Bersama
                  </span>
                )}
              </td>
              <td>
                <OverlayTrigger overlay={<Tooltip>Properti Data</Tooltip>}>
                  <span
                    style={{ cursor: "pointer" }}
                    onClick={() =>
                      setOpenInfoMetaDataFileModal(
                        !openInfoMetaDataFileModal,
                        setDataRiwayat(item)
                      )
                    }
                  >
                    {getFormatData(item.format_data)}
                    {/* <img
                      className="img-responsive mb-1"
                      width="30"
                      src={sourceIcon.formatDataDocument}getFormatData
                      alt="alert"
                    /> */}
                  </span>
                </OverlayTrigger>

                <OverlayTrigger overlay={<Tooltip>Lihat Data</Tooltip>}>
                  {item.format_data === "URL" ? (
                    <a href={item.url_upload} target="_blank">
                      <img
                        className="img-responsive mb-1 mx-2"
                        width="30"
                        src={sourceIcon.formatEkstensiURL}
                        alt={item.ekstensi_data}
                      />
                    </a>
                  ) : (
                    <a
                      href={`${localDisk}/${item.url_file_upload}`}
                      target="_blank"
                    >
                      {getFormatEkstensi(item.ekstensi_data)}
                      {/* <img
                                  className="img-responsive mb-1"
                                  width="30"
                                  alt={item.ekstensi_data}
                                /> */}
                    </a>
                  )}
                </OverlayTrigger>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>

      <InfoMetaDataFileModal
        title="Properti Data"
        value={dataRiwayat}
        namaState={openInfoMetaDataFileModal}
        namaSetState={setOpenInfoMetaDataFileModal}
        buttonCancel="Tutup"
        size="md" //lg,xl,sm,md
      />
    </>
  );
};

export default RiwayatData;
