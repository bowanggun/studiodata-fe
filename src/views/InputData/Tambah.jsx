import React, { useRef, useEffect, useState } from "react";
import { Modal, Form, Button } from "react-bootstrap";
import { PlusCircle, XCircle } from "react-feather";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import baseApi from "../../services/baseApi";
import { toast } from "react-toastify";
// import Select from "react-select";
import Loader from "react-loader-spinner";
import SelectCustom from "../../App/components/SelectCustom";

const Tambah = (props) => {
  const {
    openTambahModal,
    setOpenTambahModal,
    handleTambahModal,
    inputData,
    setInputData,
    handleChangeInputData,
    error,
    setError,
    catatan,
    setCatatan,
    handleInputCatatan,
    deskripsi,
    setDeskripsi,
    handleInputDeskripsi,
    loading,
    setLoading,
    fetchData,
  } = props;

  const [resultJenisDokumen, setResultJenisDokumen] = useState([]);
  const [jenisData, setJenisData] = useState("UPLOAD");
  const [isShared, setIsShared] = useState(1);
  const [urlFileUpload, setUrlFileUpload] = useState([]);
  const [dataTag, setDataTag] = useState([]);
  const [dataOrganisasi, setDataOrganisasi] = useState([]);
  const [dataTopik, setDataTopik] = useState([]);
  const [valueDataTag, setValueDataTag] = useState("");
  const [valueDataOrganisasi, setValueDataOrganisasi] = useState("");
  const [valueDataTopik, setValueDataTopik] = useState("");

  const innerRef = useRef();

  const fetchDataJenisDokumen = () => {
    baseApi
      .get(`/jenis-dok`)
      .then((response) => {
        if (response?.status === 200) {
          setResultJenisDokumen(response?.data?.result);
          setLoading(false);
        }
      })
      .catch((error) => {
        setLoading(false);
        toast.warning("Terjadi kesalahan", {
          position: toast.POSITION.TOP_RIGHT,
        });
        // console.log(error);
      });
  };

  const fetchDataTag = (inputValue) => {
    setLoading(true);
    baseApi
      .get(`/data-tag?q=${inputValue}&paging=false`)
      .then((response) => {
        if (response?.status === 200) {
          setDataTag(response?.data?.result);
          setLoading(false);
        }
      })
      .catch((error) => {
        setLoading(false);
        toast.warning("Terjadi kesalahan", {
          position: toast.POSITION.TOP_RIGHT,
        });
        // console.log(error);
      });
  };

  const fetchDataOrganisasi = (inputValue) => {
    setLoading(true);
    baseApi
      .get(`/organisasi?q=${inputValue}&paging=false`)
      .then((response) => {
        if (response?.status === 200) {
          setDataOrganisasi(response?.data?.result);
          setLoading(false);
        }
      })
      .catch((error) => {
        setLoading(false);
        toast.warning("Terjadi kesalahan", {
          position: toast.POSITION.TOP_RIGHT,
        });
        // console.log(error);
      });
  };

  const fetchDataTopik = (inputValue) => {
    setLoading(true);
    baseApi
      .get(`/topik?q=${inputValue}&paging=false`)
      .then((response) => {
        if (response?.status === 200) {
          setDataTopik(response?.data?.result);
          setLoading(false);
        }
      })
      .catch((error) => {
        setLoading(false);
        toast.warning("Terjadi kesalahan", {
          position: toast.POSITION.TOP_RIGHT,
        });
        // console.log(error);
      });
  };

  const handleChangeUrlFileUpload = (e) => {
    if (jenisData === "UPLOAD") {
      setUrlFileUpload(e.target.files[0]);
    } else if (jenisData === "URL") {
      setUrlFileUpload(e.target.value);
    }
  };

  const handleChangeInputDataJenisData = (e) => {
    e.persist();
    if (e.target.id === "jenis_data_upload") {
      setJenisData("UPLOAD");
    } else {
      setJenisData("URL");
    }
  };

  const handleChangeInputDataIsShared = (e) => {
    e.persist();
    if (e.target.id === "is_shared_true") {
      setIsShared(1);
    } else {
      setIsShared(0);
    }
  };

  const handleInputTrxData = (e) => {
    e.preventDefault();
    setLoading(true);

    const formData = new FormData();

    formData.append(
      "mst_jenis_dok_id",
      inputData.mst_jenis_dok_id ? inputData.mst_jenis_dok_id : ""
    );
    formData.append(
      "sumber_data",
      inputData.sumber_data ? inputData.sumber_data : ""
    );
    formData.append("jenis_data", jenisData);
    formData.append(
      "nama_data",
      inputData.nama_data ? inputData.nama_data : ""
    );
    formData.append("deskripsi_data", deskripsi);
    formData.append("catatan", catatan);
    formData.append("url_file_upload", urlFileUpload ? urlFileUpload : "");
    formData.append(
      "url_upload",
      inputData.url_upload ? inputData.url_upload : ""
    );
    formData.append("is_shared", isShared);
    formData.append(
      "data_tag",
      valueDataTag ? JSON.stringify(valueDataTag) : ""
    );
    formData.append(
      "data_topik",
      valueDataTag ? JSON.stringify(valueDataTopik) : ""
    );
    formData.append(
      "data_organisasi",
      valueDataTag ? JSON.stringify(valueDataOrganisasi) : ""
    );

    baseApi
      .post(`/trx-data`, formData)
      .then((response) => {
        if (response?.status === 200) {
          fetchData();
          toast.success(response?.data?.message, {
            position: toast.POSITION.TOP_RIGHT,
          });
          setOpenTambahModal(false);
          setCatatan("");
          setDeskripsi("");
          setInputData({});
          setLoading(false);
        } else {
          setLoading(false);
          // console.log("response", response);
        }
      })
      .catch((error) => {
        setLoading(false);
        setError(error.response?.data?.errors);

        toast.error(error.response?.data?.message, {
          position: toast.POSITION.TOP_RIGHT,
        });
      });
  };

  /*eslint-disable */
  useEffect(() => {
    setTimeout(() => {
      innerRef.current.focus();
      setCatatan("");
      setDeskripsi("");
      setInputData({});
      fetchDataJenisDokumen();
    }, 1);
  }, [setCatatan, setDeskripsi, setInputData]);

  return (
    <>
      <Modal
        show={openTambahModal}
        onHide={handleTambahModal}
        backdrop="static"
        keyboard={false}
        size="lg"
      >
        <Form onSubmit={handleInputTrxData}>
          <Modal.Header>
            <Modal.Title>Tambah Data</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form.Group className="mb-3">
              <Form.Label htmlFor="mst_jenis_dok_id">Jenis Dokumen</Form.Label>
              <Form.Control
                as="select"
                id="mst_jenis_dok_id"
                name="mst_jenis_dok_id"
                value={inputData.mst_jenis_dok_id}
                onChange={handleChangeInputData}
                style={{ cursor: "pointer" }}
              >
                <option selected value="">
                  -
                </option>
                {resultJenisDokumen?.map((item, index) => (
                  <option key={index} value={item.id}>
                    {item?.jenis_dokumen}
                  </option>
                ))}
              </Form.Control>
              <span className="text-danger">
                <i>{error?.mst_jenis_dok_id ?? ""}</i>
              </span>
            </Form.Group>

            <Form.Group className="mb-3 form-label-group">
              <Form.Control
                ref={innerRef}
                placeholder="Nama Data"
                className="form-control"
                type="text"
                id="nama_data"
                name="nama_data"
                value={inputData.nama_data}
                onChange={handleChangeInputData}
              />
              <Form.Label htmlFor="nama_data">Nama Data</Form.Label>
              <span className="text-danger">
                <i>{error?.nama_data ?? ""}</i>
              </span>
            </Form.Group>

            <Form.Group className="mb-3 form-label-group">
              <Form.Control
                placeholder="Sumber Data"
                className="form-control"
                type="text"
                id="sumber_data"
                name="sumber_data"
                value={inputData.sumber_data}
                onChange={handleChangeInputData}
              />
              <Form.Label htmlFor="sumber_data">Sumber Data</Form.Label>
              <span className="text-danger">
                <i>{error?.sumber_data ?? ""}</i>
              </span>
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label htmlFor="jenis_data">Jenis Data</Form.Label>
              <div key={`inline-radio`} className="mb-3">
                <Form.Check
                  inline
                  label="UPLOAD"
                  name="jenis_data"
                  type="radio"
                  id={`jenis_data_upload`}
                  value={jenisData}
                  onChange={handleChangeInputDataJenisData}
                  defaultChecked={jenisData === "UPLOAD"}
                />
                <Form.Check
                  inline
                  label="URL"
                  name="jenis_data"
                  type="radio"
                  id={`jenis_data_url`}
                  value={jenisData}
                  onChange={handleChangeInputDataJenisData}
                  defaultChecked={jenisData === "URL"}
                />
              </div>
              <span className="text-danger">
                <i>{error?.is_shared ?? ""}</i>
              </span>
            </Form.Group>

            {jenisData === "UPLOAD" && (
              <Form.Group className="mb-3">
                {/* <Form.Label htmlFor="url_file_upload">Unggah File</Form.Label> */}
                <Form.File
                  label="Unggah File"
                  className="form-control"
                  id="url_file_upload"
                  name="sumber_data"
                  //   value={urlUpload}
                  onChange={handleChangeUrlFileUpload}
                />
                <span className="text-danger">
                  <i>{error?.url_file_upload ?? ""}</i>
                </span>
              </Form.Group>
            )}

            {jenisData === "URL" && (
              <Form.Group className="mb-3 form-label-group">
                <Form.Control
                  placeholder="Masukan URL"
                  className="form-control"
                  type="text"
                  id="url_upload"
                  name="url_upload"
                  value={inputData.urlUpload}
                  onChange={handleChangeInputData}
                />
                <Form.Label htmlFor="url_upload">URL</Form.Label>
                <span className="text-danger">
                  <i>{error?.url_upload ?? ""}</i>
                </span>
              </Form.Group>
            )}

            <Form.Group className="mb-3">
              <Form.Label htmlFor="deskripsi_data">Deskripsi Data</Form.Label>
              <CKEditor
                editor={ClassicEditor}
                data={deskripsi}
                onChange={handleInputDeskripsi}
                onReady={(editor) => {
                  editor.editing.view.change((writer) => {
                    writer.setStyle(
                      "height",
                      "200px",
                      editor.editing.view.document.getRoot()
                    );
                  });
                }}
              />

              <span className="text-danger">
                <i>{error?.deskripsi_data ?? ""}</i>
              </span>
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label htmlFor="catatan">Catatan</Form.Label>
              <CKEditor
                editor={ClassicEditor}
                data={catatan}
                onChange={handleInputCatatan}
                onReady={(editor) => {
                  editor.editing.view.change((writer) => {
                    writer.setStyle(
                      "height",
                      "200px",
                      editor.editing.view.document.getRoot()
                    );
                  });
                }}
              />

              <span className="text-danger">
                <i>{error?.catatan ?? ""}</i>
              </span>
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label htmlFor="is_shared">Kelola Bersama</Form.Label>
              <div key={`inline-radio`} className="mb-3">
                <Form.Check
                  inline
                  label="Ya"
                  name="is_shared"
                  type="radio"
                  id={`is_shared_true`}
                  value={isShared}
                  onChange={handleChangeInputDataIsShared}
                  defaultChecked={isShared === 1}
                />
                <Form.Check
                  inline
                  label="Tidak"
                  name="is_shared"
                  type="radio"
                  id={`is_shared_false`}
                  value={isShared}
                  onChange={handleChangeInputDataIsShared}
                  defaultChecked={isShared === 0}
                />
              </div>
              <span className="text-danger">
                <i>{error?.is_shared ?? ""}</i>
              </span>
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label htmlFor="data_topik">Topik</Form.Label>
              <SelectCustom
                data={dataTopik}
                setError={setError}
                setValueData={setValueDataTopik}
                fetchData={fetchDataTopik}
                labelName="dataTopik"
              />
              <span className="text-danger">
                <i>{error?.data_topik ?? ""}</i>
              </span>
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label htmlFor="data_tag">Tag</Form.Label>
              <SelectCustom
                data={dataTag}
                setError={setError}
                setValueData={setValueDataTag}
                fetchData={fetchDataTag}
                labelName="dataTag"
              />
              <span className="text-danger">
                <i>{error?.data_tag ?? ""}</i>
              </span>
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label htmlFor="data_organisasi">Organisasi</Form.Label>
              <SelectCustom
                data={dataOrganisasi}
                setError={setError}
                setValueData={setValueDataOrganisasi}
                fetchData={fetchDataOrganisasi}
                labelName="dataOrganisasi"
              />
              <span className="text-danger">
                <i>{error?.data_organisasi ?? ""}</i>
              </span>
            </Form.Group>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="primary" type="submit" disabled={loading}>
              {loading ? (
                <Loader type="ThreeDots" color="#fff" height={10} />
              ) : (
                <>
                  <PlusCircle size={14} className="mr-1" /> Simpan
                </>
              )}
            </Button>
            <Button variant="secondary" onClick={handleTambahModal}>
              <XCircle size={14} className="mr-1" />
              Tutup
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
    </>
  );
};

export default Tambah;
