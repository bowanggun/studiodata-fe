import React from "react";
import { Table } from "react-bootstrap";
import ReactHtmlParser from "react-html-parser";
import { getFormatEkstensi } from "../../App/components/FunctionFormatData";
import { sourceIcon } from "../../App/components/Icon";
import { localDisk } from "../../services/Utilities";
import { CheckCircle, X } from "react-feather";
import moment from "moment";
// import { TagCloud } from "react-tagcloud";

const DetailData = (props) => {
  const id = require("moment/locale/id");
  moment.updateLocale("id", id);

  // const data = [
  //   { value: "jQuery", count: 25 },
  //   { value: "MongoDB", count: 18 },
  //   { value: "JavaScript", count: 38 },
  //   { value: "React", count: 30 },
  //   { value: "Nodejs", count: 28 },
  //   { value: "Express.js", count: 25 },
  //   { value: "HTML5", count: 33 },
  //   { value: "CSS3", count: 20 },
  //   { value: "Webpack", count: 22 },
  //   { value: "Babel.js", count: 7 },
  //   { value: "ECMAScript", count: 25 },
  //   { value: "Jest", count: 15 },
  //   { value: "Mocha", count: 17 },
  //   { value: "React Native", count: 27 },
  //   { value: "Angular.js", count: 30 },
  //   { value: "TypeScript", count: 15 },
  //   { value: "Flow", count: 30 },
  //   { value: "NPM", count: 11 },
  // ];

  const { resultTrxData } = props;

  return (
    <>
      <Table responsive hover striped>
        {/* <thead>
          <tr>
            <th>First Name</th>
            <th>Username</th>
          </tr>
        </thead> */}
        <tbody>
          <tr>
            <td>Jenis Dokumen</td>
            <td>{resultTrxData?.mst_jenis_dok?.ref_grup_dok?.grup_dokumen}</td>
          </tr>
          <tr>
            <td>Sumber Data</td>
            <td>{resultTrxData.sumber_data}</td>
          </tr>
          <tr>
            <td>Jenis Data</td>
            <td>{resultTrxData.jenis_data}</td>
          </tr>
          <tr>
            <td>Nama Data</td>
            <td>{resultTrxData.nama_data}</td>
          </tr>
          <tr>
            <td>Deskripsi Data</td>
            <td>{ReactHtmlParser(resultTrxData.deskripsi_data)}</td>
          </tr>
          <tr>
            <td>Format Data</td>
            <td>{resultTrxData.format_data}</td>
          </tr>
          <tr>
            <td>Media Type</td>
            <td>{resultTrxData.media_type}</td>
          </tr>
          <tr>
            <td>Size Data</td>
            <td>{resultTrxData.size_data}</td>
          </tr>
          <tr>
            <td>Ekstensi Data</td>
            <td>{resultTrxData.ekstensi_data}</td>
          </tr>
          <tr>
            <td>URL</td>
            <td>
              {resultTrxData.format_data === "URL" ? (
                <a href={resultTrxData.url_upload} target="_blank" rel="noopener noreferrer">
                  <img
                    className="img-responsive mb-1 mx-2"
                    width="30"
                    src={sourceIcon.formatEkstensiURL}
                    alt={resultTrxData.ekstensi_data}
                  />
                </a>
              ) : (
                <a
                  href={`${localDisk}/${resultTrxData.url_file_upload}`}
                  target="_blank" rel="noopener noreferrer"
                >
                  {getFormatEkstensi(resultTrxData.ekstensi_data)}
                </a>
              )}
            </td>
          </tr>
          <tr>
            <td>Catatan</td>
            <td>{ReactHtmlParser(resultTrxData.catatan)}</td>
          </tr>
          <tr>
            <td>Topik</td>
            <td>
              <ul>
                {resultTrxData?.trx_data_topik?.map((item, index) => (
                  <li key={index}>{item.topik}</li>
                ))}
              </ul>
            </td>
          </tr>
          <tr>
            <td>Tag</td>
            <td>
              <ul>
                {resultTrxData?.trx_data_tag?.map((item, index) => (
                  // <li key={index}>{item.data_tag}</li>
                  <span
                    key={item.data_tag}
                    // style={{ color }}
                    className="mr-2"
                  >
                    {item.data_tag}
                  </span>
                ))}
              </ul>
              {/* {console.log("tag", resultTrxData.trx_data_tag)}
              <TagCloud
                minSize={12}
                maxSize={12}
                tags={data}
                className="simple-cloud"
                onClick={(tag) => alert(`'${tag.value}' was selected!`)}
              /> */}
            </td>
          </tr>
          <tr>
            <td>Organisasi</td>
            <td>
              <ul>
                {resultTrxData?.trx_data_organisasi?.map((item, index) => (
                  <li key={index}>{item.organisasi}</li>
                ))}
              </ul>
            </td>
          </tr>
          <tr>
            <td>Dikelola Bersama</td>
            <td>{resultTrxData.is_shared ? <CheckCircle /> : <X />}</td>
          </tr>
          <tr>
            <td>Interoperabilitas Website Bappeda</td>
            <td>
              {resultTrxData.webbapeeda_visible ? <CheckCircle /> : <X />}
            </td>
          </tr>
          <tr>
            <td>Interoperabilitas Open Data Bogor</td>
            <td>{resultTrxData.satudata_visible ? <CheckCircle /> : <X />}</td>
          </tr>
          <tr>
            <td>Tanggal Memasukan Data</td>
            <td>
              {moment(resultTrxData.created_at).format("D MMMM YYYY H:mm:ss")}
            </td>
          </tr>
        </tbody>
      </Table>
    </>
  );
};

export default DetailData;
