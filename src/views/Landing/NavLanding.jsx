import React from 'react'
import { Button, Nav, Navbar } from 'react-bootstrap';
import { Images } from '../../App/components/Icon';
import { LogIn } from 'react-feather';

const NavLanding = () => {
  return (
    <Navbar collapseOnSelect bg="light" expand="lg" sticky='top' className='py-0'>
      <Navbar.Brand href="#">
        <img
            alt="..."
            src={Images?.logo}
            width={30}
            className='img-circle'
        />
        <span className='ml-2'>Studio Data</span>
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="responsive-navbar-nav" />
      <Navbar.Collapse id="responsive-navbar-nav">
        <Nav
          className="ml-auto my-2 my-lg-0"
        >
            <Nav.Link href="/auth/signup-1" active>
                <Button size='sm' variant='info'>
                    Masuk
                    <LogIn size={14} className='ml-2'  />
                </Button>
            </Nav.Link>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}

export default NavLanding