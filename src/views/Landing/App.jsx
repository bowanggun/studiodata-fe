import React from "react";
import NavLanding from "./NavLanding";
import { sourceIcon, iconDashboard } from "../../App/components/Icon";
import "./../../assets/scss/style.scss";
import "./style.css";

const App = () => {
  const format = "formatEkstensi";

  const getFormatEkstensi = (ekstensi) => {
    // return( <img src={`${sourceIcon}${ekstensi}`} /> )
    const getEkstensi = `${format}${ekstensi}`;
    // console.log("getEks", getEkstensi);
    return Object.keys(sourceIcon).map((item) => {
      // console.log("item", item);
      if (item === getEkstensi) {
        return <img src={sourceIcon[item]} />;
      }
    });
  };
  // console.log("getFormatEkstensi", getFormatEkstensi("Xls"));

  return (
    <>
      <NavLanding />
      <div className="auth-wrapper">
        <div className="container">
          <div className="row mt-5">
            <div className="col-md-3 mb-2">
              <div className="card user-card">
                <div className="card-header">
                  <h5>PDF</h5>
                </div>
                <div className="card-block">
                  <div className="user-image">
                    <img
                      src={iconDashboard?.pdf}
                      className="img-radius"
                      alt="User-Profile-Image"
                    />
                  </div>
                  <h6 className="f-w-600 m-t-25 m-b-10">Tipe File PDF</h6>
                  <ul className="list-unstyled activity-leval">
                    <li className="active"></li>
                    <li className="active"></li>
                    <li className="active"></li>
                    <li></li>
                    <li></li>
                  </ul>
                  <p className="m-t-15 text-muted">
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry.
                  </p>
                  <hr />
                </div>
              </div>
            </div>

            <div className="col-md-3">
              <div className="card user-card">
                <div className="card-header">
                  <h5>XLS</h5>
                </div>
                <div className="card-block">
                  <div className="user-image">
                    <img
                      src={iconDashboard?.xls}
                      className="img-radius"
                      alt="User-Profile-Image"
                    />
                  </div>
                  <h6 className="f-w-600 m-t-25 m-b-10">Tipe File XLS</h6>
                  <ul className="list-unstyled activity-leval">
                    <li className="active"></li>
                    <li className="active"></li>
                    <li className="active"></li>
                    <li></li>
                    <li></li>
                  </ul>
                  <p className="m-t-15 text-muted">
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry.
                  </p>
                  <hr />
                </div>
              </div>
            </div>

            <div className="col-md-3">
              <div className="card user-card">
                <div className="card-header">
                  <h5>PNG</h5>
                </div>
                <div className="card-block">
                  <div className="user-image">
                    <img
                      src={iconDashboard?.png}
                      className="img-radius"
                      alt="User-Profile-Image"
                    />
                  </div>
                  <h6 className="f-w-600 m-t-25 m-b-10">Tipe File PNG</h6>
                  <ul className="list-unstyled activity-leval">
                    <li className="active"></li>
                    <li className="active"></li>
                    <li className="active"></li>
                    <li></li>
                    <li></li>
                  </ul>
                  <p className="m-t-15 text-muted">
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry.
                  </p>
                  <hr />
                </div>
              </div>
            </div>

            <div className="col-md-3">
              <div className="card user-card">
                <div className="card-header">
                  <h5>RAR</h5>
                </div>
                <div className="card-block">
                  <div className="user-image">
                    <img
                      src={iconDashboard?.rar}
                      className="img-radius"
                      alt="User-Profile-Image"
                    />
                  </div>
                  <h6 className="f-w-600 m-t-25 m-b-10">Tipe File RAR</h6>
                  <ul className="list-unstyled activity-leval">
                    <li className="active"></li>
                    <li className="active"></li>
                    <li className="active"></li>
                    <li></li>
                    <li></li>
                  </ul>
                  <p className="m-t-15 text-muted">
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry.
                  </p>
                  <hr />
                </div>
              </div>
            </div>
            <div className="col-md-3">
              <div className="card user-card">
                <div className="card-header">
                  <h5>RAR</h5>
                </div>
                <div className="card-block">
                  <div className="user-image">
                    <img
                      src={iconDashboard?.rar}
                      className="img-radius"
                      alt="User-Profile-Image"
                    />
                  </div>
                  <h6 className="f-w-600 m-t-25 m-b-10">Tipe File RAR</h6>
                  <ul className="list-unstyled activity-leval">
                    <li className="active"></li>
                    <li className="active"></li>
                    <li className="active"></li>
                    <li></li>
                    <li></li>
                  </ul>
                  <p className="m-t-15 text-muted">
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry.
                  </p>
                  <hr />
                </div>
              </div>
            </div>
            <div className="col-md-3">
              <div className="card user-card">
                <div className="card-header">
                  <h5>RAR</h5>
                </div>
                <div className="card-block">
                  <div className="user-image">
                    <img
                      src={iconDashboard?.rar}
                      className="img-radius"
                      alt="User-Profile-Image"
                    />
                  </div>
                  <h6 className="f-w-600 m-t-25 m-b-10">Tipe File RAR</h6>
                  <ul className="list-unstyled activity-leval">
                    <li className="active"></li>
                    <li className="active"></li>
                    <li className="active"></li>
                    <li></li>
                    <li></li>
                  </ul>
                  <p className="m-t-15 text-muted">
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry.
                  </p>
                  <hr />
                </div>
              </div>
            </div>
          </div>
          <div className="row mt-5">
            <div className="col-md-4">
              <div className="card user-card">
                <div className="card-header">
                  <h5>ZIP</h5>
                </div>
                <div className="card-block">
                  <div className="user-image">
                    <img
                      src={iconDashboard?.zip}
                      className="img-radius"
                      alt="User-Profile-Image"
                    />
                  </div>
                  <h6 className="f-w-600 m-t-25 m-b-10">Tipe File ZIP</h6>
                  <ul className="list-unstyled activity-leval">
                    <li className="active"></li>
                    <li className="active"></li>
                    <li className="active"></li>
                    <li></li>
                    <li></li>
                  </ul>
                  <p className="m-t-15 text-muted">
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry.
                  </p>
                  <hr />
                </div>
              </div>
            </div>

            <div className="col-md-4">
              <div className="card user-card">
                <div className="card-header">
                  <h5>RAR</h5>
                </div>
                <div className="card-block">
                  <div className="user-image">
                    <img
                      src={iconDashboard?.rar}
                      className="img-radius"
                      alt="User-Profile-Image"
                    />
                  </div>
                  <h6 className="f-w-600 m-t-25 m-b-10">Tipe File RAR</h6>
                  <ul className="list-unstyled activity-leval">
                    <li className="active"></li>
                    <li className="active"></li>
                    <li className="active"></li>
                    <li></li>
                    <li></li>
                  </ul>
                  <p className="m-t-15 text-muted">
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry.
                  </p>
                  <hr />
                </div>
              </div>
            </div>

            <div className="col-md-4">
              <div className="card user-card">
                <div className="card-header">
                  <h5>JPEG/JPG</h5>
                </div>
                <div className="card-block">
                  <div className="user-image">
                    <img
                      src={iconDashboard?.jpg}
                      className="img-radius"
                      alt="User-Profile-Image"
                    />
                  </div>
                  <h6 className="f-w-600 m-t-25 m-b-10">Tipe File JPG/JPEG</h6>
                  <ul className="list-unstyled activity-leval">
                    <li className="active"></li>
                    <li className="active"></li>
                    <li className="active"></li>
                    <li></li>
                    <li></li>
                  </ul>
                  <p className="m-t-15 text-muted">
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry.
                  </p>
                  <hr />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default App;
