import React, { useRef, useEffect } from "react";
import { Modal, Form, Button } from "react-bootstrap";
import { Edit3, XCircle } from "react-feather";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import baseApi from "../../services/baseApi";
import { toast } from "react-toastify";
import Loader from "react-loader-spinner";

const Ubah = (props) => {
  const {
    openUpdateModal,
    handleUpdateModal,
    inputData,
    handleChangeInputData,
    error,
    catatan,
    handleInputCatatan,
    fetchData,
    setError,
    setOpenUpdateModal,
    // setCatatan,
    loading,
    setLoading,
    setInputData,
    row,
    isHidden,
    handleChangeInputDataIsHidden,
  } = props;

  const innerRef = useRef();

  const handleUpdateMstDashboard = (e) => {
    e.preventDefault();
    setLoading(true);

    const formData = new FormData();

    formData.append("nama_dashboard", inputData.namaDashboard);
    formData.append("frame", inputData.frame);
    formData.append("link", inputData.link);
    formData.append("catatan", catatan ? catatan : "");
    formData.append("is_hidden", isHidden);
    formData.append("_method", "PUT");

    baseApi
      .post(`/mst-dashboard/${row}`, formData)
      .then((response) => {
        if (response?.status === 200) {
          fetchData();
          toast.success(response?.data?.message, {
            position: toast.POSITION.TOP_RIGHT,
          });
          setOpenUpdateModal(false);
          setInputData({});
          setLoading(false);
        } else {
          setLoading(false);
          // console.log("response", response);
        }
      })
      .catch((error) => {
        setLoading(false);
        setError(error.response?.data?.errors);

        toast.error(error.response?.data?.message, {
          position: toast.POSITION.TOP_RIGHT,
        });
      });
  };

  useEffect(() => {
    setTimeout(() => {
      innerRef.current.focus();
    }, 1);
  }, []);

  return (
    <>
      <Modal
        show={openUpdateModal}
        onHide={handleUpdateModal}
        backdrop="static"
        keyboard={false}
        size="lg"
      >
        <Form onSubmit={handleUpdateMstDashboard}>
          <Modal.Header>
            <Modal.Title>Ubah Nama Dashboard</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form.Group className="mb-3 form-label-group">
              <Form.Control
                ref={innerRef}
                type="text"
                id="namaDashboard"
                name="namaDashboard"
                aria-describedby="passwordHelpBlock"
                value={inputData.namaDashboard}
                onChange={handleChangeInputData}
              />
              <Form.Label htmlFor="namaDashboard">Nama Dashboard</Form.Label>
              <span className="text-danger">
                <i>{error?.nama_dashboard ?? ""}</i>
              </span>
            </Form.Group>

            <Form.Group className="mb-3 form-label-group">
              <Form.Control
                id="frame"
                as="textarea"
                name="frame"
                rows={8}
                value={inputData.frame}
                onChange={handleChangeInputData}
              />
              <Form.Label>iFrame</Form.Label>
              <span className="text-danger">
                <i>{error?.frame ?? ""}</i>
              </span>
            </Form.Group>

            <Form.Group className="mb-3 form-label-group">
              <Form.Control
                id="link"
                as="textarea"
                name="link"
                rows={3}
                value={inputData.link}
                onChange={handleChangeInputData}
              />
              <Form.Label>Hyperlink</Form.Label>
              <span className="text-danger">
                <i>{error?.link ?? ""}</i>
              </span>
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>Catatan</Form.Label>
              <CKEditor
                editor={ClassicEditor}
                data={catatan}
                onChange={handleInputCatatan}
                onReady={(editor) => {
                  editor.editing.view.change((writer) => {
                    writer.setStyle(
                      "height",
                      "200px",
                      editor.editing.view.document.getRoot()
                    );
                  });
                }}
              />
              <span className="text-danger">
                <i>{error?.catatan ?? ""}</i>
              </span>
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label htmlFor="is_hidden">Sembunyikan?</Form.Label>
              <div key={`inline-radio`} className="mb-3">
                <Form.Check
                  inline
                  label="Ya"
                  name="is_hidden"
                  type="radio"
                  id={`is_hidden_true`}
                  value={isHidden}
                  onChange={handleChangeInputDataIsHidden}
                  defaultChecked={isHidden === 1}
                />
                <Form.Check
                  inline
                  label="Tidak"
                  name="is_hidden"
                  type="radio"
                  id={`is_hidden_false`}
                  value={isHidden}
                  onChange={handleChangeInputDataIsHidden}
                  defaultChecked={isHidden === 0}
                />
              </div>
              <span className="text-danger">
                <i>{error?.is_hidden ?? ""}</i>
              </span>
            </Form.Group>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="primary" type="submit" disabled={loading}>
              {loading ? (
                <Loader type="ThreeDots" color="#fff" height={10} />
              ) : (
                <>
                  <Edit3 size={14} className="mr-1" /> Ubah
                </>
              )}
            </Button>
            <Button variant="secondary" onClick={handleUpdateModal}>
              <XCircle size={14} className="mr-1" />
              Tutup
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
    </>
  );
};

export default Ubah;
