/*eslint-disable */
import React, { useEffect, useState } from "react";
import Aux from "../../../hoc/_Aux";
import {
  Button,
  Col,
  FormControl,
  InputGroup,
  Row,
  OverlayTrigger,
  Tooltip,
  Table,
  Form
} from "react-bootstrap";
import Card from "../../../App/components/MainCard";

import moment from "moment";
import { RemoteTable } from "../../../App/components/RemoteTable";
import { toast } from "react-toastify";
import DeleteModal from "../../../App/components/DeleteModal";
import CkeditorModal from "../../../App/components/CkeditorModal";

import {
    Edit3,
    Trash2,
    PlusCircle,
    XCircle,
    Search,
    ArrowLeft,
    Slash,
    Download,
    Upload,
  } from "react-feather";
  import { sourceIcon } from "../../../App/components/Icon";

import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
import "react-bootstrap-table2-toolkit/dist/react-bootstrap-table2-toolkit.min.css";
import "react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css";
import ReactHtmlParser from "react-html-parser";
import Pagination from "react-bootstrap/Pagination";
import PageItem from "react-bootstrap/PageItem";
import BootstrapTable from "react-bootstrap-table-next";

import { Link, useLocation, useHistory } from "react-router-dom";
import baseApi from "../../../services/baseApi";
import Tambah from "./Tambah";
import Ubah from "./Ubah";
// import ExportExcel from "./ExportExcel";

const Home = () => {
    const id = require("moment/locale/id");
    moment.updateLocale("id", id);

    const location = useLocation();
    const history = useHistory();

    const subtitleCard = (
        <OverlayTrigger overlay={<Tooltip>Kembali ke halaman sebelumnya</Tooltip>}>
          <Button
            variant="outline-secondary"
            size="sm"
            className="py-1 px-2"
            onClick={history.goBack}
          >
            <ArrowLeft size={16} />
          </Button>
        </OverlayTrigger>
    );

    const [resultDetailKoleksi, setResultDetailKoleksi] = useState([]);
    const [loading, setLoading] = useState(false);
    const [page, setPage] = useState(1);
    const [currentPage, setCurrentPage] = useState(1);
    const [totalRows, setTotalRows] = useState(0);
    const [sizePerPage, setSizePerPage] = useState(10);
    const [fromPage, setFromPage] = useState(1);
    const [keyword, setKeyword] = useState("");
    const [openTambahModal, setOpenTambahModal] = useState(false);
    const [openUpdateModal, setOpenUpdateModal] = useState(false);
    const [openExportExcelModal, setOpenExportExcelModal] = useState(false);
    const [openDeleteModal, setOpenDeleteModal] = useState(false);
    const [openCkeditorModal, setOpenCkeditorModal] = useState(false);
    const [inputData, setInputData] = useState({
        tahun: "",
        target: "",
        realisasi: "",
        kategori: "",
        sumber: "",
    });
    const [catatan, setCatatan] = useState("");
    const [error, setError] = useState([]);
    const [row, setRow] = useState("");
    const [paginationParameter, setPaginationParameter] = useState("");

    const headerSortingClasses = (column, sortOrder, isLastSorting, colIndex) =>
    sortOrder === "asc" ? "bg-success" : "bg-info";

    const fetchData = (e) => {
      e?.preventDefault();

      if (location.state === undefined) {
        history.push("/koleksi-data");
      }
  
      // try {
        let apiDokumen = "";
        if (keyword === "" || keyword === undefined || keyword === null) {
          apiDokumen = `/pdrb-adhk-2010-lap-usaha?idMstCollection=${location.state.idMstCollection}`;
        } else {
          apiDokumen = `/pdrb-adhk-2010-lap-usaha?idMstCollection=${location.state.idMstCollection}&q=${keyword}`;
        }

        baseApi
        .get(apiDokumen)
        .then((response) => {
          if (response?.status === 200) {
            setResultDetailKoleksi(response?.data?.result?.data);
            setTotalRows(response?.data?.result?.total);
            setPaginationParameter(response?.data?.result);
          }
        })
        .catch((err) => {
          toast.warning("Terjadi kesalahan", {
            position: toast.POSITION.TOP_RIGHT,
          });
          // console.log(error);
        });
    };

    const handleInputCatatan = (e, editor) => {
        const data = editor.getData();
        setCatatan(data);
    };

    const handleChangeInputData = (e) => {
        e.persist();
        
        const { name, value } = e.target;

        if (name === "tahun") {
          const numericValue = value.replace(/\D/g, "");
          const truncatedValue = numericValue.slice(0, 4);

          setInputData({ ...inputData, [name]: truncatedValue });
        } 
        else if(name === "target" || name === "realisasi"){
          const updatedValue = value.replace(/[,.]/g, ',');

          setInputData({ ...inputData, [name]: updatedValue });
        }
        else {
          setInputData({ ...inputData, [e.target.name]: e.target.value });
        }
    };

    const handleDeleteDokumen = (e) => {
        e.preventDefault();
        setLoading(true);
    
        baseApi
          .delete(`/pdrb-adhk-2010-lap-usaha/${row}`)
          .then((response) => {
            if (response?.status === 200) {
              toast.info(response?.data?.message, {
                position: toast.POSITION.TOP_RIGHT,
              });
    
              setLoading(false);
              fetchData();
              setOpenDeleteModal(false);
            }
          })
          .catch((err) => {
            toast.error(err.response?.data?.message, {
              position: toast.POSITION.TOP_RIGHT,
            });
    
            setLoading(false);
        });
    };

    const handleKeyword = (e) => {
        setKeyword(e.target.value);
    };

    const handleTambahModal = () => {
        setOpenTambahModal(!openTambahModal);
        setInputData({
            tahun: "",
            target: "",
            realisasi: "",
            sumber: "",
            kategori: "",
            catatan: "",
        });
        setError([]);
    };

    const handleExportExcelModal = () => {
        setOpenExportExcelModal(!openExportExcelModal);
        setInputData({
          tahun: "",
        });
        setError([]);
    };
    
    const handleUpdateModal = () => {
        setOpenUpdateModal(!openUpdateModal);
        setError([]);
    };

    const handleOnTableChange = (page, keyword) => async (e) => {
      let result = resultDetailKoleksi;
      setLoading(true);
      setKeyword(keyword);
  
      const response = await baseApi.get(`/pdrb-adhk-2010-lap-usaha?page=${page}&idMstCollection=${location?.state?.idMstCollection}&q=${keyword}`);
      if (response?.status === 200) {
        setResultDetailKoleksi(response?.data?.result?.data);
        setCurrentPage(response?.data?.result?.current_page);
        setSizePerPage(response?.data?.result.per_page);
        setFromPage(response?.data?.result?.total);
        setLoading(false);
        setPaginationParameter(response?.data?.result);
      }
    };
  
    function prevPages(hal) {
      const listPages = [];
      listPages.push(
        <PageItem onClick={handleOnTableChange(hal - 1, keyword)}>{hal - 1}</PageItem>
      );
      return listPages;
    }
  
    function nextPages(hal) {
      const listPages = [];
      listPages.push(
        <PageItem onClick={handleOnTableChange(hal + 1, keyword)}>{hal + 1}</PageItem>
      );
      return listPages;
    }

    function formatNumber(number) {
      return number.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, "$&,");
    }
    
    const columns = [
        // {
        //   dataField: "",
        //   text: "No.",
        //   formatter: (cell, row, rowIndex, extraData) => (
        //     <>{page * sizePerPage - sizePerPage + (rowIndex + 1)}</>
        //   ),
        //   headerAlign: "center",
        //   align: "center",
        // },
        {
          dataField: "",
          text: "",
          headerAlign: "center",
          align: "center",
          formatter: (cell, row, rowIndex, extraData) => (
            <div>
              {/* <ButtonGroup size="sm" aria-label="Basic example"> */}
              <OverlayTrigger overlay={<Tooltip>Ubah Data</Tooltip>}>
                <Button
                  className="py-1 px-2"
                  variant="outline-warning"
                  size="sm"
                  onClick={() =>
                    // setOpenUpdateModal(!openUpdateModal, setRow(row?.id))
                    setOpenUpdateModal(
                      !openUpdateModal,
                      setInputData({
                        tahun: row.tahun,
                        target: row.target,
                        realisasi: row.realisasi,
                        kategori: row.kategori,
                        sumber: row.sumber,
                      }),
                      setCatatan(row.catatan),
                      setRow(row?.id)
                    )
                  }
                >
                  <Edit3 size={14} />
                </Button>
              </OverlayTrigger>
    
              <OverlayTrigger
                overlay={
                  <Tooltip>
                    {row.trx_data_latest
                      ? "Data ini tidak bisa dihapus"
                      : "Hapus Data"}
                  </Tooltip>
                }
              >
                <Button
                  disabled={row.trx_data_latest}
                  className="py-1 px-2"
                  variant="outline-danger"
                  size="sm"
                  onClick={() =>
                    setOpenDeleteModal(!openDeleteModal, setRow(row?.id))
                  }
                >
                  {row.trx_data_latest ? <Slash size={14} /> : <Trash2 size={14} />}
                </Button>
              </OverlayTrigger>
    
              {/* </ButtonGroup> */}
            </div>
          ),
        },
        {
          dataField: "tahun",
          text: "Tahun",
          headerAlign: "center",
          sort: true,
          headerSortingClasses,
          formatter: (cell, row, rowIndex, extraData) => (
            <>
              <a
                href="#"
                onClick={() =>
                  // setOpenUpdateModal(!openUpdateModal, setRow(row?.id))
                  setOpenCkeditorModal(!openCkeditorModal, setCatatan(row.catatan))
                }
              >
                {row.tahun}
                {/* <File size={14} /> */}
              </a>
            </>
          ),
        },
        {
            dataField: "kategori",
            text: "Kategori",
            headerAlign: "center",
            sort: true,
            headerSortingClasses,
        },
        {
          dataField: "target",
          text: "Target",
          headerAlign: "center",
          sort: true,
          headerSortingClasses,
        },
        {
          dataField: "realisasi",
          text: "Realisasi",
          headerAlign: "center",
          sort: true,
          headerSortingClasses,
          formatter: (cell, row) => {
            if (location?.state?.satuan === "Jumlah") {
              // const formattedRealisasi = formatNumber(parseFloat(cell));
              // return formattedRealisasi;
              const numericValue = parseFloat(cell);
              const formattedValue = numericValue.toFixed(2);
              const formattedString = formattedValue.replace(/\.?0*$/, '');
              const result = formattedString.replace(/\B(?=(\d{3})+(?!\d))/g, '.');

              return result;

            } else {
              return cell;
            }
          },
        },
        {
          dataField: "sumber",
          text: "Sumber",
          headerAlign: "center",
          sort: true,
          headerSortingClasses,
        },
        {
          dataField: "kemendagri_kota_nama",
          text: "Wilayah",
          headerAlign: "center",
          sort: true,
          headerSortingClasses,
        },
        {
          dataField: "catatan",
          text: "Catatan",
          headerAlign: "center",
          sort: true,
          headerSortingClasses,
          formatter: (cell, row, rowIndex, extraData) => (
            <>{ReactHtmlParser(row.catatan)}</>
          ),
        },
        {
          dataField: "created_at",
          text: "Tanggal Dibuat",
          formatter: (cell, row, rowIndex, extraData) => (
            <>{moment(row.created_at).format("D MMMM YYYY H:mm:ss")}</>
          ),
          headerAlign: "center",
          align: "center",
          sort: true,
          headerSortingClasses,
        },
        {
          dataField: "created_by",
          text: "Dibuat Oleh",
          headerAlign: "center",
          align: "center",
          sort: true,
          headerSortingClasses,
        },
    ];

    /*eslint-disable */
    useEffect(() => {
        let cleanUp = true;
        if (cleanUp) {
            fetchData();
        }

        return () => {
            cleanUp = false;
        };
    }, []);

    return (
        <>
          <Aux>
            <Row>
              <Col>
                <Card
                  title={`${location?.state?.namaCollection}`}
                  subtitle={subtitleCard}
                  isOption
                >

                <Table responsive hover striped>
                  {/* <thead>
                    <tr>
                      <th>First Name</th>
                      <th>Username</th>
                    </tr>
                  </thead> */}
                  <tbody>
                    <tr>
                      <td>Urusan</td>
                      <td>{ location?.state?.urusan }</td>
                    </tr>
                    <tr>
                      <td>PD</td>
                      <td>{ location?.state?.organisasi }</td>
                    </tr>
                    <tr>
                      <td>Satuan</td>
                      <td>{ location?.state?.satuan }</td>
                    </tr>
                    <tr>
                      <td>Definisi</td>
                      <td>{ location?.state?.definisi }</td>
                    </tr>
                    <tr>
                      <td>Rumus Perhitungan</td>
                      <td>{ location?.state?.rumus_perhitungan }</td>
                    </tr>
                    <tr>
                      <td>Cara Memperoleh Data</td>
                      <td>{ location?.state?.cara_memperoleh_data }</td>
                    </tr>
                    <tr>
                      <td>Catatan</td>
                      <td>{ location?.state?.catatan }</td>
                    </tr>
                  </tbody>
                </Table>

                  <div>
                  <Form onSubmit={fetchData}>
                    <InputGroup className="mb-3">
                      <FormControl
                        autoFocus
                        placeholder="Cari disini..."
                        aria-label="Cari disini..."
                        aria-describedby="basic-addon2"
                        type="text"
                        value={keyword}
                        name="keyword"
                        onChange={handleKeyword}
                      />
                      <InputGroup.Append>
                        <Button
                          variant="outline-primary"
                          type="submit"
                        >
                          <Search size={14} className="mr-1" />
                          CARI
                        </Button>
                      </InputGroup.Append>
                    </InputGroup>
                  </Form>
    
                    <OverlayTrigger overlay={<Tooltip>Tambah Data</Tooltip>}>
                      <Button
                        className="py-2 px-3 btn-sync"
                        variant="outline-success"
                        onClick={handleTambahModal}
                      >
                        <PlusCircle size={14} />
                      </Button>
                    </OverlayTrigger>
    
                    <OverlayTrigger overlay={<Tooltip>Download Data</Tooltip>}>
                      <Button
                        className="py-2 px-3 btn-download"
                        variant="outline-primary"
                        onClick={handleExportExcelModal}
                      >
                        <Download size={14} />
                      </Button>
                    </OverlayTrigger>
                    <OverlayTrigger overlay={<Tooltip>Unggah Data</Tooltip>}>
                      <Button
                        className="py-2 px-3 btn-upload"
                        variant="outline-danger"
                        onClick={handleExportExcelModal}
                      >
                        <Upload size={14} />
                      </Button>
                    </OverlayTrigger>
                  </div>
                  <div className="table-responsive mb-3">
                    <BootstrapTable
                      keyField="id"
                      data={resultDetailKoleksi}
                      columns={columns}
                      hover
                      bordered
                      headerClasses="thead-dark"
                      className="table table-responsive"
                      noDataIndication={"Tidak ada data..."}
                    />

                    {resultDetailKoleksi?.length > 0 && (
                      <div className="d-lg-flex">
                        <div className="ml-auto text-center">
                          <Pagination
                            className="pagination justify-content-center"
                            listClassName="justify-content-center"
                          >
                            {paginationParameter.current_page > 1 && (
                              <>
                                <PageItem
                                  onClick={handleOnTableChange(
                                    paginationParameter.current_page - 1, keyword
                                  )}
                                  tabIndex="-1"
                                >
                                  <i className="fa fa-angle-left" />
                                  <span className="sr-only">Previous</span>
                                </PageItem>
                                {prevPages(paginationParameter?.current_page)}
                              </>
                            )}

                            <PageItem
                              className="active"
                              onClick={(e) => e.preventDefault()}
                            >
                              {paginationParameter.current_page}
                            </PageItem>
                            {paginationParameter?.next_page_url && (
                              <>
                                {nextPages(paginationParameter?.current_page)}
                                <PageItem
                                  onClick={handleOnTableChange(
                                    paginationParameter.current_page + 1, keyword
                                  )}
                                >
                                  <i className="fa fa-angle-right" />
                                  <span className="sr-only">Next</span>
                                </PageItem>
                              </>
                            )}
                          </Pagination>
                        </div>
                      </div>
                    )}
                  </div>
    
                </Card>
              </Col>
            </Row>
          </Aux>
    
          {openTambahModal && (
            <Tambah
              openTambahModal={openTambahModal}
              handleTambahModal={handleTambahModal}
              setOpenTambahModal={setOpenTambahModal}
              inputData={inputData}
              setInputData={setInputData}
              handleChangeInputData={handleChangeInputData}
              catatan={catatan}
              setCatatan={setCatatan}
              handleInputCatatan={handleInputCatatan}
              error={error}
              setError={setError}
              loading={loading}
              setLoading={setLoading}
              fetchData={fetchData}
              idMstCollection={location?.state?.idMstCollection}
              satuan={location?.state?.satuan}
            />
          )}
    
          {openUpdateModal && (
            <Ubah
              openUpdateModal={openUpdateModal}
              setOpenUpdateModal={setOpenUpdateModal}
              handleUpdateModal={handleUpdateModal}
              inputData={inputData}
              setInputData={setInputData}
              handleChangeInputData={handleChangeInputData}
              catatan={catatan}
              setCatatan={setCatatan}
              handleInputCatatan={handleInputCatatan}
              error={error}
              setError={setError}
              fetchData={fetchData}
              loading={loading}
              setLoading={setLoading}
              row={row}
              idMstCollection={location?.state?.idMstCollection}
              satuan={location?.state?.satuan}
            />
          )}
    
          {/* {openExportExcelModal && (
            <ExportExcel
              openExportExcelModal={openExportExcelModal}
              handleExportExcelModal={handleExportExcelModal}
              setOpenExportExcelModal={setOpenExportExcelModal}
              inputData={inputData}
              setInputData={setInputData}
              handleChangeInputData={handleChangeInputData}
              // catatan={catatan}
              // setCatatan={setCatatan}
              // handleInputCatatan={handleInputCatatan}
              error={error}
              setError={setError}
              loading={loading}
              setLoading={setLoading}
              // fetchData={fetchData}
              idMstCollection={location?.state?.idMstCollection}
            />
          )} */}
    
          <DeleteModal
            title="Hapus Data Ini?"
            subtitle="Data tidak dapat dikembalikan!"
            sourceIcon={sourceIcon?.alert}
            buttonConfirm="Ya, Hapus"
            buttonCancel="Batal"
            namaState={openDeleteModal}
            namaSetState={setOpenDeleteModal}
            handleForm={handleDeleteDokumen}
            loading={loading}
          />
    
          <CkeditorModal
            title="Catatan"
            value={ReactHtmlParser(catatan)}
            namaState={openCkeditorModal}
            namaSetState={setOpenCkeditorModal}
            buttonCancel="Tutup"
            size="sm" //lg,xl,sm,md
          />
        </>
    );
}

export default Home