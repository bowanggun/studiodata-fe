import React, { useRef, useEffect, useState } from "react";
import { Modal, Form, Button } from "react-bootstrap";
import { PlusCircle, XCircle } from "react-feather";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import baseApi from "../../../services/baseApi";
import { toast } from "react-toastify";
import Loader from "react-loader-spinner";
import SelectCustom from "../../../App/components/SelectCustom";

const Tambah = (props) => {
  const {
    openTambahModal,
    setOpenTambahModal,
    handleTambahModal,
    inputData,
    setInputData,
    handleChangeInputData,
    error,
    setError,
    catatan,
    setCatatan,
    handleInputCatatan,
    loading,
    setLoading,
    fetchData,
    idMstCollection,
    satuan,
  } = props;

  const [dataWilayah, setDataWilayah] = useState([]);
  const [valueDataWilayah, setValueDataWilayah] = useState("");
  const innerRef = useRef();

  const fetchDataWilayah = (inputValue) => {
    setLoading(true);
    baseApi
      .get(`/wilayah?q=${inputValue}&paging=false`)
      .then((response) => {
        if (response?.status === 200) {
          setDataWilayah(response?.data?.result);
          setLoading(false);
        }
      })
      .catch((error) => {
        setLoading(false);
        toast.warning("Terjadi kesalahan", {
          position: toast.POSITION.TOP_RIGHT,
        });
        // console.log(error);
      });
  };

  const handleInputDetailKoleksi = (e) => {
    e.preventDefault();
    setLoading(true);

    const formData = new FormData();

    formData.append("tahun", inputData.tahun ? inputData.tahun : "");
    formData.append("target",inputData.target ? inputData.target : "");
    formData.append("realisasi",inputData.realisasi ? inputData.realisasi : "");
    formData.append("sumber",inputData.sumber ? inputData.sumber : "");
    formData.append("mst_collection_id", idMstCollection);
    formData.append("catatan", catatan);
    formData.append("kemendagri_kota_kode", "32.71");
    formData.append("kemendagri_kota_nama", "KOTA BOGOR");
    // formData.append("kemendagri_kota_kode", dataWilayah[0]?.kemendagri_kota_kode ? dataWilayah[0]?.kemendagri_kota_kode : "");
    // formData.append("kemendagri_kota_nama", dataWilayah[0]?.kemendagri_kota_nama ? dataWilayah[0]?.kemendagri_kota_nama : "");

    baseApi
      .post(`/trx-collection`, formData)
      .then((response) => {
        if (response?.status === 200) {
          fetchData();
          toast.success(response?.data?.message, {
            position: toast.POSITION.TOP_RIGHT,
          });
          setOpenTambahModal(false);
          setCatatan("");
          setInputData({});
          setLoading(false);
        } else {
          // console.log("response", response);
          setLoading(false);
        }
      })
      .catch((error) => {
        setLoading(false);
        setError(error.response?.data?.errors);

        toast.error(error.response?.data?.message, {
          position: toast.POSITION.TOP_RIGHT,
        });
      });
  };

  useEffect(() => {
    setTimeout(() => {
      innerRef.current.focus();
      setCatatan("");
      setInputData({});
      fetchDataWilayah();
    }, 1);
  }, [setCatatan, setInputData]);

  return (
    <>
      <Modal
        show={openTambahModal}
        onHide={handleTambahModal}
        backdrop="static"
        keyboard={false}
      >
        <Form onSubmit={handleInputDetailKoleksi}>
          <Modal.Header>
            <Modal.Title>Tambah Data</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form.Group className="mb-3 form-label-group">
              <Form.Control
                placeholder="satuan"
                className="form-control"
                type="text"
                id="satuan"
                name="satuan"
                aria-describedby="passwordHelpBlock"
                value={satuan}
                // onChange={handleChangeInputData}
                readOnly
              />
              <Form.Label htmlFor="tahun">Satuan</Form.Label>
              {/* <span className="text-danger">
                <i>{error?.tahun ?? ""}</i>
              </span> */}
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label htmlFor="kemendagri_kota_nama">Wilayah</Form.Label>
              <SelectCustom
                data={dataWilayah}
                setError={setError}
                setValueData={setValueDataWilayah}
                fetchData={fetchDataWilayah}
                labelName="dataWilayah"
                // isMulti={false}
              />

              <span className="text-danger">
                <i>{error?.kemendagri_kota_nama ?? ""}</i>
              </span>
            </Form.Group>

            <Form.Group className="mb-3 form-label-group">
              <Form.Control
                ref={innerRef}
                placeholder="tahun"
                className="form-control"
                type="text"
                id="tahun"
                name="tahun"
                aria-describedby="passwordHelpBlock"
                value={inputData.tahun}
                onChange={handleChangeInputData}
                maxLength={4}
              />
              <Form.Label htmlFor="tahun">Tahun</Form.Label>
              <span className="text-danger">
                <i>{error?.tahun ?? ""}</i>
              </span>
            </Form.Group>

            <Form.Group className="mb-3 form-label-group">
              <Form.Control
                placeholder="target"
                className="form-control"
                type="text"
                id="target"
                name="target"
                aria-describedby="passwordHelpBlock"
                value={inputData.target}
                onChange={handleChangeInputData}
              />
              <Form.Label htmlFor="target">Target</Form.Label>
              <span className="text-danger">
                <i>{error?.target ?? ""}</i>
              </span>
            </Form.Group>

            <div className="mb-3">
              <strong>Pratinjau: </strong>
              {inputData.target ? (
                parseFloat(inputData.target.replace(",", ".")).toLocaleString("id-ID", {
                  minimumFractionDigits: 3,
                  maximumFractionDigits: 3,
                })
              ) : (
                "Hanya untuk angka"
              )}
            </div>

            <Form.Group className="mb-3 form-label-group">
              <Form.Control
                placeholder="realisasi"
                className="form-control"
                type="text"
                id="realisasi"
                name="realisasi"
                aria-describedby="passwordHelpBlock"
                value={inputData.realisasi}
                onChange={handleChangeInputData}
              />
              <Form.Label htmlFor="realisasi">Realisasi</Form.Label>
              <span className="text-danger">
                <i>{error?.realisasi ?? ""}</i>
              </span>
            </Form.Group>

            <div className="mb-3">
              <strong>Pratinjau: </strong>
              {inputData.realisasi ? (
                parseFloat(inputData.realisasi.replace(",", ".")).toLocaleString("id-ID", {
                  minimumFractionDigits: 3,
                  maximumFractionDigits: 3,
                })
              ) : (
                "Hanya untuk angka"
              )}
            </div>

            <Form.Group className="mb-3 form-label-group">
              <Form.Control
                placeholder="Sumber Data"
                className="form-control"
                type="text"
                id="sumber"
                name="sumber"
                aria-describedby="passwordHelpBlock"
                value={inputData.sumber}
                onChange={handleChangeInputData}
              />
              <Form.Label htmlFor="sumber">Sumber Data</Form.Label>
              <span className="text-danger">
                <i>{error?.sumber ?? ""}</i>
              </span>
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label htmlFor="catatan">Catatan</Form.Label>
              <CKEditor
                editor={ClassicEditor}
                data={catatan}
                onChange={handleInputCatatan}
                onReady={(editor) => {
                  editor.editing.view.change((writer) => {
                    writer.setStyle(
                      "height",
                      "200px",
                      editor.editing.view.document.getRoot()
                    );
                  });
                }}
              />
              <span className="text-danger">
                <i>{error?.catatan ?? ""}</i>
              </span>
            </Form.Group>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="primary" type="submit" disabled={loading}>
              {loading ? (
                <Loader type="ThreeDots" color="#fff" height={10} />
              ) : (
                <>
                  <PlusCircle size={14} className="mr-1" /> Simpan
                </>
              )}
            </Button>
            <Button variant="secondary" onClick={handleTambahModal}>
              <XCircle size={14} className="mr-1" />
              Tutup
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
    </>
  );
}

export default Tambah