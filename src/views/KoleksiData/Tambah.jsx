import React, { useRef, useEffect, useState } from "react";
import { Modal, Form, Button } from "react-bootstrap";
import { PlusCircle, XCircle } from "react-feather";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import baseApi from "../../services/baseApi";
import { toast } from "react-toastify";
import Loader from "react-loader-spinner";
import SelectCustom from "../../App/components/SelectCustom";

const Tambah = (props) => {
  const {
    openTambahModal,
    setOpenTambahModal,
    handleTambahModal,
    inputData,
    setInputData,
    handleChangeInputData,
    error,
    setError,
    catatan,
    setCatatan,
    handleInputCatatan,
    definisi,
    setDefinisi,
    handleInputDefinisi,
    rumusPerhitungan,
    setRumusPerhitungan,
    handleInputRumusPerhitungan,
    caraMemperolehData,
    setCaraMemperolehData,
    handleInputCaraMemperolehData,
    isMultidimensi,
    handleChangeInputDataIsMultidimensi,
    loading,
    setLoading,
    fetchData,
  } = props;

  const [dataOrganisasi, setDataOrganisasi] = useState([]);
  const [valueDataOrganisasi, setValueDataOrganisasi] = useState("");
  // const [resultOrganisasi, setResultOrganisasi] = useState([]);
  const [resultTipeCollection, setTipeCollection] = useState([]);
  const [resultDefaultChart, setDefaultChart] = useState([]);

  const innerRef = useRef();

  const fetchDataOrganisasi = (inputValue) => {
    setLoading(true);
    baseApi
      .get(`/organisasi?q=${inputValue}&paging=false`)
      .then((response) => {
        if (response?.status === 200) {
          setDataOrganisasi(response?.data?.result);
          setLoading(false);
        }
      })
      .catch((error) => {
        setLoading(false);
        toast.warning("Terjadi kesalahan", {
          position: toast.POSITION.TOP_RIGHT,
        });
        // console.log(error);
      });
  };

  const fetchDataTipeCollection = () => {
    baseApi
      .get(`/tipe-collection`)
      .then((response) => {
        if (response?.status === 200) {
          setTipeCollection(response?.data?.result);
          setLoading(false);
        }
      })
      .catch((error) => {
        setLoading(false);
        toast.warning("Terjadi kesalahan", {
          position: toast.POSITION.TOP_RIGHT,
        });
        // console.log(error);
      });
  };

  const fetchDataDefaultChart = () => {
    baseApi
      .get(`/default-chart`)
      .then((response) => {
        if (response?.status === 200) {
          setDefaultChart(response?.data?.result);
          setLoading(false);
        }
      })
      .catch((error) => {
        setLoading(false);
        toast.warning("Terjadi kesalahan", {
          position: toast.POSITION.TOP_RIGHT,
        });
        // console.log(error);
      });
  };

  // console.log("ttttt", dataOrganisasi[0]?.id);
  // console.log("organisasi", dataOrganisasi[0]?.organisasi);

  const handleInputKoleksiData = (e) => {
    e.preventDefault();
    setLoading(true);

    const formData = new FormData();

    formData.append(
      "nama_collection",
      inputData.namaCollection ? inputData.namaCollection : ""
    );
    // formData.append("judul", inputData.judul ? inputData.judul : "");
    // formData.append(
    //   "ref_organisasi_id",
    //   inputData.ref_organisasi_id ? inputData.ref_organisasi_id : ""
    // );
    // formData.append(
    //   "organisasi",
    //   inputData.organisasi ? inputData.organisasi : ""
    // );

    formData.append("ref_organisasi_id", dataOrganisasi[0]?.id ? dataOrganisasi[0]?.id : "");
    formData.append("organisasi", dataOrganisasi[0]?.organisasi ? dataOrganisasi[0]?.organisasi : "");

    formData.append("satuan", inputData.satuan ? inputData.satuan : "");
    // formData.append("tipe", inputData.tipe ? inputData.tipe : "");
    // formData.append(
    //   "default_chart",
    //   inputData.defaultChart ? inputData.defaultChart : ""
    // );
    // formData.append(
    //   "referensi_data",
    //   inputData.referensiData ? inputData.referensiData : ""
    // );
    formData.append(
      "route_name",
      inputData.routeName ? inputData.routeName : ""
    );
    formData.append(
      "table_name",
      inputData.tableName ? inputData.tableName : ""
    );
    formData.append("catatan", catatan);
    formData.append("definisi", definisi);
    formData.append("rumus_perhitungan", rumusPerhitungan);
    formData.append("cara_memperoleh_data", caraMemperolehData);
    formData.append("urusan", inputData.urusan ? inputData.urusan : "");
    formData.append("is_multidimensi", isMultidimensi);

    baseApi
      .post(`/mst-collection`, formData)
      .then((response) => {
        if (response?.status === 200) {
          fetchData();
          toast.success(response?.data?.message, {
            position: toast.POSITION.TOP_RIGHT,
          });
          setOpenTambahModal(false);
          setCatatan("");
          setDefinisi("");
          setRumusPerhitungan("");
          setCaraMemperolehData("");
          // setUrusan("");
          setInputData({});
          setLoading(false);
        } else {
          setLoading(false);
          // console.log("response", response);
        }
      })
      .catch((error) => {
        setLoading(false);
        setError(error.response?.data?.errors);

        toast.error(error.response?.data?.message, {
          position: toast.POSITION.TOP_RIGHT,
        });
      });
  };

  const getSingkatan = () => {
    return dataOrganisasi?.map(item => {
      return item?.singkatan
    })
  }

  /*eslint-disable */
  useEffect(() => {
    setTimeout(() => {
      innerRef.current.focus();
      setCatatan("");
      setDefinisi("");
      setRumusPerhitungan("");
      setCaraMemperolehData("");
      setInputData({});
      fetchDataTipeCollection();
      fetchDataDefaultChart();
      fetchDataOrganisasi();
    }, 1);
  }, [setCatatan, setInputData]);

  return (
    <>
      <Modal
        show={openTambahModal}
        onHide={handleTambahModal}
        backdrop="static"
        keyboard={false}
        size="lg"
      >
        <Form onSubmit={handleInputKoleksiData}>
          <Modal.Header>
            <Modal.Title>Tambah Koleksi Data</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form.Group className="mb-3 form-label-group">
              <Form.Control
                ref={innerRef}
                placeholder="Nama Data"
                className="form-control"
                type="text"
                id="namaCollection"
                name="namaCollection"
                aria-describedby="passwordHelpBlock"
                value={inputData.namaCollection}
                onChange={handleChangeInputData}
              />
              <Form.Label htmlFor="namaCollection">Nama Data</Form.Label>
              <span className="text-danger">
                <i>{error?.nama_collection ?? ""}</i>
              </span>
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label htmlFor="definisi">Definisi</Form.Label>
              <CKEditor
                editor={ClassicEditor}
                data={definisi}
                onChange={handleInputDefinisi}
                onReady={(editor) => {
                  editor.editing.view.change((writer) => {
                    writer.setStyle(
                      "height",
                      "200px",
                      editor.editing.view.document.getRoot()
                    );
                  });
                }}
              />

              <span className="text-danger">
                <i>{error?.definisi ?? ""}</i>
              </span>
            </Form.Group>

            {/* <Form.Group className="mb-3 form-label-group">
              <Form.Control
                placeholder="Judul"
                className="form-control"
                type="text"
                id="judul"
                name="judul"
                aria-describedby="passwordHelpBlock"
                value={inputData.judul}
                onChange={handleChangeInputData}
              />
              <Form.Label htmlFor="judul">Judul</Form.Label>
              <span className="text-danger">
                <i>{error.judul ?? ""}</i>
              </span>
            </Form.Group> */}

            <Form.Group className="mb-3 form-label-group">
              <Form.Control
                placeholder="Satuan"
                className="form-control"
                type="text"
                id="satuan"
                name="satuan"
                aria-describedby="passwordHelpBlock"
                value={inputData.satuan}
                onChange={handleChangeInputData}
              />
              <Form.Label htmlFor="satuan">Satuan</Form.Label>
              <span className="text-danger">
                <i>{error?.satuan ?? ""}</i>
              </span>
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label htmlFor="rumusPerhitungan">Rumus Perhitungan</Form.Label>
              <CKEditor
                editor={ClassicEditor}
                data={rumusPerhitungan}
                onChange={handleInputRumusPerhitungan}
                onReady={(editor) => {
                  editor.editing.view.change((writer) => {
                    writer.setStyle(
                      "height",
                      "200px",
                      editor.editing.view.document.getRoot()
                    );
                  });
                }}
              />

              <span className="text-danger">
                <i>{error?.rumus_perhitungan ?? ""}</i>
              </span>
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label htmlFor="ref_organisasi_id">Organisasi</Form.Label>
              {/* <Form.Control
                as="select"
                id="ref_organisasi_id"
                name="ref_organisasi_id"
                value={inputData.ref_organisasi_id}
                onChange={handleChangeInputData}
                style={{ cursor: "pointer" }}
              >
                <option selected value="">
                  -
                </option>
                {resultOrganisasi?.map((item, index) => (
                  <option key={index} value={item.id}>
                    {item?.organisasi}
                  </option>
                ))}
              </Form.Control> */}

              <SelectCustom
                data={dataOrganisasi}
                setError={setError}
                setValueData={setValueDataOrganisasi}
                fetchData={fetchDataOrganisasi}
                labelName="dataOrganisasi"
                // isMulti={false}
                getSingkatan={getSingkatan}
              />

              <span className="text-danger">
                <i>{error?.ref_organisasi_id ?? ""}</i>
              </span>
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label htmlFor="caraMemperolehData">Cara Memperoleh Data</Form.Label>
              <CKEditor
                editor={ClassicEditor}
                data={caraMemperolehData}
                onChange={handleInputCaraMemperolehData}
                onReady={(editor) => {
                  editor.editing.view.change((writer) => {
                    writer.setStyle(
                      "height",
                      "200px",
                      editor.editing.view.document.getRoot()
                    );
                  });
                }}
              />

              <span className="text-danger">
                <i>{error?.cara_memperoleh_data ?? ""}</i>
              </span>
            </Form.Group>

            <Form.Group className="mb-3 form-label-group">
              <Form.Control
                placeholder="Urusan"
                className="form-control"
                type="text"
                id="urusan"
                name="urusan"
                aria-describedby="passwordHelpBlock"
                value={inputData.urusan}
                onChange={handleChangeInputData}
              />
              <Form.Label htmlFor="urusan">Urusan</Form.Label>
              <span className="text-danger">
                <i>{error?.urusan ?? ""}</i>
              </span>
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label htmlFor="keterangan">Catatan</Form.Label>
              <CKEditor
                editor={ClassicEditor}
                data={catatan}
                onChange={handleInputCatatan}
                onReady={(editor) => {
                  editor.editing.view.change((writer) => {
                    writer.setStyle(
                      "height",
                      "200px",
                      editor.editing.view.document.getRoot()
                    );
                  });
                }}
              />

              <span className="text-danger">
                <i>{error?.catatan ?? ""}</i>
              </span>
            </Form.Group>

            {/* <Form.Group className="mb-3">
              <Form.Label htmlFor="tipe">Tipe</Form.Label>
              <Form.Control
                as="select"
                id="tipe"
                name="tipe"
                value={inputData.tipe}
                onChange={handleChangeInputData}
                style={{ cursor: "pointer" }}
              >
                <option selected value="">
                  -
                </option>
                {resultTipeCollection?.map((item, index) => (
                  <option key={index} value={item.tipe}>
                    {item?.tipe}
                  </option>
                ))}
              </Form.Control>
              <span className="text-danger">
                <i>{error?.tipe ?? ""}</i>
              </span>
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label htmlFor="defaultChart">Default Chart</Form.Label>
              <Form.Control
                as="select"
                id="defaultChart"
                name="defaultChart"
                value={inputData.defaultChart}
                onChange={handleChangeInputData}
                style={{ cursor: "pointer" }}
              >
                <option selected value="">
                  -
                </option>
                {resultDefaultChart?.map((item, index) => (
                  <option key={index} value={item.chart}>
                    {item?.chart}
                  </option>
                ))}
              </Form.Control>
              <span className="text-danger">
                <i>{error?.default_chart ?? ""}</i>
              </span>
            </Form.Group>

            <Form.Group className="mb-3 form-label-group">
              <Form.Control
                placeholder="Referensi Data"
                className="form-control"
                type="text"
                id="referensiData"
                name="referensiData"
                aria-describedby="passwordHelpBlock"
                value={inputData.referensiData}
                onChange={handleChangeInputData}
              />
              <Form.Label htmlFor="referensiData">Referensi Data</Form.Label>
              <span className="text-danger">
                <i>{error.referensi_data ?? ""}</i>
              </span>
            </Form.Group> */}

            <Form.Group className="mb-3">
              <Form.Label htmlFor="is_multidimensi">Multidimensional?</Form.Label>
              <div key={`inline-radio`} className="mb-3">
                <Form.Check
                  inline
                  label="Ya"
                  name="is_multidimensi"
                  type="radio"
                  id={`is_multidimensi_true`}
                  value={isMultidimensi}
                  onChange={handleChangeInputDataIsMultidimensi}
                  defaultChecked={isMultidimensi === 1}
                />
                <Form.Check
                  inline
                  label="Tidak"
                  name="is_multidimensi"
                  type="radio"
                  id={`is_hidden_false`}
                  value={isMultidimensi}
                  onChange={handleChangeInputDataIsMultidimensi}
                  defaultChecked={isMultidimensi === 0}
                />
              </div>
              <span className="text-danger">
                <i>{error?.is_multidimensi ?? ""}</i>
              </span>
            </Form.Group>

            <Form.Group className="mb-3 form-label-group">
              <Form.Control
                placeholder="Route Name"
                className="form-control"
                type="text"
                id="routeName"
                name="routeName"
                aria-describedby="passwordHelpBlock"
                value={inputData.routeName}
                onChange={handleChangeInputData}
              />
              <Form.Label htmlFor="routeName">Route Name</Form.Label>
              <span className="text-danger">
                <i>{error?.route_name ?? ""}</i>
              </span>
            </Form.Group>

            <Form.Group className="mb-3 form-label-group">
              <Form.Control
                placeholder="Table Name"
                className="form-control"
                type="text"
                id="tableName"
                name="tableName"
                aria-describedby="passwordHelpBlock"
                value={inputData.tableName}
                onChange={handleChangeInputData}
              />
              <Form.Label htmlFor="tableName">Table Name</Form.Label>
              <span className="text-danger">
                <i>{error?.table_name ?? ""}</i>
              </span>
            </Form.Group>

          </Modal.Body>
          <Modal.Footer>
            <Button variant="primary" type="submit" disabled={loading}>
              {loading ? (
                <Loader type="ThreeDots" color="#fff" height={10} />
              ) : (
                <>
                  <PlusCircle size={14} className="mr-1" /> Simpan
                </>
              )}
            </Button>
            <Button variant="secondary" onClick={handleTambahModal}>
              <XCircle size={14} className="mr-1" />
              Tutup
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
    </>
  );
};

export default Tambah;
