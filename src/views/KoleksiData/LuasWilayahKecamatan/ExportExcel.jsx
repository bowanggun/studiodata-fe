import React, { useRef, useEffect } from "react";
import { Modal, Form, Button } from "react-bootstrap";
import { Download, PlusCircle, XCircle } from "react-feather";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import baseApi from "../../../services/baseApi";
import { toast } from "react-toastify";
import Loader from "react-loader-spinner";
import moment from "moment";

const Tambah = (props) => {
  const {
    openExportExcelModal,
    setOpenExportExcelModal,
    handleExportExcelModal,
    inputData,
    setInputData,
    handleChangeInputData,
    error,
    setError,
    // catatan,
    // setCatatan,
    // handleInputCatatan,
    loading,
    setLoading,
    // fetchData,
    idMstCollection,
  } = props;

  const innerRef = useRef();

  const handleExportLuasWilayah = (e) => {
    e.preventDefault();
    setLoading(true);

    const formData = new FormData();

    formData.append("mst_collection_id", idMstCollection);
    formData.append("tahun", inputData.tahun ? inputData.tahun : "");

    baseApi
      .post(`/export-luas-wilayah-kecamatan-kelurahan`, formData, {
        method: "GET",
        responseType: "blob",
      })
      .then((response) => {
        if (response?.status === 200) {
          const url = window.URL.createObjectURL(new Blob([response.data]));
          const link = document.createElement("a");
          link.href = url;
          link.setAttribute(
            "download",
            `Luas-wilayah_${moment(new Date()).format(
              "DD-MM-YYYY_H-mm-ss"
            )}.xlsx`
          );
          document.body.appendChild(link);
          link.click();
          setLoading(false);
          setOpenExportExcelModal(false);
        } else {
          setLoading(false);
        }
      })
      .catch((error) => {
        setError(error.response?.data?.errors);

        toast.error("Gagal Download Data", {
          position: toast.POSITION.TOP_RIGHT,
        });
        setLoading(false);
      });
  };

  useEffect(() => {
    setTimeout(() => {
      innerRef.current.focus();
      //   setCatatan("");
      setInputData({});
    }, 1);
  }, [setInputData]);

  return (
    <>
      <Modal
        show={openExportExcelModal}
        onHide={handleExportExcelModal}
        backdrop="static"
        keyboard={false}
      >
        <Form onSubmit={handleExportLuasWilayah}>
          <Modal.Header>
            <Modal.Title>Download Luas Wilayah</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form.Group className="mb-3 form-label-group">
              <Form.Control
                ref={innerRef}
                placeholder="Tahun"
                className="form-control"
                type="text"
                id="tahun"
                name="tahun"
                aria-describedby="passwordHelpBlock"
                value={inputData.tahun}
                onChange={handleChangeInputData}
              />
              <Form.Label htmlFor="tahun">Tahun</Form.Label>
              <span className="text-danger">
                <i>{error?.tahun ?? ""}</i>
              </span>
            </Form.Group>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="primary" type="submit" disabled={loading}>
              {loading ? (
                <Loader type="ThreeDots" color="#fff" height={10} />
              ) : (
                <>
                  <Download size={14} className="mr-1" /> Download
                </>
              )}
            </Button>
            <Button variant="secondary" onClick={handleExportExcelModal}>
              <XCircle size={14} className="mr-1" />
              Tutup
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
    </>
  );
};

export default Tambah;
