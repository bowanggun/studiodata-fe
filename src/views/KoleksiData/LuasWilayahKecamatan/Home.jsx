import React, { useEffect, useState } from "react";
import Aux from "../../../hoc/_Aux";
import {
  Button,
  Col,
  FormControl,
  InputGroup,
  Row,
  OverlayTrigger,
  Tooltip,
} from "react-bootstrap";
import Card from "../../../App/components/MainCard";
import BarChart from "../../../App/charts/BarChart";

// import BootstrapTable from "react-bootstrap-table-next";
// import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
// import paginationFactory from "react-bootstrap-table2-paginator";
import moment from "moment";
import { RemoteTable } from "../../../App/components/RemoteTable";
import { toast } from "react-toastify";
// import Modal from "react-bootstrap/Modal";
import DeleteModal from "../../../App/components/DeleteModal";
import CkeditorModal from "../../../App/components/CkeditorModal";
// import ButtonGroup from "react-bootstrap/ButtonGroup";
import {
  Edit3,
  Trash2,
  PlusCircle,
  XCircle,
  Search,
  ArrowLeft,
  Slash,
  Download,
  Upload,
} from "react-feather";
import { sourceIcon } from "../../../App/components/Icon";

import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
import "react-bootstrap-table2-toolkit/dist/react-bootstrap-table2-toolkit.min.css";
import "react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css";
import { Link, useLocation, useHistory } from "react-router-dom";
import baseApi from "../../../services/baseApi";
import Tambah from "./Tambah";
import Ubah from "./Ubah";
import ExportExcel from "./ExportExcel";
import ReactHtmlParser from "react-html-parser";
import PieChart from "../../../App/charts/PieChart";
import LineChart from "../../../App/charts/LineChart";

const Home = () => {
  // console.log(
  //   "generateRandomHexColorCombination(6, 0.3)",
  //   generateRandomHexColorCombination(6, 0.3)
  // );
  const id = require("moment/locale/id");
  moment.updateLocale("id", id);

  const location = useLocation();
  const history = useHistory();

  const subtitleCard = (
    <OverlayTrigger overlay={<Tooltip>Kembali ke halaman sebelumnya</Tooltip>}>
      <Button
        variant="outline-secondary"
        size="sm"
        className="py-1 px-2"
        onClick={history.goBack}
      >
        <ArrowLeft size={16} />
      </Button>
    </OverlayTrigger>
  );

  // console.log(location);

  const [resultLuasWilayahKecamatan, setResultLuasWilayahKecamatan] = useState(
    []
  );
  const [loading, setLoading] = useState(false);
  const [page, setPage] = useState(1);
  const [currentPage, setCurrentPage] = useState(1);
  const [totalRows, setTotalRows] = useState(0);
  const [sizePerPage, setSizePerPage] = useState(10);
  const [fromPage, setFromPage] = useState(1);
  const [keyword, setKeyword] = useState("");
  const [openTambahModal, setOpenTambahModal] = useState(false);
  const [openUpdateModal, setOpenUpdateModal] = useState(false);
  const [openExportExcelModal, setOpenExportExcelModal] = useState(false);
  const [openDeleteModal, setOpenDeleteModal] = useState(false);
  const [openCkeditorModal, setOpenCkeditorModal] = useState(false);
  const [inputData, setInputData] = useState({
    kecamatan: "",
    jmlKelurahan: "",
    luasWilayah: "",
    satuan: "",
  });
  const [catatan, setCatatan] = useState("");
  const [error, setError] = useState([]);
  const [row, setRow] = useState("");
  const [labelsChart, setLabelsChart] = useState("");
  const [datasetChart, setDatasetChart] = useState("");

  const headerSortingClasses = (column, sortOrder, isLastSorting, colIndex) =>
    sortOrder === "asc" ? "bg-success" : "bg-info";

  const fetchData = async () => {
    // if (location.state === undefined) {
    //   history.push("/grup-dokumen");
    // }

    try {
      let apiDokumen = "";
      if (keyword === "" || keyword === undefined || keyword === null) {
        apiDokumen = `/luas-wilayah-kecamatan-kelurahan?idMstCollection=${location.state.idMstCollection}`;
      } else {
        apiDokumen = `/luas-wilayah-kecamatan-kelurahan?idMstCollection=${location.state.idMstCollection}&q=${keyword}`;
      }

      const response = await baseApi.get(apiDokumen);
      if (response?.status === 200) {
        setResultLuasWilayahKecamatan(response?.data?.result?.data);
        setTotalRows(response?.data?.result?.total);
        // setFromPage(response?.data?.result?.from);

        if (keyword !== "" && keyword !== undefined && keyword !== null) {
          setLabelsChart(
            response?.data?.result?.data.map((data) => data.kecamatan)
          );
          setDatasetChart(
            response?.data?.result?.data.map((data) => data.luas_wilayah)
          );
        }
      }
    } catch (error) {
      history.push("/koleksi-data");
      toast.warning("Terjadi kesalahan", {
        position: toast.POSITION.TOP_RIGHT,
      });
      // console.log(error);
    }
  };

  const handleInputCatatan = (e, editor) => {
    const data = editor.getData();
    setCatatan(data);
  };

  const handleChangeInputData = (e) => {
    e.persist();
    setInputData({ ...inputData, [e.target.name]: e.target.value });
  };

  const handleDeleteDokumen = (e) => {
    e.preventDefault();
    setLoading(true);

    baseApi
      .delete(`/luas-wilayah-kecamatan-kelurahan/${row}`)
      .then((response) => {
        if (response?.status === 200) {
          toast.info(response?.data?.message, {
            position: toast.POSITION.TOP_RIGHT,
          });

          setLoading(false);
          fetchData();
          setOpenDeleteModal(false);
        }
      })
      .catch((err) => {
        toast.error(err.response?.data?.message, {
          position: toast.POSITION.TOP_RIGHT,
        });

        setLoading(false);
      });
  };

  const handleKeyword = (e) => {
    setKeyword(e.target.value);
  };

  const handleTambahModal = () => {
    setOpenTambahModal(!openTambahModal);
    setInputData({
      kecamatan: "",
      jmlKelurahan: "",
      luasWilayah: "",
      satuan: "",
    });
    setError([]);
  };

  const handleExportExcelModal = () => {
    setOpenExportExcelModal(!openExportExcelModal);
    setInputData({
      tahun: "",
    });
    setError([]);
  };

  const handleUpdateModal = () => {
    setOpenUpdateModal(!openUpdateModal);
    setError([]);
  };

  const handleOnTableChange = (
    type,
    { page, sizePerPage, sortField, sortOrder }
  ) => {
    let result = resultLuasWilayahKecamatan;
    setLoading(true);

    // Handle column sort
    if (sortOrder === "asc") {
      result = result.sort((a, b) => {
        if (a[sortField] > b[sortField]) {
          return 1;
        } else if (b[sortField] > a[sortField]) {
          return -1;
        }
        return 0;
      });
    } else {
      result = result.sort((a, b) => {
        if (a[sortField] > b[sortField]) {
          return -1;
        } else if (b[sortField] > a[sortField]) {
          return 1;
        }
        return 0;
      });
    }

    const response = baseApi.get(
      `/luas-wilayah-kecamatan-kelurahan?page=${page}`
    );
    if (response?.status === 200) {
      setResultLuasWilayahKecamatan(response?.data?.result?.data);
      setPage(page);
      setCurrentPage(response?.data?.result?.current_page);
      setSizePerPage(sizePerPage);
      setFromPage(response?.data?.result?.total);
      setLoading(false);
    }
  };

  const columns = [
    // {
    //   dataField: "",
    //   text: "No.",
    //   formatter: (cell, row, rowIndex, extraData) => (
    //     <>{page * sizePerPage - sizePerPage + (rowIndex + 1)}</>
    //   ),
    //   headerAlign: "center",
    //   align: "center",
    // },
    {
      dataField: "",
      text: "",
      headerAlign: "center",
      align: "center",
      formatter: (cell, row, rowIndex, extraData) => (
        <div>
          {/* <ButtonGroup size="sm" aria-label="Basic example"> */}
          <OverlayTrigger overlay={<Tooltip>Ubah Data</Tooltip>}>
            <Button
              className="py-1 px-2"
              variant="outline-warning"
              size="sm"
              onClick={() =>
                // setOpenUpdateModal(!openUpdateModal, setRow(row?.id))
                setOpenUpdateModal(
                  !openUpdateModal,
                  setInputData({
                    jenisDokumen: row.jenis_dokumen,
                    kecamatan: row.kecamatan,
                    jmlKelurahan: row.jml_kelurahan,
                    luasWilayah: row.luas_wilayah,
                    satuan: row.satuan,
                  }),
                  setCatatan(row.catatan),
                  setRow(row?.id)
                )
              }
            >
              <Edit3 size={14} />
            </Button>
          </OverlayTrigger>

          <OverlayTrigger
            overlay={
              <Tooltip>
                {row.trx_data_latest
                  ? "Data ini tidak bisa dihapus"
                  : "Hapus Data"}
              </Tooltip>
            }
          >
            <Button
              disabled={row.trx_data_latest}
              className="py-1 px-2"
              variant="outline-danger"
              size="sm"
              onClick={() =>
                setOpenDeleteModal(!openDeleteModal, setRow(row?.id))
              }
            >
              {row.trx_data_latest ? <Slash size={14} /> : <Trash2 size={14} />}
            </Button>
          </OverlayTrigger>

          {/* </ButtonGroup> */}
        </div>
      ),
    },
    {
      dataField: "kecamatan",
      text: "Kecamatan",
      headerAlign: "center",
      sort: true,
      headerSortingClasses,
      formatter: (cell, row, rowIndex, extraData) => (
        <>
          <a
            href="#"
            onClick={() =>
              // setOpenUpdateModal(!openUpdateModal, setRow(row?.id))
              setOpenCkeditorModal(!openCkeditorModal, setCatatan(row.catatan))
            }
          >
            {row.kecamatan}
            {/* <File size={14} /> */}
          </a>
        </>
      ),
    },
    {
      dataField: "jml_kelurahan",
      text: "Jumlah Kelurahan",
      headerAlign: "center",
      sort: true,
      headerSortingClasses,
    },
    {
      dataField: "luas_wilayah",
      text: "Luas Wilayah",
      headerAlign: "center",
      sort: true,
      headerSortingClasses,
    },
    {
      dataField: "satuan",
      text: "Satuan",
      headerAlign: "center",
      sort: true,
      headerSortingClasses,
    },
    {
      dataField: "tahun",
      text: "Tahun",
      headerAlign: "center",
      sort: true,
      headerSortingClasses,
    },
    {
      dataField: "created_at",
      text: "Tanggal Dibuat",
      formatter: (cell, row, rowIndex, extraData) => (
        <>{moment(row.created_at).format("D MMMM YYYY H:mm:ss")}</>
      ),
      headerAlign: "center",
      align: "center",
      sort: true,
      headerSortingClasses,
    },
    {
      dataField: "created_by",
      text: "Dibuat Oleh",
      headerAlign: "center",
      align: "center",
      sort: true,
      headerSortingClasses,
    },
  ];

  /*eslint-disable */
  useEffect(() => {
    let cleanUp = true;
    if (cleanUp) {
      fetchData();
    }

    return () => {
      cleanUp = false;
    };
  }, []);

  // console.log("res", resultGrupDokumen);

  return (
    <>
      <Aux>
        <Row>
          <Col>
            <Card
              title={`${location?.state?.namaCollection}`}
              subtitle={subtitleCard}
              isOption
            >
              <div>
                <InputGroup className="mb-3">
                  <FormControl
                    autoFocus
                    placeholder="Cari disini..."
                    aria-label="Cari disini..."
                    aria-describedby="basic-addon2"
                    type="text"
                    value={keyword}
                    name="keyword"
                    onChange={handleKeyword}
                  />
                  <InputGroup.Append>
                    <Button
                      variant="outline-primary"
                      onClick={(e) => fetchData(e)}
                    >
                      <Search size={14} className="mr-1" />
                      CARI
                    </Button>
                  </InputGroup.Append>
                </InputGroup>

                <OverlayTrigger overlay={<Tooltip>Tambah Data</Tooltip>}>
                  <Button
                    className="py-2 px-3 btn-sync"
                    variant="outline-success"
                    onClick={handleTambahModal}
                  >
                    <PlusCircle size={14} />
                  </Button>
                </OverlayTrigger>

                <OverlayTrigger overlay={<Tooltip>Download Data</Tooltip>}>
                  <Button
                    className="py-2 px-3 btn-download"
                    variant="outline-primary"
                    onClick={handleExportExcelModal}
                  >
                    <Download size={14} />
                  </Button>
                </OverlayTrigger>
                <OverlayTrigger overlay={<Tooltip>Unggah Data</Tooltip>}>
                  <Button
                    className="py-2 px-3 btn-upload"
                    variant="outline-danger"
                    onClick={handleExportExcelModal}
                  >
                    <Upload size={14} />
                  </Button>
                </OverlayTrigger>
              </div>
              <div className="table-responsive mb-3">
                <RemoteTable
                  className="table table-responsive table-bordered"
                  keyField="id"
                  data={resultLuasWilayahKecamatan}
                  columns={columns}
                  page={page}
                  sizePerPage={sizePerPage}
                  totalSize={totalRows}
                  onTableChange={handleOnTableChange}
                  noDataIndication={loading ? "loading" : "Tidak Ada Data"}
                />
              </div>

              <div className="pt-8">
                <Card
                  title={
                    keyword
                      ? location.state.judul
                      : "Pilih Tahun pada pencarian untuk menampilkan grafik"
                  }
                >
                  {keyword && (
                    <PieChart
                      title={location.state.judul}
                      dataSet={datasetChart}
                      labels={labelsChart}
                      // backgroundColor={generateRandomHexColorCombination(6)}
                      // color={generateRandomHexColorCombination(6)}
                    />
                  )}
                </Card>
              </div>
            </Card>
          </Col>
        </Row>
      </Aux>

      {openTambahModal && (
        <Tambah
          openTambahModal={openTambahModal}
          handleTambahModal={handleTambahModal}
          setOpenTambahModal={setOpenTambahModal}
          inputData={inputData}
          setInputData={setInputData}
          handleChangeInputData={handleChangeInputData}
          catatan={catatan}
          setCatatan={setCatatan}
          handleInputCatatan={handleInputCatatan}
          error={error}
          setError={setError}
          loading={loading}
          setLoading={setLoading}
          fetchData={fetchData}
          idMstCollection={location?.state?.idMstCollection}
        />
      )}

      {openUpdateModal && (
        <Ubah
          openUpdateModal={openUpdateModal}
          setOpenUpdateModal={setOpenUpdateModal}
          handleUpdateModal={handleUpdateModal}
          inputData={inputData}
          setInputData={setInputData}
          handleChangeInputData={handleChangeInputData}
          catatan={catatan}
          setCatatan={setCatatan}
          handleInputCatatan={handleInputCatatan}
          error={error}
          setError={setError}
          fetchData={fetchData}
          loading={loading}
          setLoading={setLoading}
          row={row}
          idMstCollection={location?.state?.idMstCollection}
        />
      )}

      {openExportExcelModal && (
        <ExportExcel
          openExportExcelModal={openExportExcelModal}
          handleExportExcelModal={handleExportExcelModal}
          setOpenExportExcelModal={setOpenExportExcelModal}
          inputData={inputData}
          setInputData={setInputData}
          handleChangeInputData={handleChangeInputData}
          // catatan={catatan}
          // setCatatan={setCatatan}
          // handleInputCatatan={handleInputCatatan}
          error={error}
          setError={setError}
          loading={loading}
          setLoading={setLoading}
          // fetchData={fetchData}
          idMstCollection={location?.state?.idMstCollection}
        />
      )}

      <DeleteModal
        title="Hapus Data Ini?"
        subtitle="Data tidak dapat dikembalikan!"
        sourceIcon={sourceIcon?.alert}
        buttonConfirm="Ya, Hapus"
        buttonCancel="Batal"
        namaState={openDeleteModal}
        namaSetState={setOpenDeleteModal}
        handleForm={handleDeleteDokumen}
        loading={loading}
      />

      <CkeditorModal
        title="Catatan"
        value={ReactHtmlParser(catatan)}
        namaState={openCkeditorModal}
        namaSetState={setOpenCkeditorModal}
        buttonCancel="Tutup"
        size="sm" //lg,xl,sm,md
      />
    </>
  );
};

export default Home;
