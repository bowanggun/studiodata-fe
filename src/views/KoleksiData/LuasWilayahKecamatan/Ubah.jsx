import React, { useRef, useEffect } from "react";
import { Modal, Form, Button } from "react-bootstrap";
import { Edit3, XCircle } from "react-feather";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import baseApi from "../../../services/baseApi";
import { toast } from "react-toastify";
import Loader from "react-loader-spinner";

const Ubah = (props) => {
  const {
    openUpdateModal,
    handleUpdateModal,
    inputData,
    handleChangeInputData,
    error,
    catatan,
    handleInputCatatan,
    fetchData,
    setError,
    setOpenUpdateModal,
    // setKeterangan,
    loading,
    setLoading,
    setInputData,
    row,
    idMstCollection,
  } = props;

  const innerRef = useRef();

  const handleUpdateLuasWilayah = (e) => {
    e.preventDefault();
    setLoading(true);

    const formData = new FormData();

    formData.append("tahun", inputData.tahun ? inputData.tahun : "");
    formData.append(
      "kecamatan",
      inputData.kecamatan ? inputData.kecamatan : ""
    );
    formData.append(
      "jml_kelurahan",
      inputData.jmlKelurahan ? inputData.jmlKelurahan : ""
    );
    formData.append(
      "luas_wilayah",
      inputData.luasWilayah ? inputData.luasWilayah : ""
    );
    formData.append("satuan", inputData.satuan ? inputData.satuan : "");
    formData.append("mst_collection_id", idMstCollection);
    formData.append("catatan", catatan ? catatan : "");
    formData.append("_method", "PUT");

    baseApi
      .post(`/luas-wilayah-kecamatan-kelurahan/${row}`, formData)
      .then((response) => {
        if (response?.status === 200) {
          fetchData();
          toast.success(response?.data?.message, {
            position: toast.POSITION.TOP_RIGHT,
          });
          setOpenUpdateModal(false);
          setInputData({});
          setLoading(false);
        } else {
          // console.log("response", response);
          setLoading(false);
        }
      })
      .catch((error) => {
        setLoading(false);
        setError(error.response?.data?.errors);

        toast.error(error.response?.data?.message, {
          position: toast.POSITION.TOP_RIGHT,
        });
      });
  };

  useEffect(() => {
    setTimeout(() => {
      innerRef.current.focus();
    }, 1);
  }, []);

  return (
    <>
      <Modal
        show={openUpdateModal}
        onHide={handleUpdateModal}
        backdrop="static"
        keyboard={false}
      >
        <Form onSubmit={handleUpdateLuasWilayah}>
          <Modal.Header>
            <Modal.Title>Ubah Luas Wilayah</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form.Group className="mb-3 form-label-group">
              <Form.Control
                ref={innerRef}
                type="text"
                id="kecamatan"
                name="kecamatan"
                aria-describedby="passwordHelpBlock"
                value={inputData.kecamatan}
                onChange={handleChangeInputData}
              />
              <Form.Label htmlFor="kecamatan">Kecamatan</Form.Label>
              <span className="text-danger">
                <i>{error?.kecamatan ?? ""}</i>
              </span>
            </Form.Group>

            <Form.Group className="mb-3 form-label-group">
              <Form.Control
                type="text"
                id="tahun"
                name="tahun"
                aria-describedby="passwordHelpBlock"
                value={inputData.tahun}
                onChange={handleChangeInputData}
              />
              <Form.Label htmlFor="tahun">Tahun</Form.Label>
              <span className="text-danger">
                <i>{error?.tahun ?? ""}</i>
              </span>
            </Form.Group>

            <Form.Group className="mb-3 form-label-group">
              <Form.Control
                placeholder="Jumlah Kelurahan"
                className="form-control"
                type="text"
                id="jmlKelurahan"
                name="jmlKelurahan"
                aria-describedby="passwordHelpBlock"
                value={inputData.jmlKelurahan}
                onChange={handleChangeInputData}
              />
              <Form.Label htmlFor="jmlKelurahan">Jumlah Kelurahan</Form.Label>
              <span className="text-danger">
                <i>{error?.jml_kelurahan ?? ""}</i>
              </span>
            </Form.Group>

            <Form.Group className="mb-3 form-label-group">
              <Form.Control
                placeholder="Luas Wilayah"
                className="form-control"
                type="text"
                id="luasWilayah"
                name="luasWilayah"
                aria-describedby="passwordHelpBlock"
                value={inputData.luasWilayah}
                onChange={handleChangeInputData}
              />
              <Form.Label htmlFor="luasWilayah">Luas Wilayah</Form.Label>
              <span className="text-danger">
                <i>{error?.luas_wilayah ?? ""}</i>
              </span>
            </Form.Group>

            <Form.Group className="mb-3 form-label-group">
              <Form.Control
                placeholder="Satuan"
                className="form-control"
                type="text"
                id="satuan"
                name="satuan"
                aria-describedby="passwordHelpBlock"
                value={inputData.satuan}
                onChange={handleChangeInputData}
              />
              <Form.Label htmlFor="satuan">Satuan</Form.Label>
              <span className="text-danger">
                <i>{error?.satuan ?? ""}</i>
              </span>
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>Catatan</Form.Label>
              <CKEditor
                editor={ClassicEditor}
                data={catatan}
                onChange={handleInputCatatan}
                onReady={(editor) => {
                  editor.editing.view.change((writer) => {
                    writer.setStyle(
                      "height",
                      "200px",
                      editor.editing.view.document.getRoot()
                    );
                  });
                }}
              />
              <span className="text-danger">
                <i>{error?.catatan ?? ""}</i>
              </span>
            </Form.Group>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="primary" type="submit" disabled={loading}>
              {loading ? (
                <Loader type="ThreeDots" color="#fff" height={10} />
              ) : (
                <>
                  <Edit3 size={14} className="mr-1" /> Ubah
                </>
              )}
            </Button>
            <Button variant="secondary" onClick={handleUpdateModal}>
              <XCircle size={14} className="mr-1" />
              Tutup
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
    </>
  );
};

export default Ubah;
