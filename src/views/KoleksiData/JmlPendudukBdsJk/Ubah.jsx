import React, { useRef, useEffect, useState } from "react";
import { Modal, Form, Button } from "react-bootstrap";
import { Edit3, XCircle } from "react-feather";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import baseApi from "../../../services/baseApi";
import { toast } from "react-toastify";
import Loader from "react-loader-spinner";
import SelectCustom from "../../../App/components/SelectCustom";

const Ubah = (props) => {
  const {
    openUpdateModal,
    handleUpdateModal,
    inputData,
    handleChangeInputData,
    error,
    catatan,
    handleInputCatatan,
    fetchData,
    setError,
    setOpenUpdateModal,
    // setKeterangan,
    loading,
    setLoading,
    setInputData,
    row,
    satuan,
  } = props;

  const [dataWilayah, setDataWilayah] = useState([]);
  const [valueDataWilayah, setValueDataWilayah] = useState("");

  const innerRef = useRef();

  const fetchDataWilayah = (inputValue) => {
    setLoading(true);
    baseApi
      .get(`/wilayah?q=${inputValue}&paging=false`)
      .then((response) => {
        if (response?.status === 200) {
          setDataWilayah(response?.data?.result);
          setLoading(false);
        }
      })
      .catch((error) => {
        setLoading(false);
        toast.warning("Terjadi kesalahan", {
          position: toast.POSITION.TOP_RIGHT,
        });
        // console.log(error);
      });
  };

  const handleUpdateKoleksiData = (e) => {
    e.preventDefault();
    setLoading(true);

    const formData = new FormData();

    formData.append("nama_collection", inputData.namaCollection ? inputData.namaCollection : "");
    formData.append("tahun", inputData.tahun ? inputData.tahun : "");
    formData.append("target", inputData.target ? inputData.target : "");
    formData.append("realisasi", inputData.realisasi ? inputData.realisasi : "");
    formData.append("kategori", inputData.kategori ? inputData.kategori : "");
    formData.append("sumber", inputData.sumber ? inputData.sumber : "");
    formData.append("catatan", catatan ? catatan : "");
    formData.append("kemendagri_kota_kode", "32.71");
    formData.append("kemendagri_kota_nama", "KOTA BOGOR");
    // formData.append("kemendagri_kota_kode", dataWilayah[0]?.kemendagri_kota_kode ? dataWilayah[0]?.kemendagri_kota_kode : "");
    // formData.append("kemendagri_kota_nama", dataWilayah[0]?.kemendagri_kota_nama ? dataWilayah[0]?.kemendagri_kota_nama : "");

    formData.append("_method", "PUT");

    baseApi
      .post(`/jumlah_penduduk-bds-jk/${row}`, formData)
      .then((response) => {
        if (response?.status === 200) {
          fetchData();
          toast.success(response?.data?.message, {
            position: toast.POSITION.TOP_RIGHT,
          });
          setOpenUpdateModal(false);
          setInputData({});
          setLoading(false);
        } else {
          setLoading(false);
          // console.log("response", response);
        }
      })
      .catch((error) => {
        setLoading(false);
        setError(error.response?.data?.errors);

        toast.error(error.response?.data?.message, {
          position: toast.POSITION.TOP_RIGHT,
        });
      });
  };

  /*eslint-disable */
  useEffect(() => {
    setTimeout(() => {
      setDataWilayah([
        {
          value: inputData.kemendagri_kota_kode,
          label: inputData.kemendagri_kota_nama,
        },
      ]);

      innerRef.current.focus();
    }, 1);
  }, []);

  return (
    <>
      <Modal
        show={openUpdateModal}
        onHide={handleUpdateModal}
        backdrop="static"
        keyboard={false}
        size="lg"
      >
        <Form onSubmit={handleUpdateKoleksiData}>
          <Modal.Header>
            <Modal.Title>Ubah Koleksi Data</Modal.Title>
          </Modal.Header>
          <Modal.Body>

            <Form.Group className="mb-3 form-label-group">
              <Form.Control
                placeholder="satuan"
                className="form-control"
                type="text"
                id="satuan"
                name="satuan"
                aria-describedby="passwordHelpBlock"
                value={satuan}
                // onChange={handleChangeInputData}
                readOnly
              />
              <Form.Label htmlFor="tahun">Satuan</Form.Label>
              {/* <span className="text-danger">
                <i>{error?.tahun ?? ""}</i>
              </span> */}
            </Form.Group>

            {/* <Form.Group className="mb-3">
              <Form.Label htmlFor="wilayah">Wilayah</Form.Label>
              <SelectCustom
                data={dataWilayah}
                setError={setError}
                setValueData={setValueDataWilayah}
                fetchData={fetchDataWilayah}
                labelName="dataWilayah"
                valueDataOrganisasi={dataWilayah[0]}
                // isMulti={false}
              />

              <span className="text-danger">
                <i>{error?.kemendagri_kota_kode ?? ""}</i>
              </span>
            </Form.Group> */}

            <Form.Group className="mb-3 form-label-group">
              <Form.Control
                placeholder="Tahun"
                className="form-control"
                type="text"
                id="tahun"
                name="tahun"
                aria-describedby="passwordHelpBlock"
                value={inputData.tahun}
                onChange={handleChangeInputData}
                ref={innerRef}
                maxLength={4}
              />
              <Form.Label htmlFor="tahun">Tahun</Form.Label>
              <span className="text-danger">
                <i>{error?.tahun ?? ""}</i>
              </span>
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label htmlFor="kategori">Kategori</Form.Label>
              <Form.Control
                as="select"
                id="kategori"
                name="kategori"
                value={inputData.kategori}
                onChange={handleChangeInputData}
                style={{ cursor: "pointer" }}
              >
                <option selected value="">
                  -
                </option>
                <option value="Laki-laki">Laki-laki</option>
                <option value="Perempuan">Perempuan</option>
                
              </Form.Control>
              <span className="text-danger">
                <i>{error?.kategori ?? ""}</i>
              </span>
            </Form.Group>

            <Form.Group className="mb-3 form-label-group">
              <Form.Control
                placeholder="Target"
                className="form-control"
                type="text"
                id="target"
                name="target"
                aria-describedby="passwordHelpBlock"
                value={inputData.target}
                onChange={handleChangeInputData}
              />
              <Form.Label htmlFor="target">Target</Form.Label>
              <span className="text-danger">
                <i>{error?.target ?? ""}</i>
              </span>
            </Form.Group>

            <div className="mb-3">
              <strong>Pratinjau: </strong>
              {inputData.target ? (
                parseFloat(inputData.target.replace(",", ".")).toLocaleString("id-ID", {
                  minimumFractionDigits: 3,
                  maximumFractionDigits: 3,
                })
              ) : (
                "Hanya untuk angka"
              )}
            </div>

            <Form.Group className="mb-3 form-label-group">
              <Form.Control
                placeholder="Realisasi"
                className="form-control"
                type="text"
                id="realisasi"
                name="realisasi"
                aria-describedby="passwordHelpBlock"
                value={inputData.realisasi}
                onChange={handleChangeInputData}
              />
              <Form.Label htmlFor="realisasi">Realisasi</Form.Label>
              <span className="text-danger">
                <i>{error?.realisasi ?? ""}</i>
              </span>
            </Form.Group>

            <div className="mb-3">
              <strong>Pratinjau: </strong>
              {inputData.realisasi ? (
                parseFloat(inputData.realisasi.replace(",", ".")).toLocaleString("id-ID", {
                  minimumFractionDigits: 3,
                  maximumFractionDigits: 3,
                })
              ) : (
                "Hanya untuk angka"
              )}
            </div>

            <Form.Group className="mb-3 form-label-group">
              <Form.Control
                placeholder="Sumber"
                className="form-control"
                type="text"
                id="sumber"
                name="sumber"
                aria-describedby="passwordHelpBlock"
                value={inputData.sumber}
                onChange={handleChangeInputData}
              />
              <Form.Label htmlFor="sumber">Sumber</Form.Label>
              <span className="text-danger">
                <i>{error?.sumber ?? ""}</i>
              </span>
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>Catatan</Form.Label>
              <CKEditor
                editor={ClassicEditor}
                data={catatan}
                onChange={handleInputCatatan}
                onReady={(editor) => {
                  editor.editing.view.change((writer) => {
                    writer.setStyle(
                      "height",
                      "200px",
                      editor.editing.view.document.getRoot()
                    );
                  });
                }}
              />
              <span className="text-danger">
                <i>{error?.catatan ?? ""}</i>
              </span>
            </Form.Group>

          </Modal.Body>
          <Modal.Footer>
            <Button variant="primary" type="submit" disabled={loading}>
              {loading ? (
                <Loader type="ThreeDots" color="#fff" height={10} />
              ) : (
                <>
                  <Edit3 size={14} className="mr-1" /> Ubah
                </>
              )}
            </Button>
            <Button variant="secondary" onClick={handleUpdateModal}>
              <XCircle size={14} className="mr-1" />
              Tutup
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
    </>
  );
}

export default Ubah