import React, { useRef, useEffect, useState } from "react";
import { Modal, Form, Button } from "react-bootstrap";
import { Edit3, XCircle } from "react-feather";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import baseApi from "../../services/baseApi";
import { toast } from "react-toastify";
import Loader from "react-loader-spinner";
import SelectCustom from "../../App/components/SelectCustom";

const Ubah = (props) => {
  const {
    openUpdateModal,
    handleUpdateModal,
    inputData,
    handleChangeInputData,
    error,
    catatan,
    handleInputCatatan,
    fetchData,
    setError,
    setOpenUpdateModal,
    // setKeterangan,
    loading,
    setLoading,
    setInputData,
    row,
    definisi,
    handleInputDefinisi,
    rumusPerhitungan,
    handleInputRumusPerhitungan,
    caraMemperolehData,
    handleInputCaraMemperolehData,
    isMultidimensi,
    handleChangeInputDataIsMultidimensi,
  } = props;

  const [dataOrganisasi, setDataOrganisasi] = useState([]);
  const [valueDataOrganisasi, setValueDataOrganisasi] = useState("");
  const [resultTipeCollection, setTipeCollection] = useState([]);
  const [resultDefaultChart, setDefaultChart] = useState([]);
  const [tipe, setTipe] = useState(inputData.tipe);
  const [defChart, setDefChart] = useState(inputData.defaultChart);

  const innerRef = useRef();

  const fetchDataOrganisasi = (inputValue) => {
    setLoading(true);
    baseApi
      .get(`/organisasi?q=${inputValue}&paging=false`)
      .then((response) => {
        if (response?.status === 200) {
          setDataOrganisasi(response?.data?.result);
          setLoading(false);
        }
      })
      .catch((error) => {
        setLoading(false);
        toast.warning("Terjadi kesalahan", {
          position: toast.POSITION.TOP_RIGHT,
        });
        // console.log(error);
      });
  };

  const fetchDataTipeCollection = () => {
    baseApi
      .get(`/tipe-collection`)
      .then((response) => {
        if (response?.status === 200) {
          setTipeCollection(response?.data?.result);
          setLoading(false);
        }
      })
      .catch((error) => {
        setLoading(false);
        toast.warning("Terjadi kesalahan", {
          position: toast.POSITION.TOP_RIGHT,
        });
        // console.log(error);
      });
  };

  // console.log("dataorg", dataOrganisasi);

  const fetchDataDefaultChart = () => {
    baseApi
      .get(`/default-chart`)
      .then((response) => {
        if (response?.status === 200) {
          setDefaultChart(response?.data?.result);
          setLoading(false);
        }
      })
      .catch((error) => {
        setLoading(false);
        toast.warning("Terjadi kesalahan", {
          position: toast.POSITION.TOP_RIGHT,
        });
        // console.log(error);
      });
  };

  const handleUpdateKoleksiData = (e) => {
    e.preventDefault();
    setLoading(true);

    const formData = new FormData();

    formData.append(
      "nama_collection",
      inputData.namaCollection ? inputData.namaCollection : ""
    );
    formData.append("judul", inputData.judul ? inputData.judul : "");
    formData.append("tipe", tipe ? tipe : "");
    formData.append("default_chart", defChart ? defChart : "");
    formData.append(
      "referensi_data",
      inputData.referensiData ? inputData.referensiData : ""
    );
    formData.append(
      "route_name",
      inputData.routeName ? inputData.routeName : ""
    );
    formData.append(
      "table_name",
      inputData.tableName ? inputData.tableName : ""
    );
    formData.append("catatan", catatan ? catatan : "");
    formData.append("definisi", definisi ? definisi: "");
    formData.append("rumus_perhitungan", rumusPerhitungan ? rumusPerhitungan : "");
    formData.append("cara_memperoleh_data", caraMemperolehData ? caraMemperolehData : "");
    formData.append("urusan", inputData.urusan ? inputData.urusan : "");
    formData.append("is_multidimensi", isMultidimensi);
    formData.append("_method", "PUT");

    baseApi
      .post(`/mst-collection/${row}`, formData)
      .then((response) => {
        if (response?.status === 200) {
          fetchData();
          toast.success(response?.data?.message, {
            position: toast.POSITION.TOP_RIGHT,
          });
          setOpenUpdateModal(false);
          setInputData({});
          setLoading(false);
        } else {
          setLoading(false);
          // console.log("response", response);
        }
      })
      .catch((error) => {
        setLoading(false);
        setError(error.response?.data?.errors);

        toast.error(error.response?.data?.message, {
          position: toast.POSITION.TOP_RIGHT,
        });
      });
  };

  /*eslint-disable */
  useEffect(() => {
    setTimeout(() => {
      setDataOrganisasi([
        {
          value: inputData.ref_organisasi_id,
          label: inputData.organisasi,
        },
      ]);

      innerRef.current.focus();
      fetchDataTipeCollection();
      fetchDataDefaultChart();
    }, 1);
  }, []);

  // console.log(valueDataOrganisasi);

  return (
    <>
      <Modal
        show={openUpdateModal}
        onHide={handleUpdateModal}
        backdrop="static"
        keyboard={false}
        size="lg"
      >
        <Form onSubmit={handleUpdateKoleksiData}>
          <Modal.Header>
            <Modal.Title>Ubah Koleksi Data</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form.Group className="mb-3 form-label-group">
              <Form.Control
                ref={innerRef}
                type="text"
                id="namaCollection"
                name="namaCollection"
                aria-describedby="passwordHelpBlock"
                value={inputData.namaCollection}
                onChange={handleChangeInputData}
              />
              <Form.Label htmlFor="namaCollection">Nama Data</Form.Label>
              <span className="text-danger">
                <i>{error?.nama_collection ?? ""}</i>
              </span>
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>Definisi</Form.Label>
              <CKEditor
                editor={ClassicEditor}
                data={definisi}
                onChange={handleInputDefinisi}
                onReady={(editor) => {
                  editor.editing.view.change((writer) => {
                    writer.setStyle(
                      "height",
                      "200px",
                      editor.editing.view.document.getRoot()
                    );
                  });
                }}
              />
              <span className="text-danger">
                <i>{error?.definisi ?? ""}</i>
              </span>
            </Form.Group>

            {/* <Form.Group className="mb-3 form-label-group">
              <Form.Control
                type="text"
                id="judul"
                name="judul"
                aria-describedby="passwordHelpBlock"
                value={inputData.judul}
                onChange={handleChangeInputData}
              />
              <Form.Label htmlFor="judul">Judul</Form.Label>
              <span className="text-danger">
                <i>{error?.judul ?? ""}</i>
              </span>
            </Form.Group> */}

            <Form.Group className="mb-3 form-label-group">
              <Form.Control
                placeholder="Satuan"
                className="form-control"
                type="text"
                id="satuan"
                name="satuan"
                aria-describedby="passwordHelpBlock"
                value={inputData.satuan}
                onChange={handleChangeInputData}
              />
              <Form.Label htmlFor="satuan">Satuan</Form.Label>
              <span className="text-danger">
                <i>{error?.satuan ?? ""}</i>
              </span>
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>Rumus Perhitungan</Form.Label>
              <CKEditor
                editor={ClassicEditor}
                data={rumusPerhitungan}
                onChange={handleInputRumusPerhitungan}
                onReady={(editor) => {
                  editor.editing.view.change((writer) => {
                    writer.setStyle(
                      "height",
                      "200px",
                      editor.editing.view.document.getRoot()
                    );
                  });
                }}
              />
              <span className="text-danger">
                <i>{error?.rumus_perhitungan ?? ""}</i>
              </span>
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label htmlFor="ref_organisasi_id">Organisasi</Form.Label>
              {/* <Form.Control
                as="select"
                id="ref_organisasi_id"
                name="ref_organisasi_id"
                value={inputData.ref_organisasi_id}
                onChange={handleChangeInputData}
                style={{ cursor: "pointer" }}
              >
                <option selected value="">
                  -
                </option>
                {resultOrganisasi?.map((item, index) => (
                  <option key={index} value={item.id}>
                    {item?.organisasi}
                  </option>
                ))}
              </Form.Control> */}

              <SelectCustom
                data={dataOrganisasi}
                setError={setError}
                setValueData={setValueDataOrganisasi}
                fetchData={fetchDataOrganisasi}
                labelName="dataOrganisasi"
                valueDataOrganisasi={dataOrganisasi[0]}
                // isMulti={false}
              />

              <span className="text-danger">
                <i>{error?.ref_organisasi_id ?? ""}</i>
              </span>
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>Cara Memperoleh Data</Form.Label>
              <CKEditor
                editor={ClassicEditor}
                data={caraMemperolehData}
                onChange={handleInputCaraMemperolehData}
                onReady={(editor) => {
                  editor.editing.view.change((writer) => {
                    writer.setStyle(
                      "height",
                      "200px",
                      editor.editing.view.document.getRoot()
                    );
                  });
                }}
              />
              <span className="text-danger">
                <i>{error?.cara_memperoleh_data ?? ""}</i>
              </span>
            </Form.Group>

            <Form.Group className="mb-3 form-label-group">
              <Form.Control
                placeholder="Urusan"
                className="form-control"
                type="text"
                id="urusan"
                name="urusan"
                aria-describedby="passwordHelpBlock"
                value={inputData.urusan}
                onChange={handleChangeInputData}
              />
              <Form.Label htmlFor="urusan">Urusan</Form.Label>
              <span className="text-danger">
                <i>{error?.urusan ?? ""}</i>
              </span>
            </Form.Group>

            {/* <Form.Group className="mb-3">
              <Form.Label htmlFor="tipe">Tipe</Form.Label>
              <Form.Control
                as="select"
                id="tipe"
                name="tipe"
                value={tipe}
                // onChange={handleChangeTipe}
                onChange={(e) => setTipe(e.target.value)}
                style={{ cursor: "pointer" }}
              >
                <option value="">-</option>
                {resultTipeCollection?.map((item, index) => (
                  <option key={index} value={item.tipe}>
                    {item?.tipe}
                  </option>
                ))}
              </Form.Control>
              <span className="text-danger">
                <i>{error?.tipe ?? ""}</i>
              </span>
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label htmlFor="defaultChart">Default Chart</Form.Label>
              <Form.Control
                as="select"
                id="defaultChart"
                name="defaultChart"
                value={defChart}
                // onChange={handleChangeInputData}
                onChange={(e) => setDefChart(e.target.value)}
                style={{ cursor: "pointer" }}
              >
                <option selected value="">
                  -
                </option>
                {resultDefaultChart?.map((item, index) => (
                  <option key={index} value={item.chart}>
                    {item?.chart}
                  </option>
                ))}
              </Form.Control>
              <span className="text-danger">
                <i>{error?.default_chart ?? ""}</i>
              </span>
            </Form.Group>

            <Form.Group className="mb-3 form-label-group">
              <Form.Control
                type="text"
                id="referensiData"
                name="referensiData"
                aria-describedby="passwordHelpBlock"
                value={inputData.referensiData}
                onChange={handleChangeInputData}
              />
              <Form.Label htmlFor="referensiData">Referensi Data</Form.Label>
              <span className="text-danger">
                <i>{error?.referensi_data ?? ""}</i>
              </span>
            </Form.Group> */}

            <Form.Group className="mb-3">
              <Form.Label>Catatan</Form.Label>
              <CKEditor
                editor={ClassicEditor}
                data={catatan}
                onChange={handleInputCatatan}
                onReady={(editor) => {
                  editor.editing.view.change((writer) => {
                    writer.setStyle(
                      "height",
                      "200px",
                      editor.editing.view.document.getRoot()
                    );
                  });
                }}
              />
              <span className="text-danger">
                <i>{error?.catatan ?? ""}</i>
              </span>
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label htmlFor="is_multidimensi">Multidimensional?</Form.Label>
              <div key={`inline-radio`} className="mb-3">
                <Form.Check
                  inline
                  label="Ya"
                  name="is_multidimensi"
                  type="radio"
                  id={`is_multidimensi_true`}
                  value={isMultidimensi}
                  onChange={handleChangeInputDataIsMultidimensi}
                  defaultChecked={isMultidimensi === 1}
                />
                <Form.Check
                  inline
                  label="Tidak"
                  name="is_multidimensi"
                  type="radio"
                  id={`is_multidimensi_false`}
                  value={isMultidimensi}
                  onChange={handleChangeInputDataIsMultidimensi}
                  defaultChecked={isMultidimensi === 0}
                />
              </div>
              <span className="text-danger">
                <i>{error?.is_multidimensi ?? ""}</i>
              </span>
            </Form.Group>

            <Form.Group className="mb-3 form-label-group">
              <Form.Control
                type="text"
                id="routeName"
                name="routeName"
                aria-describedby="passwordHelpBlock"
                value={inputData.routeName}
                onChange={handleChangeInputData}
              />
              <Form.Label htmlFor="routeName">Route Name</Form.Label>
              <span className="text-danger">
                <i>{error?.route_name ?? ""}</i>
              </span>
            </Form.Group>

            <Form.Group className="mb-3 form-label-group">
              <Form.Control
                type="text"
                id="tableName"
                name="tableName"
                aria-describedby="passwordHelpBlock"
                value={inputData.tableName}
                onChange={handleChangeInputData}
              />
              <Form.Label htmlFor="tableName">Table Name</Form.Label>
              <span className="text-danger">
                <i>{error?.table_name ?? ""}</i>
              </span>
            </Form.Group>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="primary" type="submit" disabled={loading}>
              {loading ? (
                <Loader type="ThreeDots" color="#fff" height={10} />
              ) : (
                <>
                  <Edit3 size={14} className="mr-1" /> Ubah
                </>
              )}
            </Button>
            <Button variant="secondary" onClick={handleUpdateModal}>
              <XCircle size={14} className="mr-1" />
              Tutup
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
    </>
  );
};

export default Ubah;
