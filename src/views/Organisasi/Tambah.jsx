import React, { useRef, useEffect } from "react";
import { Modal, Form, Button } from "react-bootstrap";
import { PlusCircle, XCircle } from "react-feather";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import baseApi from "../../services/baseApi";
import { toast } from "react-toastify";
import Loader from "react-loader-spinner";

const Tambah = (props) => {
  const {
    openTambahModal,
    setOpenTambahModal,
    handleTambahModal,
    inputData,
    setInputData,
    handleChangeInputData,
    error,
    setError,
    keterangan,
    setKeterangan,
    handleInputKeterangan,
    loading,
    setLoading,
    fetchData,
  } = props;

  const innerRef = useRef();

  const handleInputOrganisasi = (e) => {
    e.preventDefault();
    setLoading(true);

    const formData = new FormData();

    formData.append(
      "organisasi",
      inputData.organisasi ? inputData.organisasi : ""
    );
    formData.append(
      "singkatan",
      inputData.singkatan ? inputData.singkatan : ""
    );
    formData.append("keterangan", keterangan);

    baseApi
      .post(`/organisasi`, formData)
      .then((response) => {
        if (response?.status === 200) {
          fetchData();
          toast.success(response?.data?.message, {
            position: toast.POSITION.TOP_RIGHT,
          });
          setOpenTambahModal(false);
          setKeterangan("");
          setInputData({});
          setLoading(false);
        } else {
          setLoading(false);
          // console.log("response", response);
        }
      })
      .catch((error) => {
        setLoading(false);
        setError(error.response?.data?.errors);

        toast.error(error.response?.data?.message, {
          position: toast.POSITION.TOP_RIGHT,
        });
      });
  };

  useEffect(() => {
    setTimeout(() => {
      innerRef.current.focus();
      setKeterangan("");
      setInputData({});
    }, 1);
  }, [setInputData, setKeterangan]);

  return (
    <>
      <Modal
        show={openTambahModal}
        onHide={handleTambahModal}
        backdrop="static"
        keyboard={false}
      >
        <Form onSubmit={handleInputOrganisasi}>
          <Modal.Header>
            <Modal.Title>Tambah Organisasi</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form.Group className="mb-3 form-label-group">
              <Form.Control
                ref={innerRef}
                placeholder="Organisasi"
                className="form-control"
                type="text"
                id="organisasi"
                name="organisasi"
                aria-describedby="passwordHelpBlock"
                value={inputData.organisasi}
                onChange={handleChangeInputData}
              />
              <Form.Label htmlFor="organisasi">Organisasi</Form.Label>
              <span className="text-danger">
                <i>{error?.organisasi ?? ""}</i>
              </span>
            </Form.Group>

            <Form.Group className="mb-3 form-label-group">
              <Form.Control
                placeholder="Singkatan"
                className="form-control"
                type="text"
                id="singkatan"
                name="singkatan"
                aria-describedby="passwordHelpBlock"
                value={inputData.singkatan}
                onChange={handleChangeInputData}
              />
              <Form.Label htmlFor="singkatan">Singkatan</Form.Label>
              <span className="text-danger">
                <i>{error?.singkatan ?? ""}</i>
              </span>
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label htmlFor="keterangan">Keterangan</Form.Label>
              <CKEditor
                editor={ClassicEditor}
                data={keterangan}
                onChange={handleInputKeterangan}
                onReady={(editor) => {
                  editor.editing.view.change((writer) => {
                    writer.setStyle(
                      "height",
                      "200px",
                      editor.editing.view.document.getRoot()
                    );
                  });
                }}
              />
              <span className="text-danger">
                <i>{error?.keterangan ?? ""}</i>
              </span>
            </Form.Group>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="primary" type="submit" disabled={loading}>
              {loading ? (
                <Loader type="ThreeDots" color="#fff" height={10} />
              ) : (
                <>
                  <PlusCircle size={14} className="mr-1" /> Simpan
                </>
              )}
            </Button>
            <Button variant="secondary" onClick={handleTambahModal}>
              <XCircle size={14} className="mr-1" />
              Tutup
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
    </>
  );
};

export default Tambah;
