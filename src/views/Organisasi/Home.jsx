/*eslint-disable */
import React, { useEffect, useState } from "react";
import Aux from "../../hoc/_Aux";
import {
  Button,
  Col,
  FormControl,
  InputGroup,
  Row,
  OverlayTrigger,
  Tooltip,
  Form
} from "react-bootstrap";
import Card from "../../App/components/MainCard";

import moment from "moment";
import { RemoteTable } from "../../App/components/RemoteTable";
import { toast } from "react-toastify";
import DeleteModal from "../../App/components/DeleteModal";
import CkeditorModal from "../../App/components/CkeditorModal";
import { Edit3, Trash2, PlusCircle, Search, Slash } from "react-feather";
import { sourceIcon } from "../../App/components/Icon";
import { Link } from "react-router-dom";
import baseApi from "../../services/baseApi";
import Tambah from "./Tambah";
import Ubah from "./Ubah";
import ReactHtmlParser from "react-html-parser";

import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
import "react-bootstrap-table2-toolkit/dist/react-bootstrap-table2-toolkit.min.css";
import "react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css";
import BootstrapTable from "react-bootstrap-table-next";
import Pagination from "react-bootstrap/Pagination";
import PageItem from "react-bootstrap/PageItem";

const Home = () => {
  const id = require("moment/locale/id");
  moment.updateLocale("id", id);

  const [resultOrganisasi, setResultOrganisasi] = useState([]);
  const [loading, setLoading] = useState(false);
  const [page, setPage] = useState(1);
  const [currentPage, setCurrentPage] = useState(1);
  const [totalRows, setTotalRows] = useState(0);
  const [sizePerPage, setSizePerPage] = useState(10);
  const [fromPage, setFromPage] = useState(1);
  const [keyword, setKeyword] = useState("");
  const [openTambahModal, setOpenTambahModal] = useState(false);
  const [openUpdateModal, setOpenUpdateModal] = useState(false);
  const [openDeleteModal, setOpenDeleteModal] = useState(false);
  const [openCkeditorModal, setOpenCkeditorModal] = useState(false);
  const [paginationParameter, setPaginationParameter] = useState("");

  const [inputData, setInputData] = useState({
    organisasi: "",
    singkatan: "",
  });

  const [keterangan, setKeterangan] = useState("");
  const [error, setError] = useState([]);
  const [row, setRow] = useState("");

  const headerSortingClasses = (column, sortOrder, isLastSorting, colIndex) =>
    sortOrder === "asc" ? "bg-success" : "bg-info";

  const fetchData = async (e) => {
    try {
      e?.preventDefault();
      let apiOrganisasi = "";
      if (keyword === "" || keyword === undefined || keyword === null) {
        apiOrganisasi = "/organisasi";
      } else {
        apiOrganisasi = `/organisasi?q=${keyword}`;
      }

      const response = await baseApi.get(apiOrganisasi);
      if (response?.status === 200) {
        setResultOrganisasi(response?.data?.result?.data);
        setTotalRows(response?.data?.result?.total);
        // setFromPage(response?.data?.result?.from);
        setPaginationParameter(response?.data?.result);
      }
    } catch (error) {
      toast.warning("Terjadi kesalahan", {
        position: toast.POSITION.TOP_RIGHT,
      });
      console.log(error);
    }
  };

  const handleInputKeterangan = (e, editor) => {
    const data = editor.getData();
    setKeterangan(data);
  };

  const handleChangeInputData = (e) => {
    e.persist();
    setInputData({ ...inputData, [e.target.name]: e.target.value });
  };

  const handleDeleteOrganisasi = (e) => {
    e.preventDefault();
    setLoading(true);

    baseApi
      .delete(`/organisasi/${row}`)
      .then((response) => {
        if (response?.status === 200) {
          toast.info(response?.data?.message, {
            position: toast.POSITION.TOP_RIGHT,
          });

          setLoading(false);
          fetchData();
          setOpenDeleteModal(false);
        }
      })
      .catch((err) => {
        toast.error(err.response?.data?.message, {
          position: toast.POSITION.TOP_RIGHT,
        });

        setLoading(false);
      });
  };

  const handleKeyword = (e) => {
    setKeyword(e.target.value);
  };

  const handleTambahModal = () => {
    setOpenTambahModal(!openTambahModal);
    setInputData({
      organisasi: "",
      singkatan: "",
      keterangan: "",
    });
    setError([]);
  };
  const handleUpdateModal = () => {
    setOpenUpdateModal(!openUpdateModal);
    setError([]);
  };

  const handleOnTableChange = (page) => async (e) => {
    let result = resultOrganisasi;
    setLoading(true);
    setPage(page);

    const response = await baseApi.get(`/organisasi?page=${page}`);
    if (response?.status === 200) {
      setResultOrganisasi(response?.data?.result?.data);
      setCurrentPage(response?.data?.result?.current_page);
      setSizePerPage(response?.data?.result.per_page);
      setFromPage(response?.data?.result?.total);
      setLoading(false);
      setPaginationParameter(response?.data?.result);
    }
  };

  function prevPages(hal) {
    const listPages = [];
    listPages.push(
      <PageItem onClick={handleOnTableChange(hal - 1)}>{hal - 1}</PageItem>
    );
    return listPages;
  }

  function nextPages(hal) {
    const listPages = [];
    listPages.push(
      <PageItem onClick={handleOnTableChange(hal + 1)}>{hal + 1}</PageItem>
    );
    return listPages;
  }

  const columns = [
    // {
    //   dataField: "",
    //   text: "No.",
    //   formatter: (cell, row, rowIndex, extraData) => (
    //     <>{page * sizePerPage - sizePerPage + (rowIndex + 1)}</>
    //   ),
    //   headerAlign: "center",
    //   align: "center",
    // },
    {
      dataField: "",
      text: "",
      headerAlign: "center",
      align: "center",
      formatter: (cell, row, rowIndex, extraData) => (
        <div>
          {/* <ButtonGroup size="sm" aria-label="Basic example"> */}
          <OverlayTrigger overlay={<Tooltip>Ubah Data</Tooltip>}>
            <Button
              className="py-1 px-2"
              variant="outline-warning"
              size="sm"
              onClick={() =>
                // setOpenUpdateModal(!openUpdateModal, setRow(row?.id))
                setOpenUpdateModal(
                  !openUpdateModal,
                  setInputData({
                    organisasi: row.organisasi,
                    singkatan: row.singkatan,
                  }),
                  setKeterangan(row.keterangan),
                  setRow(row?.id)
                )
              }
            >
              <Edit3 size={14} />
            </Button>
          </OverlayTrigger>

          <OverlayTrigger
            overlay={
              <Tooltip>
                {row.trx_data_organisasi_latest
                  ? "Data ini tidak bisa dihapus"
                  : "Hapus Data"}
              </Tooltip>
            }
          >
            <Button
              disabled={row.trx_data_organisasi_latest}
              className="py-1 px-2"
              variant="outline-danger"
              size="sm"
              onClick={() =>
                setOpenDeleteModal(!openDeleteModal, setRow(row?.id))
              }
            >
              {row.trx_data_organisasi_latest ? (
                <Slash size={14} />
              ) : (
                <Trash2 size={14} />
              )}
            </Button>
          </OverlayTrigger>

          {/* </ButtonGroup> */}
        </div>
      ),
    },
    {
      dataField: "organisasi",
      text: "Organisasi",
      headerAlign: "center",
      sort: true,
      headerSortingClasses,
      formatter: (cell, row, rowIndex, extraData) => (
        <>
          <a
            href="#"
            onClick={() =>
              // setOpenUpdateModal(!openUpdateModal, setRow(row?.id))
              setOpenCkeditorModal(
                !openCkeditorModal,
                setKeterangan(row.keterangan)
              )
            }
          >
            {row.organisasi}
            {/* <File size={14} /> */}
          </a>
        </>
      ),
    },
    {
      dataField: "singkatan",
      text: "Singkatan",
      headerAlign: "center",
      sort: true,
      headerSortingClasses,
    },
    // {
    //   dataField: "keterangan",
    //   text: "Keterangan",
    //   headerAlign: "center",
    //   sort: true,
    //   headerSortingClasses,
    // },
    {
      dataField: "created_at",
      text: "Tanggal Dibuat",
      formatter: (cell, row, rowIndex, extraData) => (
        <>{moment(row.created_at).format("D MMMM YYYY H:mm:ss")}</>
      ),
      headerAlign: "center",
      align: "center",
      sort: true,
      headerSortingClasses,
    },
    {
      dataField: "created_by",
      text: "Dibuat Oleh",
      headerAlign: "center",
      align: "center",
      sort: true,
      headerSortingClasses,
    },
  ];

  /*eslint-disable */
  useEffect(() => {
    let cleanUp = true;
    if (cleanUp) {
      fetchData();
    }

    return () => {
      cleanUp = false;
    };
  }, []);

  // console.log("res", resultGrupDokumen);

  return (
    <>
      <Aux>
        <Row>
          <Col>
            <Card title="Organisasi" isOption>
              <div>
              <Form onSubmit={fetchData}>
                <InputGroup className="mb-3">
                  <FormControl
                    autoFocus
                    placeholder="Cari disini..."
                    aria-label="Cari disini..."
                    aria-describedby="basic-addon2"
                    type="text"
                    value={keyword}
                    name="keyword"
                    onChange={handleKeyword}
                  />
                  <InputGroup.Append>
                    <Button
                      variant="outline-primary"
                      type="submit"
                    >
                      <Search size={14} className="mr-1" />
                      CARI
                    </Button>
                  </InputGroup.Append>
                </InputGroup>
              </Form>

                <OverlayTrigger overlay={<Tooltip>Tambah Data</Tooltip>}>
                  <Button
                    className="py-2 px-3 btn-sync"
                    variant="outline-success"
                    onClick={handleTambahModal}
                  >
                    <PlusCircle size={14} />
                  </Button>
                </OverlayTrigger>
              </div>

              <div className="table-responsive">
                <BootstrapTable
                  keyField="id"
                  data={resultOrganisasi}
                  columns={columns}
                  hover
                  bordered
                  headerClasses="thead-dark"
                  className="table table-responsive"
                  noDataIndication={"Tidak ada data..."}
                />

                {resultOrganisasi?.length > 0 && (
                  <div className="d-lg-flex">
                    <div className="ml-auto text-center">
                      <Pagination
                        className="pagination justify-content-center"
                        listClassName="justify-content-center"
                      >
                        {paginationParameter.current_page > 1 && (
                          <>
                            <PageItem
                              onClick={handleOnTableChange(
                                paginationParameter.current_page - 1
                              )}
                              tabIndex="-1"
                            >
                              <i className="fa fa-angle-left" />
                              <span className="sr-only">Previous</span>
                            </PageItem>
                            {prevPages(paginationParameter?.current_page)}
                          </>
                        )}

                        <PageItem
                          className="active"
                          onClick={(e) => e.preventDefault()}
                        >
                          {paginationParameter.current_page}
                        </PageItem>
                        {paginationParameter?.next_page_url && (
                          <>
                            {nextPages(paginationParameter?.current_page)}
                            <PageItem
                              onClick={handleOnTableChange(
                                paginationParameter.current_page + 1
                              )}
                            >
                              <i className="fa fa-angle-right" />
                              <span className="sr-only">Next</span>
                            </PageItem>
                          </>
                        )}
                      </Pagination>
                    </div>
                  </div>
                )}
              </div>
            </Card>
          </Col>
        </Row>
      </Aux>

      {openTambahModal && (
        <Tambah
          openTambahModal={openTambahModal}
          setOpenTambahModal={setOpenTambahModal}
          handleTambahModal={handleTambahModal}
          inputData={inputData}
          setInputData={setInputData}
          handleChangeInputData={handleChangeInputData}
          keterangan={keterangan}
          setKeterangan={setKeterangan}
          handleInputKeterangan={handleInputKeterangan}
          error={error}
          setError={setError}
          loading={loading}
          setLoading={setLoading}
          fetchData={fetchData}
        />
      )}

      {openUpdateModal && (
        <Ubah
          openUpdateModal={openUpdateModal}
          setOpenUpdateModal={setOpenUpdateModal}
          handleUpdateModal={handleUpdateModal}
          inputData={inputData}
          setInputData={setInputData}
          handleChangeInputData={handleChangeInputData}
          keterangan={keterangan}
          setKeterangan={setKeterangan}
          handleInputKeterangan={handleInputKeterangan}
          error={error}
          setError={setError}
          fetchData={fetchData}
          loading={loading}
          setLoading={setLoading}
          row={row}
        />
      )}

      <DeleteModal
        title="Hapus Data Ini?"
        subtitle="Data tidak dapat dikembalikan!"
        sourceIcon={sourceIcon?.alert}
        buttonConfirm="Ya, Hapus"
        buttonCancel="Batal"
        namaState={openDeleteModal}
        namaSetState={setOpenDeleteModal}
        handleForm={handleDeleteOrganisasi}
        loading={loading}
      />

      <CkeditorModal
        title="Keterangan"
        value={ReactHtmlParser(keterangan)}
        namaState={openCkeditorModal}
        namaSetState={setOpenCkeditorModal}
        buttonCancel="Tutup"
        size="sm" //lg,xl,sm,md
      />
    </>
  );
};

export default Home;
