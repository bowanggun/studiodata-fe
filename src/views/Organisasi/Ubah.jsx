import React, { useRef, useEffect } from "react";
import { Modal, Form, Button } from "react-bootstrap";
import { Edit3, XCircle } from "react-feather";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import baseApi from "../../services/baseApi";
import { toast } from "react-toastify";
import Loader from "react-loader-spinner";

const Ubah = (props) => {
  const {
    openUpdateModal,
    handleUpdateModal,
    inputData,
    handleChangeInputData,
    error,
    keterangan,
    handleInputKeterangan,
    fetchData,
    setError,
    setOpenUpdateModal,
    // setKeterangan,
    loading,
    setLoading,
    setInputData,
    row,
  } = props;

  const innerRef = useRef();

  const handleUpdateOrganisasi = (e) => {
    e.preventDefault();
    setLoading(true);

    const formData = new FormData();

    formData.append("organisasi", inputData.organisasi);
    formData.append(
      "singkatan",
      inputData.singkatan ? inputData.singkatan : ""
    );
    formData.append("keterangan", keterangan ? keterangan : "");
    formData.append("_method", "PUT");

    baseApi
      .post(`/organisasi/${row}`, formData)
      .then((response) => {
        if (response?.status === 200) {
          fetchData();
          toast.success(response?.data?.message, {
            position: toast.POSITION.TOP_RIGHT,
          });
          setOpenUpdateModal(false);
          setInputData({});
          setLoading(false);
        } else {
          setLoading(false);
          // console.log("response", response);
        }
      })
      .catch((error) => {
        setLoading(false);
        setError(error.response?.data?.errors);

        toast.error(error.response?.data?.message, {
          position: toast.POSITION.TOP_RIGHT,
        });
      });
  };

  useEffect(() => {
    setTimeout(() => {
      innerRef.current.focus();
    }, 1);
  }, []);

  return (
    <>
      <Modal
        show={openUpdateModal}
        onHide={handleUpdateModal}
        backdrop="static"
        keyboard={false}
      >
        <Form onSubmit={handleUpdateOrganisasi}>
          <Modal.Header>
            <Modal.Title>Ubah Organisasi</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form.Group className="mb-3 form-label-group">
              <Form.Control
                ref={innerRef}
                type="text"
                id="organisasi"
                name="organisasi"
                aria-describedby="passwordHelpBlock"
                value={inputData.organisasi}
                onChange={handleChangeInputData}
              />
              <Form.Label htmlFor="organisasi">Organisasi</Form.Label>
              <span className="text-danger">
                <i>{error?.organisasi ?? ""}</i>
              </span>
            </Form.Group>

            <Form.Group className="mb-3 form-label-group">
              <Form.Control
                type="text"
                id="singkatan"
                name="singkatan"
                aria-describedby="passwordHelpBlock"
                value={inputData.singkatan}
                onChange={handleChangeInputData}
              />
              <Form.Label htmlFor="singkatan">Singkatan</Form.Label>
              <span className="text-danger">
                <i>{error?.singkatan ?? ""}</i>
              </span>
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>Keterangan</Form.Label>
              <CKEditor
                editor={ClassicEditor}
                data={keterangan}
                onChange={handleInputKeterangan}
                onReady={(editor) => {
                  editor.editing.view.change((writer) => {
                    writer.setStyle(
                      "height",
                      "200px",
                      editor.editing.view.document.getRoot()
                    );
                  });
                }}
              />
              <span className="text-danger">
                <i>{error?.keterangan ?? ""}</i>
              </span>
            </Form.Group>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="primary" type="submit" disabled={loading}>
              {loading ? (
                <Loader type="ThreeDots" color="#fff" height={10} />
              ) : (
                <>
                  <Edit3 size={14} className="mr-1" /> Ubah
                </>
              )}
            </Button>
            <Button variant="secondary" onClick={handleUpdateModal}>
              <XCircle size={14} className="mr-1" />
              Tutup
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
    </>
  );
};

export default Ubah;
