export const product = [
    {
        "id": 1,
        "title": "iPhone 9",
        "description": "An apple mobile which is nothing like apple",
        "price": 549,
        "stock": 94,
        "brand": "Apple",
        "category": "smartphones",
        "store": "JABODETABEK"
    },
    {
        "id": 2,
        "title": "Samsung Galaxy A34",
        "description": "An samsung mobile which is nothing like samsung",
        "price": 409,
        "stock": 101,
        "brand": "Samsung",
        "category": "smartphones",
        "store": "JABODETABEK"
    },
    {
        "id": 3,
        "title": "MacBook Pro",
        "description": "An laptop office which is support your work",
        "price": 1109,
        "stock": 40,
        "brand": "MacBook",
        "category": "laptop",
        "store": "JABODETABEK"
    },
    {
        "id": 4,
        "title": "Asus ZenBook X541",
        "description": "An laptop office which is support your work",
        "price": 1022,
        "stock": 60,
        "brand": "ASUS",
        "category": "laptop",
        "store": "JABODETABEK"
    },
]