import React, { useCallback, useState, useEffect } from "react";
import { useLocation } from "react-router-dom";
import canvasDatagrid from "canvas-datagrid";
import XLSX, { read, utils, writeFileXLSX, writeFile } from "xlsx";
import Spreadsheet from "react-spreadsheet";
import { OutTable, ExcelRenderer } from "react-excel-renderer";
import { Col, Row, Table } from "react-bootstrap";
import Card from "../../App/components/MainCard";
import { product } from "./data";

const ExcelPage = () => {
  const location = useLocation();
  const [cols, setCols] = useState([]);
  const [rows, setRows] = useState([]);
  const [sampleXlsx, setSampleXlsx] = useState([]);

  //spreadsheet state dan function
  const [url, setUrl] = React.useState("https://sheetjs.com/pres.numbers");
  const [done, setDone] = React.useState(false);
  const ref = React.useRef(); // ref to DIV container
  const set_url = React.useCallback((evt) => setUrl(evt.target.value));
  const [cdg, setCdg] = React.useState(null); // reference to grid object

  const [data, setData] = useState([
    [
      { value: "Vanilla" },
      { value: "Chocolate" },
      { value: "Ovaltine" },
      { value: "Matcha" },
    ],
    [
      { value: "Strawberry" },
      { value: "Cookies" },
      { value: "Strawberry" },
      { value: "Cookies" },
    ],
  ]);

  const fileHandler = (event) => {
    let fileObj = event.target.files[0];

    //just pass the fileObj as parameter
    ExcelRenderer(fileObj, (err, resp) => {
      if (err) {
        // console.log(err)
      } else {
        setCols(resp.cols);
        setRows(resp.rows);
      }
    });
  };

  /* Fetch and update the state once */
  useEffect(() => {
    (async () => {
      /* Download file */
      const f = await (
        await fetch("https://sheetjs.com/pres.xlsx")
      ).arrayBuffer();
      const wb = read(f); // parse the array buffer
      const ws = wb.Sheets[wb.SheetNames[0]]; // get the first worksheet
      const data = utils.sheet_to_json(ws); // generate objects
      setSampleXlsx(data); // update state
    })();
  }, []);

  /* get state data and export to XLSX */
  const exportFile = useCallback(() => {
    const ws = utils.json_to_sheet(sampleXlsx);
    const wb = utils.book_new();
    utils.book_append_sheet(wb, ws, "Data");
    writeFileXLSX(wb, "sampelTest.xlsx");
  }, [sampleXlsx]);

  return (
    <Row>
      <Col>
        <Card title="Excel Dengan Data Array" isOption>
          <p>State From URL: {location?.state?.fromUrl}</p>
          <p>State To URL: {location?.state?.toUrl}</p>
          <Table responsive bordered striped>
            <thead>
              <tr>
                <th>No</th>
                <th>Brand</th>
                <th>Product</th>
                <th>Deskripsi</th>
                <th>Harga</th>
                <th>Stok</th>
                <th>Kategori</th>
                <th>Tersedia</th>
              </tr>
            </thead>
            <tbody>
              {product?.map((item, i) => (
                <tr key={i}>
                  <td>{i + 1}</td>
                  <td>{item?.brand}</td>
                  <td>{item?.title}</td>
                  <td>{item?.description}</td>
                  <td>{item?.price}</td>
                  <td>{item?.stock}</td>
                  <td>{item?.category}</td>
                  <td>{item?.store}</td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Card>
        <Card title="Excel Dengan Data Array" isOption>
          <div>
            <Spreadsheet data={data} onChange={setData} />
          </div>
        </Card>
        <Card title="Excel Dengan Upload File" isOption>
          <div>
            <input
              type="file"
              onChange={fileHandler}
              style={{ padding: "10px" }}
            />
          </div>
          <div>
            <OutTable
              data={rows}
              columns={cols}
              tableClassName="ExcelTable2007"
              tableHeaderRowClass="heading"
            />
          </div>
        </Card>
        <Card title="Sampel Excel dengan fetch API" isOption>
          <div>
            <table>
              <thead>
                <th>Name</th>
                <th>Index</th>
              </thead>
              <tbody>
                {sampleXlsx?.map((row) => (
                  <tr>
                    <td>{row.Name}</td>
                    <td>{row.Index}</td>
                  </tr>
                ))}
              </tbody>
            </table>
            <button onClick={exportFile}>Export XLSX</button>
          </div>
        </Card>
        <Card title="Excel Fetch API" isOption>
          <div height={300} width={300} ref={ref} />
          {!done && (
            <>
              <b>URL: </b>
              <input type="text" value={url} onChange={set_url} size="50" />
              <br />
              <button
                onClick={async () => {
                  /* fetch and parse workbook */
                  const wb = XLSX.read(await (await fetch(url)).arrayBuffer());
                  const ws = wb.Sheets[wb.SheetNames[0]];
                  const data = XLSX.utils.sheet_to_json(ws, { header: 1 });

                  /* set up grid and load data */
                  if (!cdg)
                    setCdg(canvasDatagrid({ parentNode: ref.current, data }));
                  else cdg.data = data;
                  setDone(true);
                }}
              >
                <b>Fetch!</b>
              </button>
            </>
          )}
        </Card>
      </Col>
    </Row>
  );
};

export default ExcelPage;
