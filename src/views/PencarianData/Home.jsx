import React, { useRef, useEffect, useState } from "react";
import Aux from "../../hoc/_Aux";
import {
  Button,
  Col,
  Form,
  OverlayTrigger,
  Row,
  Table,
  Tooltip,
} from "react-bootstrap";
import Card from "../../App/components/MainCard";
import baseApi from "../../services/baseApi";
import { Search } from "react-feather";
import Loader from "react-loader-spinner";
import { toast } from "react-toastify";
import avatar2 from "../../assets/images/user/avatar-2.jpg";
import { sourceIcon } from "../../App/components/Icon";
import InfoMetaDataFileModal from "../../App/components/InfoMetaDataFile";
import moment from "moment";
import { localDisk } from "../../services/Utilities";
import {
  getFormatEkstensi,
  getFormatData,
} from "../../App/components/FunctionFormatData";

const Home = () => {
  const [hasilPencarian, setHasilPencarian] = useState({});
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState([]);
  const [inputData, setInputData] = useState({
    nama_data: "",
    sumber_data: "",
    deskripsi_data: "",
    ekstensi_data: "",
    topik: "",
    tag: "",
    organisasi: "",
  });
  const [dataRiwayat, setDataRiwayat] = useState({});
  const [openInfoMetaDataFileModal, setOpenInfoMetaDataFileModal] =
    useState(false);

  const id = require("moment/locale/id");
  moment.updateLocale("id", id);

  const innerRef = useRef();

  const handleChangeInputData = (e) => {
    e.persist();
    setInputData({ ...inputData, [e.target.name]: e.target.value });
  };

  const handleCariData = (e) => {
    e.preventDefault();
    setLoading(true);

    if (
      inputData.nama_data.length === 0 &&
      inputData.sumber_data.length === 0 &&
      inputData.deskripsi_data.length === 0 &&
      inputData.ekstensi_data.length === 0 &&
      inputData.topik.length === 0 &&
      inputData.tag.length === 0 &&
      inputData.organisasi.length === 0
    ) {
      toast.warning("Masukan kata kunci pencarian", {
        position: toast.POSITION.TOP_CENTER,
      });
      setLoading(false);
      return false;
    }

    const formData = new FormData();

    formData.append(
      "nama_data",
      inputData.nama_data ? inputData.nama_data : ""
    );
    formData.append(
      "sumber_data",
      inputData.sumber_data ? inputData.sumber_data : ""
    );
    formData.append(
      "deskripsi_data",
      inputData.deskripsi_data ? inputData.deskripsi_data : ""
    );
    formData.append(
      "ekstensi_data",
      inputData.ekstensi_data ? inputData.ekstensi_data : ""
    );
    formData.append("topik", inputData.topik ? inputData.topik : "");
    formData.append("tag", inputData.tag ? inputData.tag : "");
    formData.append(
      "organisasi",
      inputData.organisasi ? inputData.organisasi : ""
    );

    baseApi
      .post(`/pencarian-data`, formData)
      .then((response) => {
        if (response?.status === 200) {
          if (response?.data?.result.length === 0) {
            toast.warning(response?.data?.message, {
              position: toast.POSITION.TOP_RIGHT,
            });
          } else {
            toast.success(response?.data?.message, {
              position: toast.POSITION.TOP_RIGHT,
            });
          }

          setHasilPencarian(response?.data?.result);
          setLoading(false);
        } else {
          setLoading(false);
          // console.log("response", response);
        }
      })
      .catch((error) => {
        setLoading(false);
        setError(error.response?.data?.errors);

        toast.error(error.response?.data?.message, {
          position: toast.POSITION.TOP_RIGHT,
        });
      });
  };

  /*eslint-disable */
  useEffect(() => {
    setTimeout(() => {
      innerRef.current.focus();
    }, 1);
  }, []);

  return (
    <>
      <Aux>
        <Row>
          <Col>
            <Card title="Cari Data" isOption>
              <Form onSubmit={handleCariData}>
                <Form.Group className="mb-1 form-label-group">
                  <Form.Control
                    ref={innerRef}
                    placeholder="Nama Data"
                    className="form-control"
                    type="text"
                    id="nama_data"
                    name="nama_data"
                    value={inputData.nama_data}
                    onChange={handleChangeInputData}
                  />
                  <Form.Label htmlFor="nama_data">Nama Data</Form.Label>
                  <span className="text-danger">
                    <i>{error?.nama_data ?? ""}</i>
                  </span>
                </Form.Group>

                <Form.Group className="mb-1 form-label-group">
                  <Form.Control
                    placeholder="Sumber Data"
                    className="form-control"
                    type="text"
                    id="sumber_data"
                    name="sumber_data"
                    value={inputData.sumber_data}
                    onChange={handleChangeInputData}
                  />
                  <Form.Label htmlFor="sumber_data">Sumber Data</Form.Label>
                  <span className="text-danger">
                    <i>{error?.sumber_data ?? ""}</i>
                  </span>
                </Form.Group>

                <Form.Group className="mb-1 form-label-group">
                  <Form.Control
                    placeholder="Deskripsi Data"
                    className="form-control"
                    type="text"
                    id="deskripsi_data"
                    name="deskripsi_data"
                    value={inputData.deskripsi_data}
                    onChange={handleChangeInputData}
                  />
                  <Form.Label htmlFor="deskripsi_data">
                    Deskripsi Data
                  </Form.Label>
                  <span className="text-danger">
                    <i>{error?.deskripsi_data ?? ""}</i>
                  </span>
                </Form.Group>

                <Form.Group className="mb-1 form-label-group">
                  <Form.Control
                    placeholder="Ekstensi Data"
                    className="form-control"
                    type="text"
                    id="ekstensi_data"
                    name="ekstensi_data"
                    value={inputData.ekstensi_data}
                    onChange={handleChangeInputData}
                  />
                  <Form.Label htmlFor="ekstensi_data">
                    Ekstensi Data (jpg, xls, pptx)
                  </Form.Label>
                  <span className="text-danger">
                    <i>{error?.ekstensi_data ?? ""}</i>
                  </span>
                </Form.Group>

                <Form.Group className="mb-1 form-label-group">
                  <Form.Control
                    placeholder="Topik"
                    className="form-control"
                    type="text"
                    id="topik"
                    name="topik"
                    value={inputData.topik}
                    onChange={handleChangeInputData}
                  />
                  <Form.Label htmlFor="topik">Topik</Form.Label>
                  <span className="text-danger">
                    <i>{error?.topik ?? ""}</i>
                  </span>
                </Form.Group>

                <Form.Group className="mb-1 form-label-group">
                  <Form.Control
                    placeholder="Tag"
                    className="form-control"
                    type="text"
                    id="tag"
                    name="tag"
                    value={inputData.tag}
                    onChange={handleChangeInputData}
                  />
                  <Form.Label htmlFor="tag">Tag</Form.Label>
                  <span className="text-danger">
                    <i>{error?.tag ?? ""}</i>
                  </span>
                </Form.Group>

                <Form.Group className="mb-1 form-label-group">
                  <Form.Control
                    placeholder="Organisasi"
                    className="form-control"
                    type="text"
                    id="organisasi"
                    name="organisasi"
                    value={inputData.organisasi}
                    onChange={handleChangeInputData}
                  />
                  <Form.Label htmlFor="organisasi">Organisasi</Form.Label>
                  <span className="text-danger">
                    <i>{error?.organisasi ?? ""}</i>
                  </span>
                </Form.Group>

                <Button variant="primary" type="submit" disabled={loading}>
                  {loading ? (
                    <Loader type="ThreeDots" color="#fff" height={10} />
                  ) : (
                    <>
                      <Search size={14} className="mr-1" /> Cari
                    </>
                  )}
                </Button>
              </Form>
              {hasilPencarian && hasilPencarian.length > 0 && (
                // <ResultPencarian resultTrxData={hasilPencarian} />
                <Table responsive hover>
                  <tbody>
                    {hasilPencarian?.map((item, index) => (
                      <tr className="unread" key={index} value={item.id}>
                        <td>
                          <img
                            className="rounded-circle"
                            style={{ width: "40px" }}
                            src={avatar2}
                            alt="activity-user"
                          />
                        </td>
                        <td>
                          <h6 className="mb-1 text-primary">
                            {item.nama_data}
                          </h6>
                          <p className="m-0">{item.created_by}</p>
                        </td>
                        <td>
                          <h6 className="text-muted">
                            <i className="fa fa-circle text-c-green f-10 m-r-15" />
                            {moment(item.created_at).format(
                              "D MMMM YYYY H:mm:ss"
                            )}
                          </h6>
                        </td>
                        <td>
                          {item.is_shared === true ? (
                            <span className="label theme-bg2 text-white f-12">
                              Dikelola Bersama
                            </span>
                          ) : (
                            <span className="label theme-bg text-white f-12">
                              Tidak Dikelola Bersama
                            </span>
                          )}
                        </td>
                        <td>
                          <OverlayTrigger
                            overlay={<Tooltip>Properti Data</Tooltip>}
                          >
                            <a
                              href="#"
                              onClick={() =>
                                setOpenInfoMetaDataFileModal(
                                  !openInfoMetaDataFileModal,
                                  setDataRiwayat(item)
                                )
                              }
                            >
                              {getFormatData(item.format_data)}
                              {/* <img
                      className="img-responsive mb-1"
                      width="30"
                      src={sourceIcon.formatDataDocument}getFormatData
                      alt="alert"
                    /> */}
                            </a>
                          </OverlayTrigger>

                          <OverlayTrigger
                            overlay={<Tooltip>Lihat Data</Tooltip>}
                          >
                            {item.format_data === "URL" ? (
                              <a href={item.url_upload} target="_blank">
                                <img
                                  className="img-responsive mb-1 mx-2"
                                  width="30"
                                  src={sourceIcon.formatEkstensiURL}
                                  alt={item.ekstensi_data}
                                />
                              </a>
                            ) : (
                              <a
                                href={`${localDisk}/${item.url_file_upload}`}
                                target="_blank"
                              >
                                {getFormatEkstensi(item.ekstensi_data)}
                                {/* <img
                                  className="img-responsive mb-1"
                                  width="30"
                                  alt={item.ekstensi_data}
                                /> */}
                              </a>
                            )}
                          </OverlayTrigger>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </Table>
              )}
            </Card>
          </Col>
        </Row>
      </Aux>

      <InfoMetaDataFileModal
        title="Properti Data"
        value={dataRiwayat}
        namaState={openInfoMetaDataFileModal}
        namaSetState={setOpenInfoMetaDataFileModal}
        buttonCancel="Tutup"
        size="md" //lg,xl,sm,md
      />
    </>
  );
};

export default Home;
