import React, { useState } from "react";

const SearchFilter = ({ onFilterChange }) => {
  const [filterValue, setFilterValue] = useState("");

  const handleFilterChange = (event) => {
    const value = event.target.value;
    setFilterValue(value);
    onFilterChange(value); // Memanggil fungsi onFilterChange pada komponen Home dengan nilai filter baru
  };

  return (
    <div>
      <input
        type="text"
        placeholder="Filter data..."
        value={filterValue}
        onChange={handleFilterChange}
      />
    </div>
  );
};

export default SearchFilter;
