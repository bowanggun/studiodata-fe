import React from "react";

const CollapseExpand = ({ onClick, isExpanded }) => {
  const handleClick = () => {
    onClick(!isExpanded);
  };

  return (
    <div onClick={handleClick}>
      {isExpanded ? (
        <span className="collapse-expand-icon">-</span>
      ) : (
        <span className="collapse-expand-icon">+</span>
      )}
    </div>
  );
};

export default CollapseExpand;
