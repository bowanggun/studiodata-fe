import React, { useEffect, useState } from "react";
import { toast } from "react-toastify";
import Iframe from "react-iframe";
import Aux from "../../hoc/_Aux";
// import Card from "../../App/components/MainCard";
import baseApi from "../../services/baseApi";
import { Col, Row, Tab, Tabs } from "react-bootstrap";

const Home = () => {
  const [resultDashboard, setResultDashboard] = useState([]);
  const [loading, setLoading] = useState(false);

  const fetchDataDashboard = () => {
    baseApi
      .get(`/mst-dashboard?paging=false&active=active`)
      .then((response) => {
        if (response?.status === 200) {
          setResultDashboard(response?.data?.result);
          setLoading(false);
        }
      })
      .catch((error) => {
        setLoading(false);
        toast.warning("Terjadi kesalahan", {
          position: toast.POSITION.TOP_RIGHT,
        });
        // console.log(error);
      });
  };

  /*eslint-disable */
  useEffect(() => {
    fetchDataDashboard();
  }, []);

  return (
    <>
      <Aux>
        <Row>
          <Col>
            <Tabs defaultActiveKey="APBD" className="">
              {resultDashboard?.map((item, index) => (
                <Tab
                  eventKey={item.nama_dashboard}
                  title={item.nama_dashboard}
                  key={index}
                >
                  <Iframe
                    // allowFullScreen
                    // display="block"
                    // position="relative"
                    id={index}
                    className=""
                    url={item.link}
                    width="100%"
                    height="700"
                    frameBorder={0}
                    styles={0}
                  />
                </Tab>
              ))}
            </Tabs>
          </Col>
        </Row>
      </Aux>
    </>
  );
};

export default Home;
