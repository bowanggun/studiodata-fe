import React, { useRef, useEffect } from "react";
import { Modal, Form, Button } from "react-bootstrap";
import { PlusCircle, XCircle } from "react-feather";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import baseApi from "../../services/baseApi";
import { toast } from "react-toastify";
import Loader from "react-loader-spinner";

const Tambah = (props) => {
  const {
    openTambahModal,
    setOpenTambahModal,
    handleTambahModal,
    inputData,
    setInputData,
    handleChangeInputData,
    error,
    setError,
    keterangan,
    setKeterangan,
    handleInputKeterangan,
    loading,
    setLoading,
    fetchData,
    refGrupDokId,
  } = props;

  const innerRef = useRef();

  const handleInputJenisDokumen = (e) => {
    e.preventDefault();
    setLoading(true);

    const formData = new FormData();

    formData.append(
      "jenis_dokumen",
      inputData.jenisDokumen ? inputData.jenisDokumen : ""
    );
    formData.append("ref_grup_dok_id", refGrupDokId);
    formData.append("keterangan", keterangan);

    baseApi
      .post(`/jenis-dok`, formData)
      .then((response) => {
        if (response?.status === 200) {
          fetchData();
          toast.success(response?.data?.message, {
            position: toast.POSITION.TOP_RIGHT,
          });
          setOpenTambahModal(false);
          setKeterangan("");
          setInputData({});
          setLoading(false);
        } else {
          // console.log("response", response);
          setLoading(false);
        }
      })
      .catch((error) => {
        setLoading(false);
        setError(error.response?.data?.errors);

        toast.error(error.response?.data?.message, {
          position: toast.POSITION.TOP_RIGHT,
        });
      });
  };

  useEffect(() => {
    setTimeout(() => {
      innerRef.current.focus();
      setKeterangan("");
      setInputData({});
    }, 1);
  }, [setInputData, setKeterangan]);

  return (
    <>
      <Modal
        show={openTambahModal}
        onHide={handleTambahModal}
        backdrop="static"
        keyboard={false}
      >
        <Form onSubmit={handleInputJenisDokumen}>
          <Modal.Header>
            <Modal.Title>Tambah Jenis Dokumen</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form.Group className="mb-3 form-label-group">
              <Form.Control
                ref={innerRef}
                placeholder="Jenis Dokumen"
                className="form-control"
                type="text"
                id="jenisDokumen"
                name="jenisDokumen"
                aria-describedby="passwordHelpBlock"
                value={inputData.jenisDokumen}
                onChange={handleChangeInputData}
              />
              <Form.Label htmlFor="jenisDokumen">Jenis Dokumen</Form.Label>
              <span className="text-danger">
                <i>{error?.jenis_dokumen ?? ""}</i>
              </span>
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label htmlFor="keterangan">Keterangan</Form.Label>
              <CKEditor
                editor={ClassicEditor}
                data={keterangan}
                onChange={handleInputKeterangan}
                onReady={(editor) => {
                  editor.editing.view.change((writer) => {
                    writer.setStyle(
                      "height",
                      "200px",
                      editor.editing.view.document.getRoot()
                    );
                  });
                }}
              />
              <span className="text-danger">
                <i>{error?.keterangan ?? ""}</i>
              </span>
            </Form.Group>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="primary" type="submit" disabled={loading}>
              {loading ? (
                <Loader type="ThreeDots" color="#fff" height={10} />
              ) : (
                <>
                  <PlusCircle size={14} className="mr-1" /> Simpan
                </>
              )}
            </Button>
            <Button variant="secondary" onClick={handleTambahModal}>
              <XCircle size={14} className="mr-1" />
              Tutup
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
    </>
  );
};

export default Tambah;
