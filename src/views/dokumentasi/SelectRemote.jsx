import axios from "axios";
import React, { useState, useCallback } from "react";
import AsyncSelect from "react-select/async";

const SelectRemote = () => {
  const [data, setData] = useState([]);
  const [value, setValue] = useState([]);
  const [error, setError] = useState("");
  const [loading, setLoading] = useState(false);

  const options = {
    method: "GET",
    url: "https://moviesdatabase.p.rapidapi.com/titles/x/upcoming",
    headers: {
      "X-RapidAPI-Key": "a354fb4e36msh40ac7be70f9f8ecp178c2fjsn4760105bfb88",
      "X-RapidAPI-Host": "moviesdatabase.p.rapidapi.com",
    },
  };

  const fetchData = async (inputValue) => {
    setLoading(true);
    try {
      // console.log("inputValue di fetch", inputValue);
      const response = await axios.request(options);
      if (response?.status === 200) {
        setData(response?.data?.results);
        setLoading(false);
        setError("");
      } else {
        alert("gagal menampilkan data");
      }
    } catch (error) {
      // console.log(error);
    }
  };

  const optionsMovies = data?.map((item) => ({
    value: item.id,
    label: item.titleText.text,
  }));

  const filterMovies = (inputValue) => {
    if (inputValue?.length >= 3) {
      setError("");
      return optionsMovies.filter((i) =>
        i.label.toLowerCase().includes(inputValue.toLowerCase())
      );
    }
  };

  const promiseOptions = (inputValue) =>
    new Promise((resolve) => {
      setTimeout(() => {
        if (inputValue?.length < 3) {
          resolve(filterMovies(null));
        } else if (inputValue?.length === 3 || inputValue?.length > 3) {
          resolve(filterMovies(inputValue));
        }
      }, 1000);
    });

  const loadOptionsWithCallback = (inputValue, callback) => {
    if (inputValue?.length < 3) {
      callback(filterMovies(null));
    } else if (inputValue?.length === 3 || inputValue?.length > 3) {
      callback(filterMovies(inputValue));
    }
  };

  const handleValueMovies = (e) => {
    const data = e?.map((item) => {
      return item.label;
    });
    setValue(data);
    // console.log("valLength", e);
  };

  const handleInputChangeMovies = (inputValue) => {
    // console.log("inputValue", inputValue);
    if (inputValue?.length < 3) {
      return setError("Ketikan minimal 3 sampai 4 karakter");
    } else {
      return fetchData(inputValue);
    }
  };

  // useEffect(() => {
  //     fetchData()
  // }, [])

  return (
    <div>
      <p>Movies:</p>
      <ul>
        {value?.map((item) => (
          <li>{item}</li>
        ))}
      </ul>
      <span className="text-danger">{error ?? ""}</span>
      <AsyncSelect
        isMulti
        cacheOptions
        defaultOptions
        loadOptions={promiseOptions}
        onChange={handleValueMovies}
        onInputChange={handleInputChangeMovies}
        // onKeyDown={fetchData}
        noOptionsMessage={() => "Data tidak ditemukan.."}
      />
    </div>
  );
};

export default SelectRemote;
