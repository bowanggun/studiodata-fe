/*eslint-disable */
import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import { toast } from "react-toastify";
// import { RemoteTable } from "../../App/components/RemoteTable"
// import RemotePagination from './RemotePagination'
import BootstrapTable from "react-bootstrap-table-next";
import { Badge, Button } from "react-bootstrap";
import Pagination from "react-bootstrap/Pagination";
import PageItem from "react-bootstrap/PageItem";
// import Loader from 'react-loader-spinner';
import Card from "../../App/components/MainCard";
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
import "react-bootstrap-table2-toolkit/dist/react-bootstrap-table2-toolkit.min.css";
import "react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css";

const apiSimarin = "https://api-testsimarinv2.brin.go.id/api";
const token = `eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiI5NzViZGViNC04MmJmLTQ0ZjQtYTg0Ny1lODY2ZTQzN2M1YzEiLCJqdGkiOiIwMzFlOTdlMzFmNWM2OGI4MjUyYzFjY2Y4NmUwMjEwNGU1ZTVkNTI2YWJmN2JiMDNhMjdhZDYwMmRmNjkyOTFhOWZlMDFlNThkYTIyNWJkOSIsImlhdCI6MTY4Nzk2MTUxMy42NDUxODYsIm5iZiI6MTY4Nzk2MTUxMy42NDUxOSwiZXhwIjoxNjg3OTkwMzEzLjY0MDkyOCwic3ViIjoiMjUiLCJzY29wZXMiOltdfQ.nIUCSWggne5gGPr73KcHwziFcM6JZPNnJDA08ny8LVMig72j-2KAZf0oZmTxieVRDmVGJhbUMuNmaLHRFnsxK4LvmF4lafw6VVEZnMCw-YrTZZiRBI9O_-sWGwRf_eBStVo71-6aJa8i-EKoL1mGhldbLBYt51WjKn9oIpZ5QhYE1835y_yxoPhaR3z71hF8rDlhxi5WKwCbGO5PJvyEB4RvYQ5DqQae3LDpweDG0BY_CMDj1UqmUy0bKgUj4nQaxokIgnD7PS4NxNEDTjrJAZSc75Gc-o-hONsablVQAcA-l_o-IsNRMuWtvcyJYux4IO3ADZD-c56f9V_oJix3tHHaDvR3Sm0qyvydKNduiI48Ttl31e3VTX5_m1Omm3NYkTMJV3ypcK-Ud5pQJiRKH2cfoz2GZC7oN2SCfrvrD_J8h6kVXU9iUihqU3HkuJgQHqowvwP0LN5El4P1OQHk0lUGbwdvkMBLEGMJycKG55906fYvSoMEK3zkBUKVV8Ro4TjvoVSsl4v69CcfEIwHx4YTpajzFoWDggMzIPKjBe-iylEzFLV6_deccSIYe2mM8aeaJzNBNRzuGZ_CvvkN32niOqJYRVSSoamVGTUk4bQMLSo6AOqHn8GHkD_xbcLmydYekJ305jGmESLhkYw8VdxWOa5VL24hJKGwfNIDEEs`;

const App = () => {
  const [data, setData] = useState([]);
  const [movies, setMovies] = useState([]);
  const [keyword, setKeyword] = useState("");
  const [loading, setLoading] = useState(false);

  //state remotetabel
  const [page, setPage] = useState(1);
  const [totalRows, setTotalRows] = useState(0);
  const [sizePerPage, setSizePerPage] = useState(10);
  const [paginationParameter, setPaginationParameter] = useState("");
  const [lastPage, setLastPage] = useState(null);

  const options = {
    method: "GET",
    url: "https://moviesdatabase.p.rapidapi.com/titles/x/upcoming",
    headers: {
      "X-RapidAPI-Key": "a354fb4e36msh40ac7be70f9f8ecp178c2fjsn4760105bfb88",
      "X-RapidAPI-Host": "moviesdatabase.p.rapidapi.com",
    },
  };

  const getHeaders = {
    headers: {
      "X-RapidAPI-Key": "a354fb4e36msh40ac7be70f9f8ecp178c2fjsn4760105bfb88",
      "X-RapidAPI-Host": "moviesdatabase.p.rapidapi.com",
    },
  };

  const fetchData = async () => {
    setLoading(true);
    try {
      const response = await axios.request(options);
      if (response?.status === 200) {
        setMovies(response?.data?.results);
        setPaginationParameter(response?.data);
        setSizePerPage(response?.data?.entries);
        setLoading(false);
      } else {
        alert("gagal menampilkan data");
      }
    } catch (error) {
      // console.log(error);
    }
  };

  const fetchDataSimarin = () => {
    setLoading(true);
    let apiTubel;
    if (keyword === "" || keyword === undefined || keyword === null) {
      apiTubel = `peg-tubel`;
    } else if (keyword !== "" || keyword !== undefined || keyword !== null) {
      apiTubel = `peg-tubel?q=${keyword}`;
    }
    axios
      .get(`${apiSimarin}/${apiTubel}`, {
        headers: {
          Authorization: `Bearer ${token}`,
          Accept: "application/json",
          ContentType: "application/json",
        },
      })
      .then((response) => {
        if (response?.status === 200) {
          setData(response?.data?.data?.data);
          setTotalRows(response?.data?.data?.total);
          setPaginationParameter(response?.data?.data);
          setLastPage(response?.data?.data?.last_page);
          setLoading(false);
        } else {
          toast.error(response?.data?.message, {
            position: toast.POSITION.TOP_RIGHT,
          });
          setLoading(false);
        }
      })
      .catch((err) => {
        toast.error(err?.response?.data?.message, {
          position: toast.POSITION.TOP_RIGHT,
        });
        setLoading(false);
      });
  };

  const onChangePage = (page) => async (e) => {
    setLoading(true);
    setPage(page);
    try {
      // const response = await axios.get(`https://moviesdatabase.p.rapidapi.com/titles/x/upcoming?=${page}`, getHeaders)
      const response = await axios.get(`${apiSimarin}/peg-tubel?page=${page}`, {
        headers: {
          Authorization: `Bearer ${token}`,
          Accept: "application/json",
          ContentType: "application/json",
        },
      });
      if (response?.status === 200) {
        setData(response?.data?.data?.data);
        setPaginationParameter(response?.data?.data);
        setSizePerPage(response?.data?.data?.per_page);
        setLoading(false);
      } else {
        alert("gagal menampilkan data");
      }
    } catch (error) {
      // console.log(error);
    }
  };

  function prevPages(hal) {
    const listPages = [];
    // if (hal - 1 > 1) {
    //     listPages.push(<PageItem onClick={onChangePage(hal - 2)}>{hal - 2}</PageItem>)
    //     listPages.push(<PageItem onClick={onChangePage(hal - 1)}>{hal - 1}</PageItem>)
    // } else if (hal - 1 == 1) {
    //     listPages.push(<PageItem onClick={onChangePage(hal - 1)}>{hal - 1}</PageItem>)
    // } else {
    //     listPages.push()
    // }
    listPages.push(
      <PageItem onClick={onChangePage(hal - 1)}>{hal - 1}</PageItem>
    );
    return listPages;
  }

  function nextPages(hal) {
    const listPages = [];
    // if (paginationParameter?.last_page - hal > 1) {
    //     listPages.push(<PageItem onClick={onChangePage(hal + 1)}>{hal + 1}</PageItem>)
    //     listPages.push(<PageItem onClick={onChangePage(hal + 2)}>{hal + 2}</PageItem>)
    // } else if (paginationParameter?.last_page - hal == 1) {
    //     listPages.push(<PageItem onClick={onChangePage(hal + 1)}>{hal + 1}</PageItem>)
    // } else {
    //     listPages.push()
    // }
    listPages.push(
      <PageItem onClick={onChangePage(hal + 1)}>{hal + 1}</PageItem>
    );
    return listPages;
  }

  const handleTableChangeSimarin = async (type, { page, sizePerPage }) => {
    setLoading(true);
    const response = await axios.get(`${apiSimarin}/peg-tubel?page=${page}`, {
      headers: {
        Authorization: `Bearer ${token}`,
        Accept: "application/json",
        ContentType: "application/json",
      },
    });
    if (response?.status === 200) {
      const getData = response?.data?.data?.data;
      setData(getData);
      setPage(page);
      setSizePerPage(sizePerPage);
      setLoading(false);
    } else {
      toast.error(response?.data?.message, {
        position: toast.POSITION.TOP_RIGHT,
      });
      setLoading(false);
    }
  };

  // const handleTableChange = async (type, { page, sizePerPage }) => {
  //     setLoading(true)
  //       const response = await axios.get(`https://moviesdatabase.p.rapidapi.com/titles/x/upcoming?page=${page}`, {
  //         headers: {
  //             'X-RapidAPI-Key': 'a354fb4e36msh40ac7be70f9f8ecp178c2fjsn4760105bfb88',
  //             'X-RapidAPI-Host': 'moviesdatabase.p.rapidapi.com'
  //         }
  //       })
  //       if (response?.status === 200) {
  //         setData(response?.data?.results)
  //         setPage(page)
  //         setSizePerPage(sizePerPage)
  //         setLoading(false)
  //       } else {
  //         toast.error(response?.data?.message, {
  //             position: toast.POSITION.TOP_RIGHT,
  //         });
  //         setLoading(false)
  //       }
  // }

  /*eslint-disable */
  useEffect(() => {
    fetchDataSimarin();
    fetchData();
  }, []);

  /** get number increament */
  //const numberPage = Array.from({length: totalRows}, (_, index) => index + 1)

  // const indexOfLastRecord = page * sizePerPage; // 10 => 20 => 30
  // const indexOfFirstRecord = indexOfLastRecord - sizePerPage; // 0 => 10 => 20
  // const record = 10 - 10

  // const currentRecords = data?.slice(indexOfFirstRecord, indexOfLastRecord);
  // const nPages = Math?.ceil(data?.length / sizePerPage)

  const columns = [
    {
      dataField: "",
      text: "",
      formatter: (cell, row, rowIndex, extraData) => (
        <div>{page * sizePerPage - sizePerPage + (rowIndex + 1)}</div>
      ),
    },
    {
      dataField: "originalTitleText.text",
      text: "Titile",
      sort: true,
    },
    {
      dataField: "releaseDate.year",
      text: "Tahun Realease",
      sort: true,
    },
    {
      dataField: "titleType.categories.value",
      text: "Kategori",
      sort: true,
    },
  ];

  const columnsSimarin = [
    {
      dataField: "",
      text: "",
      formatter: (cell, row, rowIndex, extraData) => (
        <div>{page * sizePerPage - sizePerPage + (rowIndex + 1)}</div>
      ),
    },
    {
      dataField: "",
      text: "",
      formatter: (cell, row, rowIndex, extraData) => (
        <div>
          <Link
            to={{
              pathname: "/app-detail",
              state: {
                fromUrl: "app",
                toUrl: "detail",
              },
            }}
          >
            <Button className="btn btn-sm">kirim</Button>
          </Link>
        </div>
      ),
    },
    {
      dataField: "tahun",
      text: "Tahun",
      sort: true,
    },
    {
      dataField: "idp_pegawai",
      text: "Idp_Pegawai",
      sort: true,
    },
    {
      dataField: "nip",
      text: "NIP",
      sort: true,
    },
    {
      dataField: "nama",
      text: "Nama Pegawai",
      sort: true,
    },
    {
      dataField: "hukdis",
      text: "hukdis",
      sort: true,
      formatter: (cell, row, rowIndex, extraData) => (
        <div>
          {row?.hukdis === 1 ? (
            <Badge variant="danger">Ya</Badge>
          ) : (
            <Badge variant="secondary">Tidak</Badge>
          )}
        </div>
      ),
    },
    {
      dataField: "kawasan",
      text: "kawasan",
      sort: true,
      formatter: (cell, row, rowIndex, extraData) => (
        <div>{row?.kawasan ?? "-"}</div>
      ),
    },
    {
      dataField: "kondisi",
      text: "kondisi",
      sort: true,
      formatter: (cell, row, rowIndex, extraData) => (
        <div>{row?.kondisi ?? "-"}</div>
      ),
    },
  ];

  return (
    <>
      <div>
        {/* <RemoteTable
                    key="id"
                    data={ data }
                    columns={ columnsSimarin }
                    page={ page }
                    sizePerPage={ sizePerPage }
                    totalSize={ totalRows }
                    onTableChange={ handleTableChangeSimarin }
                    noDataIndication={
                        loading ? (
                            <Loader
                                type="ThreeDots"
                                color="#242323"
                                height={50}
                                className="text-center"
                            />
                        ) : (
                            <div className="text-center">
                                <Alert color="warning" className="text-center">
                                    <strong>Tidak ada data...</strong><br/>
                                </Alert>
                            </div>
                        )
                    }
                /> */}

        {/* <RemotePagination
                    data={ data }
                    page={ page }
                    sizePerPage={ sizePerPage }
                    totalSize={ totalRows }
                    onTableChange={ handleTableChangeSimarin }
                    columns={ columnsSimarin }
                /> */}
      </div>
      <div className="table-responsive">
        <Card title="Movies List" isOption>
          <BootstrapTable
            keyField="id"
            data={data}
            columns={columnsSimarin}
            hover
            bordered
            headerClasses="thead-dark"
            className="table table-responsive"
            noDataIndication={"Tidak ada data..."}
          />
          {data?.length > 0 && (
            <div className="d-lg-flex">
              <div className="">
                <span>
                  Halaman {paginationParameter.current_page} dari{" "}
                  {paginationParameter.last_page} <br />
                  Row {paginationParameter.from} s.d {paginationParameter.to}{" "}
                  dari {paginationParameter.total}
                </span>
              </div>
              <div className="ml-auto text-center">
                <Pagination
                  className="pagination justify-content-center"
                  listClassName="justify-content-center"
                >
                  {paginationParameter.current_page > 1 && (
                    <>
                      <PageItem onClick={onChangePage(1)} tabIndex="-1">
                        <i className="fa fa-angle-double-left" />
                        <span className="sr-only">First</span>
                      </PageItem>
                      <PageItem
                        onClick={onChangePage(
                          paginationParameter.current_page - 1
                        )}
                        tabIndex="-1"
                      >
                        <i className="fa fa-angle-left" />
                        <span className="sr-only">Previous</span>
                      </PageItem>
                      {prevPages(paginationParameter?.current_page)}
                    </>
                  )}

                  <PageItem
                    className="active"
                    onClick={(e) => e.preventDefault()}
                  >
                    {paginationParameter.current_page}
                  </PageItem>

                  {paginationParameter?.current_page <
                    paginationParameter.last_page && (
                    <>
                      {nextPages(paginationParameter?.current_page)}
                      <PageItem
                        onClick={onChangePage(
                          paginationParameter.current_page + 1
                        )}
                      >
                        <i className="fa fa-angle-right" />
                        <span className="sr-only">Next</span>
                      </PageItem>
                      <PageItem
                        onClick={onChangePage(paginationParameter?.last_page)}
                      >
                        <i className="fa fa-angle-double-right" />
                        <span className="sr-only">Last</span>
                      </PageItem>
                    </>
                  )}
                </Pagination>
              </div>
            </div>
          )}
        </Card>
      </div>
    </>
  );
};

export default App;
