import React from 'react'
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory, { PaginationProvider, PaginationListStandalone } from 'react-bootstrap-table2-paginator';

const RemotePagination = ({ data, columns, page, sizePerPage, onTableChange, totalSize }) => {
    return (
        <div>
            <PaginationProvider
                pagination={
                    paginationFactory({
                        custom: true,
                        page,
                        sizePerPage,
                        totalSize
                    })
                }
            >
                {
                ({
                    paginationProps,
                    paginationTableProps
                }) => (
                    <div>
                    <div>
                        <p>Current Page: { paginationProps.page }</p>
                        <p>Current SizePerPage: { paginationProps.sizePerPage }</p>
                    </div>
                    <div>
                        <PaginationListStandalone
                        { ...paginationProps }
                        />
                    </div>
                    <BootstrapTable
                        remote
                        keyField="id"
                        data={ data }
                        columns={ columns }
                        onTableChange={ onTableChange }
                        { ...paginationTableProps }
                    />
                    </div>
                )
                }
            </PaginationProvider>
        </div>
    )
};

export default RemotePagination