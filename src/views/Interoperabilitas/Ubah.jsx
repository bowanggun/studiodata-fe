import React from "react";
import { Modal, Form, Button } from "react-bootstrap";
import { Edit3, XCircle } from "react-feather";
import baseApi from "../../services/baseApi";
import { toast } from "react-toastify";

const Ubah = (props) => {
  const {
    openUpdateModal,
    setOpenUpdateModal,
    handleUpdateModal,
    isShared,
    handleChangeInputDataIsShared,
    webbappedaVisible,
    handleChangeInputDataWebBappeda,
    satudataVisible,
    handleChangeInputDataSatuData,
    error,
    setError,
    setLoading,
    fetchData,
    row,
  } = props;

  const handleUpdateData = (e) => {
    e.preventDefault();
    setLoading(true);

    const formData = new FormData();

    formData.append("is_shared", isShared);
    formData.append("webbappeda_visible", webbappedaVisible);
    formData.append("satudata_visible", satudataVisible);
    formData.append("_method", "PUT");

    baseApi
      .post(`/ubah-interoperabilitas/${row}`, formData)
      .then((response) => {
        if (response?.status === 200) {
          fetchData();
          toast.success(response?.data?.message, {
            position: toast.POSITION.TOP_RIGHT,
          });
          setOpenUpdateModal(false);
          // setInputData({});
        } else {
          // console.log("response", response);
        }
      })
      .catch((error) => {
        setError(error.response?.data?.errors);

        toast.error(error.response?.data?.message, {
          position: toast.POSITION.TOP_RIGHT,
        });
      });
  };

  return (
    <>
      <Modal
        show={openUpdateModal}
        onHide={handleUpdateModal}
        backdrop="static"
        keyboard={false}
      >
        <Form onSubmit={handleUpdateData}>
          <Modal.Header>
            <Modal.Title>Pengaturan Interoperabilitas</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form.Group className="mb-3">
              <Form.Label htmlFor="is_shared">Kelola Bersama</Form.Label>
              <div key={`inline-radio`} className="mb-3">
                <Form.Check
                  inline
                  label="Ya"
                  name="is_shared"
                  type="radio"
                  id={`is_shared_true`}
                  value={isShared}
                  onChange={handleChangeInputDataIsShared}
                  defaultChecked={isShared === 1}
                />
                <Form.Check
                  inline
                  label="Tidak"
                  name="is_shared"
                  type="radio"
                  id={`is_shared_false`}
                  value={isShared}
                  onChange={handleChangeInputDataIsShared}
                  defaultChecked={isShared === 0}
                />
              </div>
              <span className="text-danger">
                <i>{error?.is_shared ?? ""}</i>
              </span>
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label htmlFor="webbappeda_visible">
                Interoperabilitas Website Bappeda?
              </Form.Label>
              <div key={`inline-radio`} className="mb-3">
                <Form.Check
                  inline
                  label="Ya"
                  name="webbappeda_visible"
                  type="radio"
                  id={`webbappeda_visible_true`}
                  value={webbappedaVisible}
                  onChange={handleChangeInputDataWebBappeda}
                  defaultChecked={webbappedaVisible === 1}
                />
                <Form.Check
                  inline
                  label="Tidak"
                  name="webbappeda_visible"
                  type="radio"
                  id={`webbappeda_visible_false`}
                  value={webbappedaVisible}
                  onChange={handleChangeInputDataWebBappeda}
                  defaultChecked={webbappedaVisible === 0}
                />
              </div>
              <span className="text-danger">
                <i>{error?.webbappeda_visible ?? ""}</i>
              </span>
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label htmlFor="satudata_visible">
                Interoperabilitas Open Data Bogor?
              </Form.Label>
              <div key={`inline-radio`} className="mb-3">
                <Form.Check
                  inline
                  label="Ya"
                  name="satudata_visible"
                  type="radio"
                  id={`satudata_visible_true`}
                  value={satudataVisible}
                  onChange={handleChangeInputDataSatuData}
                  defaultChecked={satudataVisible === 1}
                />
                <Form.Check
                  inline
                  label="Tidak"
                  name="satudata_visible"
                  type="radio"
                  id={`satudata_visible_false`}
                  value={satudataVisible}
                  onChange={handleChangeInputDataSatuData}
                  defaultChecked={satudataVisible === 0}
                />
              </div>
              <span className="text-danger">
                <i>{error?.satudata_visible ?? ""}</i>
              </span>
            </Form.Group>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="primary" type="submit">
              <Edit3 size={14} className="mr-1" />
              Ubah
            </Button>
            <Button variant="secondary" onClick={handleUpdateModal}>
              <XCircle size={14} className="mr-1" />
              Tutup
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
    </>
  );
};

export default Ubah;
