/*eslint-disable */
import React, { useEffect, useState } from "react";
import Aux from "../../hoc/_Aux";
import {
  Button,
  Col,
  FormControl,
  InputGroup,
  Row,
  OverlayTrigger,
  Tooltip,
  Form
} from "react-bootstrap";
import Card from "../../App/components/MainCard";

import moment from "moment";
import { RemoteTable } from "../../App/components/RemoteTable";
import { toast } from "react-toastify";
import CkeditorModal from "../../App/components/CkeditorModal";
import ReactHtmlParser from "react-html-parser";
// import DeleteModal from "../../App/components/DeleteModal";
import { Search, Link as LinkIcon, X, Check } from "react-feather";
// import { sourceIcon } from "../../App/components/Icon";
// import { Link } from "react-router-dom";
import baseApi from "../../services/baseApi";
import { localDisk } from "../../services/Utilities";
import Ubah from "./Ubah";

import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
import "react-bootstrap-table2-toolkit/dist/react-bootstrap-table2-toolkit.min.css";
import "react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css";
import BootstrapTable from "react-bootstrap-table-next";
import Pagination from "react-bootstrap/Pagination";
import PageItem from "react-bootstrap/PageItem";

const Home = () => {
  const id = require("moment/locale/id");
  moment.updateLocale("id", id);

  const [resultData, setResultData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [page, setPage] = useState(1);
  const [currentPage, setCurrentPage] = useState(1);
  const [totalRows, setTotalRows] = useState(0);
  const [sizePerPage, setSizePerPage] = useState(10);
  const [fromPage, setFromPage] = useState(1);
  const [keyword, setKeyword] = useState("");
  const [openUpdateModal, setOpenUpdateModal] = useState(false);
  const [openCkeditorModalDesk, setOpenCkeditorModalDesk] = useState(false);
  const [paginationParameter, setPaginationParameter] = useState("");

  const [isShared, setIsShared] = useState(0);
  const [webbappedaVisible, setWebbappedaVisible] = useState(0);
  const [satudataVisible, setSatudataVisible] = useState(0);
  const [deskripsi, setDeskripsi] = useState("");

  const [error, setError] = useState([]);
  const [row, setRow] = useState("");

  const headerSortingClasses = (column, sortOrder, isLastSorting, colIndex) =>
    sortOrder === "asc" ? "bg-success" : "bg-info";

  const fetchData = async (e) => {
    try {
      e?.preventDefault();
      let apiData = "";
      if (keyword === "" || keyword === undefined || keyword === null) {
        apiData = "/trx-data";
      } else {
        apiData = `/trx-data?q=${keyword}`;
      }

      const response = await baseApi.get(apiData);
      if (response?.status === 200) {
        setResultData(response?.data?.result?.data);
        setTotalRows(response?.data?.result?.total);
        // setFromPage(response?.data?.result?.from);
        setPaginationParameter(response?.data?.result);
      }
    } catch (error) {
      toast.warning("Terjadi kesalahan", {
        position: toast.POSITION.TOP_RIGHT,
      });
      console.log(error);
    }
  };

  const handleChangeInputDataIsShared = (e) => {
    e.persist();
    if (e.target.id === "is_shared_true") {
      setIsShared(1);
    } else {
      setIsShared(0);
    }
  };

  const handleChangeInputDataWebBappeda = (e) => {
    e.persist();
    if (e.target.id === "webbappeda_visible_true") {
      setWebbappedaVisible(1);
    } else {
      setWebbappedaVisible(0);
    }
  };

  const handleChangeInputDataSatuData = (e) => {
    e.persist();
    if (e.target.id === "satudata_visible_true") {
      setSatudataVisible(1);
    } else {
      setSatudataVisible(0);
    }
  };

  const handleKeyword = (e) => {
    setKeyword(e.target.value);
  };

  const handleUpdateModal = () => {
    setOpenUpdateModal(!openUpdateModal);
    setError([]);
  };

  const handleOnTableChange = (page) => async (e) => {
    let result = resultData;
    setLoading(true);
    setPage(page);

    const response = await baseApi.get(`/trx-data?page=${page}`);
    if (response?.status === 200) {
      setResultData(response?.data?.result?.data);
      setCurrentPage(response?.data?.result?.current_page);
      setSizePerPage(response?.data?.result.per_page);
      setFromPage(response?.data?.result?.total);
      setLoading(false);
      setPaginationParameter(response?.data?.result);
    }
  };

  function prevPages(hal) {
    const listPages = [];
    listPages.push(
      <PageItem onClick={handleOnTableChange(hal - 1)}>{hal - 1}</PageItem>
    );
    return listPages;
  }

  function nextPages(hal) {
    const listPages = [];
    listPages.push(
      <PageItem onClick={handleOnTableChange(hal + 1)}>{hal + 1}</PageItem>
    );
    return listPages;
  }

  const columns = [
    // {
    //   dataField: "",
    //   text: "No.",
    //   formatter: (cell, row, rowIndex, extraData) => (
    //     <>{page * sizePerPage - sizePerPage + (rowIndex + 1)}</>
    //   ),
    //   headerAlign: "center",
    //   align: "center",
    // },
    {
      dataField: "",
      text: "",
      headerAlign: "center",
      align: "center",
      formatter: (cell, row, rowIndex, extraData) => (
        <div>
          {/* <ButtonGroup size="sm" aria-label="Basic example"> */}
          <OverlayTrigger overlay={<Tooltip>Ubah Data</Tooltip>}>
            <Button
              className="py-1 px-2"
              variant="outline-warning"
              size="sm"
              onClick={() =>
                // setOpenUpdateModal(!openUpdateModal, setRow(row?.id))
                setOpenUpdateModal(
                  !openUpdateModal,
                  setIsShared(row.is_shared === true ? 1 : 0),
                  setWebbappedaVisible(row.webbappeda_visible === true ? 1 : 0),
                  setSatudataVisible(row.satudata_visible === true ? 1 : 0),
                  setRow(row?.id)
                )
              }
            >
              <LinkIcon size={14} />
            </Button>
          </OverlayTrigger>

          {/* </ButtonGroup> */}
        </div>
      ),
    },
    {
      dataField: "id",
      text: "ID Data",
      headerAlign: "center",
      align: "center",
      sort: true,
      headerSortingClasses,
    },
    {
      dataField: "mst_jenis_dok.jenis_dok",
      text: "Jenis Dokumen",
      headerAlign: "center",
      sort: true,
      headerSortingClasses,
    },
    {
      dataField: "nama_data",
      text: "Nama Data",
      headerAlign: "center",
      sort: true,
      headerSortingClasses,
      formatter: (cell, row, rowIndex, extraData) => (
        <>
          {row.jenis_data === "UPLOAD" && (
            <a href={`${localDisk}/${row.url_file_upload}`} target="_blank">
              {row.nama_data}
              {/* <File size={14} /> */}
            </a>
          )}
          {row.jenis_data === "URL" && (
            <a href={row.url_file_upload} target="_blank">
              {row.nama_data}
              {/* <File size={14} /> */}
            </a>
          )}
        </>
      ),
    },
    {
      dataField: "sumber_data",
      text: "Sumber Data",
      headerAlign: "center",
      sort: true,
      headerSortingClasses,
      formatter: (cell, row, rowIndex, extraData) => (
        <>
          <a
            href="#"
            onClick={() =>
              setOpenCkeditorModalDesk(
                !openCkeditorModalDesk,
                setDeskripsi(row.deskripsi_data)
              )
            }
          >
            {row?.sumber_data}
          </a>
        </>
      ),
    },
    {
      dataField: "created_at",
      text: "Tanggal Dibuat",
      formatter: (cell, row, rowIndex, extraData) => (
        <>{moment(row.created_at).format("D MMMM YYYY H:mm:ss")}</>
      ),
      headerAlign: "center",
      align: "center",
      sort: true,
      headerSortingClasses,
    },
    {
      dataField: "created_by",
      text: "Dibuat Oleh",
      headerAlign: "center",
      align: "center",
      sort: true,
      headerSortingClasses,
    },
    {
      dataField: "is_shared",
      text: "Kelola Bersama",
      headerAlign: "center",
      align: "center",
      sort: true,
      headerSortingClasses,
      formatter: (cell, row, rowIndex, extraData) => (
        <>{row?.is_shared === true ? <Check /> : <X />}</>
      ),
    },
    {
      dataField: "webbappeda_visible",
      text: "Website Bappeda",
      headerAlign: "center",
      align: "center",
      sort: true,
      headerSortingClasses,
      formatter: (cell, row, rowIndex, extraData) => (
        <>{row?.webbappeda_visible === true ? <Check /> : <X />}</>
      ),
    },
    {
      dataField: "satudata_visible",
      text: "Open Data Bogor",
      headerAlign: "center",
      align: "center",
      sort: true,
      headerSortingClasses,
      formatter: (cell, row, rowIndex, extraData) => (
        <>{row?.satudata_visible === true ? <Check /> : <X />}</>
      ),
    },
  ];

  /*eslint-disable */
  useEffect(() => {
    let cleanUp = true;
    if (cleanUp) {
      fetchData();
    }

    return () => {
      cleanUp = false;
    };
  }, []);

  // console.log("res", resultGrupDokumen);

  return (
    <>
      <Aux>
        <Row>
          <Col>
            <Card title="Interoperabilitas" isOption>
              <div>
              <Form onSubmit={fetchData}>
                <InputGroup className="mb-3">
                  <FormControl
                    autoFocus
                    placeholder="Cari disini..."
                    aria-label="Cari disini..."
                    aria-describedby="basic-addon2"
                    type="text"
                    value={keyword}
                    name="keyword"
                    onChange={handleKeyword}
                  />
                  <InputGroup.Append>
                    <Button
                      variant="outline-primary"
                      type="submit"
                    >
                      <Search size={14} className="mr-1" />
                      CARI
                    </Button>
                  </InputGroup.Append>
                </InputGroup>
              </Form>
              </div>

              <div className="table-responsive">
                <BootstrapTable
                  keyField="id"
                  data={resultData}
                  columns={columns}
                  hover
                  bordered
                  headerClasses="thead-dark"
                  className="table table-responsive"
                  noDataIndication={"Tidak ada data..."}
                />

                {resultData?.length > 0 && (
                  <div className="d-lg-flex">
                    <div className="ml-auto text-center">
                      <Pagination
                        className="pagination justify-content-center"
                        listClassName="justify-content-center"
                      >
                        {paginationParameter.current_page > 1 && (
                          <>
                            <PageItem
                              onClick={handleOnTableChange(
                                paginationParameter.current_page - 1
                              )}
                              tabIndex="-1"
                            >
                              <i className="fa fa-angle-left" />
                              <span className="sr-only">Previous</span>
                            </PageItem>
                            {prevPages(paginationParameter?.current_page)}
                          </>
                        )}

                        <PageItem
                          className="active"
                          onClick={(e) => e.preventDefault()}
                        >
                          {paginationParameter.current_page}
                        </PageItem>
                        {paginationParameter?.next_page_url && (
                          <>
                            {nextPages(paginationParameter?.current_page)}
                            <PageItem
                              onClick={handleOnTableChange(
                                paginationParameter.current_page + 1
                              )}
                            >
                              <i className="fa fa-angle-right" />
                              <span className="sr-only">Next</span>
                            </PageItem>
                          </>
                        )}
                      </Pagination>
                    </div>
                  </div>
                )}
              </div>
            </Card>
          </Col>
        </Row>
      </Aux>

      {openUpdateModal && (
        <Ubah
          openUpdateModal={openUpdateModal}
          handleUpdateModal={handleUpdateModal}
          isShared={isShared}
          webbappedaVisible={webbappedaVisible}
          satudataVisible={satudataVisible}
          handleChangeInputDataIsShared={handleChangeInputDataIsShared}
          handleChangeInputDataWebBappeda={handleChangeInputDataWebBappeda}
          handleChangeInputDataSatuData={handleChangeInputDataSatuData}
          error={error}
          setLoading={setLoading}
          fetchData={fetchData}
          setOpenUpdateModal={setOpenUpdateModal}
          setError={setError}
          row={row}
        />
      )}

      <CkeditorModal
        title="Deskripsi Data"
        value={ReactHtmlParser(deskripsi)}
        namaState={openCkeditorModalDesk}
        namaSetState={setOpenCkeditorModalDesk}
        buttonCancel="Tutup"
        size="sm" //lg,xl,sm,md
      />
    </>
  );
};

export default Home;
