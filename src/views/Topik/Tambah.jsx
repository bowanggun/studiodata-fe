import React, { useRef, useEffect } from "react";
import { Modal, Form, Button } from "react-bootstrap";
import { PlusCircle, XCircle } from "react-feather";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import baseApi from "../../services/baseApi";
import { toast } from "react-toastify";
import Loader from "react-loader-spinner";

const Tambah = (props) => {
  const {
    openTambahModal,
    setOpenTambahModal,
    handleTambahModal,
    inputData,
    setInputData,
    handleChangeInputData,
    error,
    setError,
    keterangan,
    setKeterangan,
    handleInputKeterangan,
    loading,
    setLoading,
    fetchData,
  } = props;

  const innerRef = useRef();

  const handleInputTopik = (e) => {
    e.preventDefault();
    setLoading(true);

    const formData = new FormData();

    formData.append("topik", inputData.topik ? inputData.topik : "");
    formData.append("keterangan", keterangan ? keterangan : "");

    baseApi
      .post(`/topik`, formData)
      .then((response) => {
        if (response?.status === 200) {
          fetchData();
          toast.success(response?.data?.message, {
            position: toast.POSITION.TOP_RIGHT,
          });
          setOpenTambahModal(false);
          setKeterangan("");
          setInputData({});
          setLoading(false);
        } else {
          setLoading(false);
          // console.log("response", response);
        }
      })
      .catch((error) => {
        setLoading(false);
        setError(error.response?.data?.errors);

        toast.error(error.response?.data?.message, {
          position: toast.POSITION.TOP_RIGHT,
        });
      });
  };

  useEffect(() => {
    setTimeout(() => {
      innerRef.current.focus();
      setKeterangan("");
      setInputData({});
    }, 1);
  }, [setInputData, setKeterangan]);

  return (
    <>
      <Modal
        show={openTambahModal}
        onHide={handleTambahModal}
        backdrop="static"
        keyboard={false}
      >
        <Form onSubmit={handleInputTopik}>
          <Modal.Header>
            <Modal.Title>Tambah Topik</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form.Group className="mb-3 form-label-group">
              <Form.Control
                ref={innerRef}
                placeholder="Topik"
                className="form-control"
                type="text"
                id="topik"
                name="topik"
                aria-describedby="passwordHelpBlock"
                value={inputData.topik}
                onChange={handleChangeInputData}
              />
              <Form.Label htmlFor="topik">Topik</Form.Label>
              <span className="text-danger">
                <i>{error?.topik ?? ""}</i>
              </span>
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label htmlFor="keterangan">Keterangan</Form.Label>
              <CKEditor
                editor={ClassicEditor}
                data={keterangan}
                onChange={handleInputKeterangan}
                onReady={(editor) => {
                  editor.editing.view.change((writer) => {
                    writer.setStyle(
                      "height",
                      "200px",
                      editor.editing.view.document.getRoot()
                    );
                  });
                }}
              />
              <span className="text-danger">
                <i>{error?.keterangan ?? ""}</i>
              </span>
            </Form.Group>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="primary" type="submit" disabled={loading}>
              {loading ? (
                <Loader type="ThreeDots" color="#fff" height={10} />
              ) : (
                <>
                  <PlusCircle size={14} className="mr-1" /> Simpan
                </>
              )}
            </Button>
            <Button variant="secondary" onClick={handleTambahModal}>
              <XCircle size={14} className="mr-1" />
              Tutup
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
    </>
  );
};

export default Tambah;
