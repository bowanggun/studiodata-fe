import React from 'react';
import DEMO  from './../../../../../store/constant';
import Aux from "../../../../../hoc/_Aux";
import { Images } from '../../../../components/Icon';

const navLogo = (props) => {
    let toggleClass = ['mobile-menu'];
    if (props.collapseMenu) {
        toggleClass = [...toggleClass, 'on'];
    }

    return (
        <Aux>
            <div className="navbar-brand header-logo">
                 <a href={DEMO.BLANK_LINK} className="b-brand">
                    <div className='b-bg'>
                        <img
                            alt="..."
                            src={Images?.logo}
                            width={30}
                            className='img-circle'
                        />
                    </div>
                    <span className="b-title">Studio Data</span>
                 </a>
                <a className={toggleClass.join(' ')} id="mobile-collapse" onClick={props.onToggleNavigation} style={{cursor: 'pointer'}}><span /></a>
            </div>
        </Aux>
    );
};

export default navLogo;
