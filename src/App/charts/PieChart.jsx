import React from "react";
import "chart.js/auto";
import { Pie } from "react-chartjs-2";
import ChartDataLabels from "chartjs-plugin-datalabels";
import { generateRandomHexColorCombination } from "../components/HexColorRandom";

const PieChart = (props) => {
  const { title, dataSet, labels } = props;

  const backgroundColor = generateRandomHexColorCombination(labels.length, 1);
  const borderColor = backgroundColor.map(
    (color) => color.replace(/[^0-9,]/g, "") + ",1)"
  );

  const data = {
    labels: labels,
    datasets: [
      {
        label: title,
        data: dataSet,
        backgroundColor: backgroundColor,
        borderColor: borderColor,
        borderWidth: 2,
      },
    ],
  };

  const options = {
    plugins: {
      title: {
        display: true,
        text: title,
        font: {
          weight: "bold",
          size: 18,
        },
        padding: {
          bottom: 30,
        },
      },
      legend: {
        display: false, // Menghilangkan legend
      },
      tooltip: {
        enabled: false,
      },
      datalabels: {
        anchor: "end",
        align: "end",
        offset: 2,
        font: {
          weight: "bold",
        },
        formatter: (value, context) => {
          const datapoints = context.dataset.data;
          const totalValue = datapoints.reduce(
            (total, datapoint) => total + Number(datapoint),
            0
          );

          const percentageValue = ((Number(value) / totalValue) * 100).toFixed(
            2
          );

          return `${
            context.chart.data.labels[context.dataIndex]
          }: ${value} (${percentageValue}%)`;
        },
        padding: {
          top: 5,
          bottom: 10,
        },
        color: (context) => {
          const dataset = context.dataset;
          const backgroundColor = dataset.backgroundColor;
          return backgroundColor;
        },
      },
    },
    responsive: true,
    maintainAspectRatio: false,
    layout: {
      padding: {
        top: 20,
        bottom: 20,
      },
    },
  };

  return (
    <div>
      <Pie
        data={data}
        options={options}
        height={null}
        plugins={[ChartDataLabels]}
      />
    </div>
  );
};

export default PieChart;
