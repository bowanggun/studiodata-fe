import React from "react";
import { Row, Col, Form, Button } from "react-bootstrap";
import Modal from "react-bootstrap/Modal";

const CkeditorModal = (props) => {
  const { title, value, namaState, namaSetState, buttonCancel, size } = props;

  return (
    <Modal
      show={namaState}
      onHide={() => namaSetState(!namaState)}
      backdrop="static"
      keyboard={false}
      size={size}
    >
      <div className="modal-body">
        <Row>
          <Col md="12" className="pt-1 pb-1">
            <div className="text-center">
              <h3 className="font-weight-bolder">{title}</h3>
            </div>

            <div className="justify-contenct-center">
              <p>{value}</p>
            </div>
          </Col>
          <Col md="12" className="pb-2 mt-5">
            <div className="d-flex justify-content-center">
              <Button
                variant="outline-secondary"
                className="text-dark"
                color="secondary"
                outline
                onClick={() => namaSetState(!namaState)}
              >
                {buttonCancel}
              </Button>
            </div>
          </Col>
        </Row>
      </div>
    </Modal>
  );
};

export default CkeditorModal;
