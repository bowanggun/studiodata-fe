import React from "react";
import { Row, Col, Button, Card, Table } from "react-bootstrap";
import Modal from "react-bootstrap/Modal";
import ReactHtmlParser from "react-html-parser";
import moment from "moment";
import { CheckCircle, X } from "react-feather";

const InfoMetaDataFileModal = (props) => {
  const id = require("moment/locale/id");
  moment.updateLocale("id", id);

  const { title, namaState, namaSetState, buttonCancel, size, value } = props;

  return (
    <Modal
      show={namaState}
      onHide={() => namaSetState(!namaState)}
      backdrop="static"
      keyboard={false}
      size={size}
    >
      <div className="modal-body">
        <Row>
          <Col>
            <Card>
              <Card.Header>
                <Card.Title as="h5">{title}</Card.Title>
                {/* <span className="d-block m-t-5">
                use props <code>striped</code> with <code>Table</code> component
              </span> */}
              </Card.Header>
              <Card.Body>
                <Table striped responsive>
                  <tbody>
                    <tr>
                      <td>Sumber Data</td>
                      <td>{value.sumber_data}</td>
                    </tr>
                    <tr>
                      <td>Deskripsi Data</td>
                      <td>{ReactHtmlParser(value.deskripsi_data)}</td>
                    </tr>
                    <tr>
                      <td>Catatan</td>
                      <td>{ReactHtmlParser(value.catatan)}</td>
                    </tr>
                    <tr>
                      <td>Jenis Data</td>
                      <td>{value.jenis_data}</td>
                    </tr>
                    <tr>
                      <td>Format Data</td>
                      <td>{value.format_data}</td>
                    </tr>
                    <tr>
                      <td>Media type</td>
                      <td>{value.media_type}</td>
                    </tr>
                    <tr>
                      <td>Size Data</td>
                      <td>{value.size_data}</td>
                    </tr>
                    <tr>
                      <td>Ekstensi Data</td>
                      <td>{value.ekstensi_data}</td>
                    </tr>
                    <tr>
                      <td>Interoperabilitas Website Bappeda</td>
                      <td>
                        {value.webbapeeda_visible ? <CheckCircle /> : <X />}
                      </td>
                    </tr>
                    <tr>
                      <td>Interoperabilitas Open Data Bogor</td>
                      <td>
                        {value.satudata_visible ? <CheckCircle /> : <X />}
                      </td>
                    </tr>

                    {/* <tr>
                      <td>Topik</td>
                      <td>
                        <ul>
                          {value?.trx_data_topik?.map((item, index) => (
                            <li key={index}>{item.topik}</li>
                          ))}
                        </ul>
                      </td>
                    </tr>
                    <tr>
                      <td>Tag</td>
                      <td>
                        <ul>
                          {value?.trx_data_tag?.map((item, index) => (
                            <li key={index}>{item.data_tag}</li>
                          ))}
                        </ul>
                      </td>
                    </tr>
                    <tr>
                      <td>Organisasi</td>
                      <td>
                        <ul>
                          {value?.trx_data_organisasi?.map((item, index) => (
                            <li key={index}>{item.organisasi}</li>
                          ))}
                        </ul>
                      </td>
                    </tr> */}

                    <tr>
                      <td>Tanggal Memasukan Data</td>
                      <td>
                        {moment(value.created_at).format("D MMMM YYYY H:mm:ss")}
                      </td>
                    </tr>
                  </tbody>
                </Table>
              </Card.Body>
            </Card>

            <div className="d-flex justify-content-center">
              <Button
                variant="outline-secondary"
                className="text-dark"
                color="secondary"
                outline
                onClick={() => namaSetState(!namaState)}
              >
                {buttonCancel}
              </Button>
            </div>
          </Col>
        </Row>
      </div>
    </Modal>
  );
};

export default InfoMetaDataFileModal;
