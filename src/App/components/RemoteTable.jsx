import React from "react";
import Proptypes from "prop-types";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";

import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
import "react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css";

export const RemoteTable = ({
  data,
  columns,
  page,
  sizePerPage,
  onTableChange,
  rowClasses,
  totalSize,
  noDataIndication,
}) => {
  return (
    <>
      <BootstrapTable
        className="table table-responsive"
        responsive
        striped
        hover
        condensed
        remote
        keyField="id"
        data={data}
        columns={columns}
        pagination={paginationFactory({
          page,
          sizePerPage,
          totalSize,
          hideSizePerPage: true,
        })}
        onTableChange={onTableChange}
        headerClasses="thead-light"
        // classes={`table-responsive table-flush table-hover align-items-center mb-4`}
        rowClasses={rowClasses}
        noDataIndication={noDataIndication}
      />
    </>
  );
};

RemoteTable.propTypes = {
  data: Proptypes.array.isRequired,
  page: Proptypes.number.isRequired,
  totalSize: Proptypes.number.isRequired,
  sizePerPage: Proptypes.number.isRequired,
  onTableChange: Proptypes.func.isRequired,
};
