import React from "react";
import { sourceIcon } from "./Icon";

export const format = "formatEkstensi";
export const formatData = "formatData";

/*eslint-disable */
export const getFormatEkstensi = (ekstensi) => {
  return Object.keys(sourceIcon).map((item) => {
    if (item === `${format}${ekstensi}`) {
      return (
        <img
          src={sourceIcon[item]}
          className="img-responsive mb-1 mx-2"
          width="30"
          alt={ekstensi}
        />
      );
    }
  });
};

export const getFormatData = (ekstensi) => {
  // return( <img src={`${sourceIcon}${ekstensi}`} /> )
  const getEkstensi = `${formatData}${ekstensi}`;
  return Object.keys(sourceIcon).map((item) => {
    if (item === getEkstensi) {
      return (
        <img
          src={sourceIcon[item]}
          className="img-responsive mb-1"
          width="30"
          alt={ekstensi}
        />
      );
    }
  });
};
