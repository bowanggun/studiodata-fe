export function generateRandomHexColorCombination(count, opacity) {
  const chartColorPalette = [
    "#ff6384", // Merah
    "#36a2eb", // Biru
    "#ffce56", // Kuning
    "#4bc0c0", // Hijau
    "#9966ff", // Ungu
    "#ff9f40", // Oranye
    "#ff4da6", // Pink
    "#00cc66", // Hijau Terang
  ];

  const hexColors = new Set();
  while (hexColors.size < count) {
    const randomIndex = Math.floor(Math.random() * chartColorPalette.length);
    const randomColor = chartColorPalette[randomIndex];
    const hexColor = randomColor;
    const hexColorWithOpacity = addOpacityToHex(hexColor, opacity);
    hexColors.add(hexColorWithOpacity);
  }

  return Array.from(hexColors);
}

function addOpacityToHex(hexColor, opacity) {
  const opacityValue = Math.round(opacity * 255)
    .toString(16)
    .padStart(2, "0");
  return hexColor + opacityValue;
}
