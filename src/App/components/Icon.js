export const Images = {
  logo: require("../../assets/logo/bappedalogo.png"),
};

export const sourceIcon = {
  alert: require("../../assets/logo/alert-icon.png"),
  quest: require("../../assets/logo/questions.png"),
  info: require("../../assets/logo/info-icon.png"),

  // format data
  formatDataAUDIO: require("../../assets/logo/format-data/format-data-audio.png"),
  formatDataRAR: require("../../assets/logo/format-data/format-data-compressed.png"),
  formatDataDOCUMENT: require("../../assets/logo/format-data/format-data-document.png"),
  formatDataEXCEL: require("../../assets/logo/format-data/format-data-excel.png"),
  formatDataIMAGE: require("../../assets/logo/format-data/format-data-image.png"),
  formatDataPDF: require("../../assets/logo/format-data/format-data-pdf.png"),
  formatDataPOWERPOINT: require("../../assets/logo/format-data/format-data-ppt.png"),
  formatDataVIDEO: require("../../assets/logo/format-data/format-data-video.png"),
  formatDataUnknown: require("../../assets/logo/format-data/missing-format.png"),
  formatDataURL: require("../../assets/logo/format-data/url3.png"),
  // end format data

  //format ekstensi
  formatEkstensi7z: require("../../assets/logo/ekstensi/7z.png"),
  formatEkstensicsv: require("../../assets/logo/ekstensi/csv.png"),
  formatEkstensidoc: require("../../assets/logo/ekstensi/doc.png"),
  formatEkstensidocx: require("../../assets/logo/ekstensi/docx.png"),
  formatEkstensigz: require("../../assets/logo/ekstensi/gz.png"),
  formatEkstensijpeg: require("../../assets/logo/ekstensi/jpeg.png"),
  formatEkstensijpg: require("../../assets/logo/ekstensi/jpg.png"),
  formatEkstensiJPG: require("../../assets/logo/ekstensi/jpg.png"),
  formatEkstensiUnknown: require("../../assets/logo/ekstensi/missing-ekstensi.png"),
  formatEkstensimp3: require("../../assets/logo/ekstensi/mp3.png"),
  formatEkstensimp4: require("../../assets/logo/ekstensi/mp4.png"),
  formatEkstensimpeg: require("../../assets/logo/ekstensi/mpeg.png"),
  formatEkstensipdf: require("../../assets/logo/ekstensi/pdf.png"),
  formatEkstensipng: require("../../assets/logo/ekstensi/png.png"),
  formatEkstensippt: require("../../assets/logo/ekstensi/ppt.png"),
  formatEkstensipptx: require("../../assets/logo/ekstensi/pptx.png"),
  formatEkstensirar: require("../../assets/logo/ekstensi/rar.png"),
  formatEkstensirtf: require("../../assets/logo/ekstensi/rtf.png"),
  formatEkstensixls: require("../../assets/logo/ekstensi/xls.png"),
  formatEkstensixlsx: require("../../assets/logo/ekstensi/xlsx.png"),
  formatEkstensizip: require("../../assets/logo/ekstensi/zip.png"),
  formatEkstensiURL: require("../../assets/logo/format-data/url4.png"),
  //end format ekstensi
};

export const iconDashboard = {
  zip: require("../../assets/icon/zip.png"),
  rar: require("../../assets/icon/rar.png"),
  pdf: require("../../assets/icon/pdf.png"),
  xls: require("../../assets/icon/xls.png"),
  jpg: require("../../assets/icon/jpg-file.png"),
  png: require("../../assets/icon/png-file-.png"),
}
