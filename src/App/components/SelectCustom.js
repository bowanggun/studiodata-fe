import React from "react";
import AsyncSelect from "react-select/async";

const SelectCustom = (props) => {
  const {
    data,
    setError,
    setValueData,
    fetchData,
    labelName,
    valueDataOrganisasi,
  } = props;

  const optionsData = data.map((item) => ({
    value: labelName === "dataWilayah"
    ? item.kemendagri_kota_kode
    : item.id,
    label:
      labelName === "dataTag"
        ? item.data_tag
        : labelName === "dataTopik"
        ? item.topik
        : labelName === "dataWilayah"
        ? item.kemendagri_kota_nama
        : item.organisasi,
  }));

  const filterData = (inputValueFilter) => {
    if (inputValueFilter?.length >= 2) {
      setError("");
      return optionsData?.filter((i) =>
        i.label?.toLowerCase().includes(inputValueFilter?.toLowerCase())
      );
    }
  };

  const promiseOptions = (inputValuePromise) =>
    new Promise((resolve) => {
      setTimeout(() => {
        if (inputValuePromise?.length < 2) {
          resolve(filterData(null));
        } else if (
          inputValuePromise?.length === 2 ||
          inputValuePromise?.length > 2
        ) {
          resolve(filterData(inputValuePromise));
        }
      }, 1000);
    });

  const handleInputChangeData = (inputValueInputChange) => {
    // console.log("inputValueInputChange", inputValueInputChange);
    if (inputValueInputChange?.length < 2) {
      return setError("Ketikan minimal 3 sampai 4 karakter");
    } else {
      return fetchData(inputValueInputChange);
    }
  };

  const handleValueData = (e) => {
    // const data = e?.map((item) => {
    //   return item.label;
    // });
    // console.log("test e", e);
    if (e.length > 0) {
      setValueData(e);
    } else {
      setValueData();
    }

    // console.log("valLength", e);
  };

  return (
    <>
      <AsyncSelect
        isMulti
        cacheOptions
        defaultOptions
        loadOptions={promiseOptions}
        onChange={handleValueData}
        onInputChange={handleInputChangeData}
        // onKeyDown={fetchData}
        placeholder="Masukan 3 karakter atau lebih"
        noOptionsMessage={() => "Data tidak ditemukan.."}
        value={valueDataOrganisasi}
      />
    </>
  );
};

export default SelectCustom;
