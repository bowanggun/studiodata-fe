import React from "react";
import Loader from "react-loader-spinner";
import { Row, Col, Form, Button } from "react-bootstrap";
import Modal from "react-bootstrap/Modal";

const Deletes = (props) => {
  const {
    title,
    subtitle,
    sourceIcon,
    buttonConfirm,
    buttonCancel,
    namaState,
    namaSetState,
    handleForm,
    loading,
  } = props;

  return (
    <Modal
      show={namaState}
      onHide={() => namaSetState(!namaState)}
      backdrop="static"
      keyboard={false}
    >
      <Form onSubmit={handleForm}>
        <div className="modal-body">
          <Row>
            <Col md="12" className="pt-1 pb-1">
              <div className="text-center">
                <img
                  className="img-responsive mb-1"
                  width="80"
                  src={sourceIcon}
                  alt="alert"
                />
                <h1 className="font-weight-bolder">{title}</h1>
                <h5 className="text-danger">{subtitle}</h5>
              </div>
            </Col>
            <Col md="12" className="pb-2 mt-5">
              <div className="d-flex justify-content-center">
                <Button
                  variant="outline-danger"
                  type="submit"
                  // color="danger"
                  className="mr-1"
                  disabled={loading}
                >
                  {loading ? (
                    <Loader type="ThreeDots" color="#fff" height={10} />
                  ) : (
                    buttonConfirm
                  )}
                </Button>
                {!loading && (
                  <Button
                    variant="outline-secondary"
                    className="text-dark"
                    // color="secondary"
                    outline
                    onClick={() => namaSetState(!namaState)}
                  >
                    {buttonCancel}
                  </Button>
                )}
              </div>
            </Col>
          </Row>
        </div>
      </Form>
    </Modal>
  );
};

export default Deletes;
