import React from "react";
import $ from "jquery";

window.jQuery = $;
window.$ = $;
global.jQuery = $;

const DashboardDefault = React.lazy(() => import("./Demo/Dashboard/Default"));
const OtherDocs = React.lazy(() => import("./Demo/Other/Docs"));
const AppTest = React.lazy(() => import("./views/dokumentasi/App"));
const ExcelApp = React.lazy(() => import("./views/dumy/ExcelPage"));
const GrupDokumen = React.lazy(() => import("./views/GrupDokumen/Home"));
const JenisDokumen = React.lazy(() => import("./views/DetailDokumen/Home"));
const Organisasi = React.lazy(() => import("./views/Organisasi/Home"));
const DataTag = React.lazy(() => import("./views/DataTag/Home"));
const Topik = React.lazy(() => import("./views/Topik/Home"));
const InputData = React.lazy(() => import("./views/InputData/Home"));
const Interoperabilitas = React.lazy(() => import("./views/Interoperabilitas/Home"));
const ConfVisualisasi = React.lazy(() => import("./views/ConfVisualisasi/Home"));
const KelolaData = React.lazy(() => import("./views/InputData/KelolaData"));
const RemoteSelect = React.lazy(() => import("./views/dokumentasi/SelectRemote"));
const PencarianData = React.lazy(() => import("./views/PencarianData/Home"));
const Dashboard = React.lazy(() => import("./views/Dashboard/Home"));
const KoleksiData = React.lazy(() => import("./views/KoleksiData/Home"));
const Tree = React.lazy(() => import("./views/Tree/Home"));
const LuasWilayahKecamatan = React.lazy(() =>import("./views/KoleksiData/LuasWilayahKecamatan/Home"));
const DetailKoleksi = React.lazy(() => import("./views/KoleksiData/DetailKoleksi/Home"));
const LajuPertumbuhanPdrb = React.lazy(() => import("./views/KoleksiData/LajuPertumbuhanPdrb/Home"));
const PdrbAdhk2010LapUsaha = React.lazy(() => import("./views/KoleksiData/PdrbAdhk2010LapUsaha/Home"));
const JmlMuseumBdsPengelola = React.lazy(() => import("./views/KoleksiData/JmlMuseumBdsPengelola/Home"));
const JmlPendudukBdsJk = React.lazy(() => import("./views/KoleksiData/JmlPendudukBdsJk/Home"));
const PdrbAdhkJenisPengeluaran = React.lazy(() => import("./views/KoleksiData/PdrbAdhkJenisPengeluaran/Home"));
const JmlAngkatanKerjaTkPendidikan = React.lazy(() => import("./views/KoleksiData/JmlAngkatanKerjaTkPendidikan/Home"));
const DetailKoleksi1Dimensi = React.lazy(() => import("./views/KoleksiData/DetailKoleksi1Dimensi/Home"));

const routes = [
  {
    path: "/dashboard",
    exact: true,
    name: "Default",
    // component: DashboardDefault,
    component: Dashboard,
  },
  {
    path: "/detail-dokumen",
    exact: true,
    name: "Detail Dokumen",
    component: JenisDokumen,
  },
  {
    path: "/organisasi",
    exact: true,
    name: "Organisasi",
    component: Organisasi,
  },
  {
    path: "/data-tag",
    exact: true,
    name: "Tag Data",
    component: DataTag,
  },
  {
    path: "/topik",
    exact: true,
    name: "Topik",
    component: Topik,
  },
  {
    path: "/input-data",
    exact: true,
    name: "Input Data",
    component: InputData,
  },
  {
    path: "/tree",
    exact: true,
    name: "Tree",
    component: Tree,
  },
  {
    path: "/koleksi-data",
    exact: true,
    name: "Koleksi Data",
    component: KoleksiData,
  },
  {
    path: "/luas-wilayah-kecamatan",
    exact: true,
    name: "Luas Wilayah Kecamatan",
    component: LuasWilayahKecamatan,
  },
  {
    path: "/detail-koleksi",
    exact: true,
    name: "Detail Koleksi",
    component: DetailKoleksi,
  },
  {
    path: "/detail-koleksi-1",
    exact: true,
    name: "Detail Koleksi 1",
    component: DetailKoleksi1Dimensi,
  },
  // {
  //   path: "/laju-pertumbuhan-pdrb-lapangan-usaha",
  //   exact: true,
  //   name: "Laju Pertumbuhan PDRB (Lapangan Usaha)",
  //   component: LajuPertumbuhanPdrb,
  // },
  // {
  //   path: "/pdrb-adhk-2010-lap-usaha",
  //   exact: true,
  //   name: "PDRB ADHK 2010",
  //   component: PdrbAdhk2010LapUsaha,
  // },
  // {
  //   path: "/jumlah-museum-bds-pengelola",
  //   exact: true,
  //   name: "Jumlah Museum Berdasarkan Pengelola",
  //   component: JmlMuseumBdsPengelola,
  // },
  // {
  //   path: "/jumlah_penduduk-bds-jk",
  //   exact: true,
  //   name: "Jumlah Penduduk Berdasarkan Jenis Kelamin",
  //   component: JmlPendudukBdsJk,
  // },
  // {
  //   path: "/pdrb-adhk-jenis-pengeluaran",
  //   exact: true,
  //   name: "Produk Domestik Regional Bruto Atas Dasar Harga Berlaku Menurut Jenis Pengeluaran Di Kota Bogor",
  //   component: PdrbAdhkJenisPengeluaran,
  // },
  // {
  //   path: "/jml-angkatan-kerja-tk-pendidikan",
  //   exact: true,
  //   name: "Jumlah Angkatan Kerja Yang Bekerja Menurut Tingkat Pendidikan",
  //   component: JmlAngkatanKerjaTkPendidikan,
  // },
  {
    path: "/interoperabilitas",
    exact: true,
    name: "Interoperabilitas",
    component: Interoperabilitas,
  },
  {
    path: "/conf-visualisasi",
    exact: true,
    name: "Pengaturan Visualisasi",
    component: ConfVisualisasi,
  },
  {
    path: "/kelola-data",
    exact: true,
    name: "Kelola Data",
    component: KelolaData,
  },

  { path: "/docs", exact: true, name: "Documentation", component: OtherDocs },
  { path: "/app", exact: true, name: "App doc", component: AppTest },
  { path: "/excel-app", exact: true, name: "Excel App", component: ExcelApp },
  { path: "/app-detail", exact: true, name: "Detail App", component: ExcelApp },
  {
    path: "/remote-select",
    exact: true,
    name: "Remote",
    component: RemoteSelect,
  },
  {
    path: "/grup-dokumen",
    exact: true,
    name: "Grup Dokumen",
    component: GrupDokumen,
  },
  {
    path: "/pencarian-data",
    exact: true,
    name: "Pencarian Data",
    component: PencarianData,
  },
];

export default routes;
