export let localDisk;

if (process.env.REACT_APP_API_SERVER === "http://127.0.0.1:8000/api/v1") {
  localDisk = "http://127.0.0.1:8000/storage";
}
else {
  localDisk = "http://10.10.10.104:8000/storage";
}
// else if (
//   process.env.REACT_APP_API_SERVER === "https://apiekinerja.brin.go.id/api"
// ) {
//   localDisk = "https://minio.brin.go.id/simarinv2-brin";
// }
