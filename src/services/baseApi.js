// import Swal from "sweetalert2";
import axios from "axios";

// const token = localStorage.getItem("simarinv2_token");

/**
 * Base URL Endpoint
 * @type {import('axios').AxiosInstance}
 */
const baseApi = axios.create({
  baseURL: `${process.env.REACT_APP_API_SERVER}`,
  withCredentials: true,
  headers: {
    // Authorization: `Bearer ${token}`,
    Accept: "application/json",
    ContentType: "application/json",
  },
});

baseApi.interceptors.request.use(
  (config) => {
    // const tokenBeforeRequest = localStorage.getItem("simarinv2_token");

    // config.headers["Authorization"] = `Bearer ${tokenBeforeRequest}`;
    config.headers["Accept"] = `application/json`;
    config.headers["ContentType"] = `application/json`;

    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

//Add a response interceptor
baseApi.interceptors.response.use(
  function (response) {
    return response;
  },
  function (error) {
    if (error?.response?.status === 401) {
      //   Swal.fire({
      //     title: "Unauthorized",
      //     icon: "warning",
      //     text: "Silahkan login kembali",
      //   });
      alert("error di baseapi");
      window.localStorage.clear();
      window.location.href = "/";
    }
    return Promise.reject(error);
  }
);

export default baseApi;
